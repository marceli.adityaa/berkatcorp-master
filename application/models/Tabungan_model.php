<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabungan_model extends MY_Model {
    public $_table = 'tabungan';
    private $kolom = array('nama','nominal','jenis','arus','nominal', 'kas.label');

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit'])?$filter['limit']:'';
		$offset = isset($filter['offset'])?$filter['offset']:'';
		$sort   = isset($filter['sort'])?$filter['sort']:'update_time';
        $order  = isset($filter['sort'])?$filter['order']:'desc';
        
        # SELECT
        $this->select("tabungan.*, nama, label");

		# WHERE
        $where['id'] = isset($filter['id'])?$filter['id']:'';
        
        # JOIN
        $this->join('pegawai', 'pegawai.id = pegawai_id');
        $this->join('kas', 'kas.id = kas_id','left');

		# Exclude
		$excludes = isset($filter['excludes'])?$filter['excludes']:array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		if (!empty($limit) OR !empty($offset)) {
			$clone = clone($this->db);
			$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total'])?$results:$results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'],$filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes']);
		return trim_array($filter);
	}

	public function get_widget()
	{
		$this->select("SUM(CASE WHEN arus = 'in' THEN nominal ELSE 0 END) AS setor, SUM(CASE WHEN arus = 'out' THEN nominal ELSE 0 END) AS tarik");
		$this->where('MONTH(tanggal_transaksi)', date('m'));
		$this->where('YEAR(tanggal_transaksi)', date('Y'));
		return $this->get_all()[0];
	}

	public function get_top_penabung()
	{
		$this->select("SUM(nominal) AS nominal, nama");
		$this->join('pegawai', 'pegawai_id=pegawai.id');
		$this->where('MONTH(tanggal_transaksi)', date('m'));
		$this->where('YEAR(tanggal_transaksi)', date('Y'));
		$this->where('arus', 'in');
		$this->group_by('pegawai_id');
		return $this->get_all();
	}
}