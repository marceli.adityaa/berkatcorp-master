<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arus_kas_mutasi_model extends MY_Model {
	public $_table = 'arus_kas_mutasi';
	// private $kolom = array('label','pegawai.nama');

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_detail($id = null)
	{
		if(!empty($id)){
			$this->db->select('*, DATE_FORMAT(tgl_transaksi, "%Y-%m-%d") as tgl_transaksi');
			$this->db->where('id', $id);
			return $this->db->get('arus_kas_mutasi')->row_array();
		}
	}

	
	public function get_data($param)
	{
		$this->db->select('a.*, if(b.is_rekening = 1, concat(e.bank, " | ", b.no_rekening, " | ", b.nama), b.label) as kas_from_label, if(c.is_rekening = 1, concat(f.bank, " | ", c.no_rekening, " | ", c.nama), c.label) as kas_to_label, d.username as username_input');

		if(isset($param['kas']) && $param['kas'] != 'all'){
            $this->db->where('a.kas_from', $param['kas']);
            $this->db->or_where('a.kas_to', $param['kas']);
        }
        if(isset($param['start']) && $param['start'] != ''){
            $this->db->where('a.tanggal >=', $param['start']);
        }
        if(isset($param['end']) && $param['end'] != ''){
            $this->db->where('a.tanggal <=', $param['end']);
        }

		$this->db->join('kas b', 'a.kas_from = b.id');
		$this->db->join('kas c', 'a.kas_to = c.id');
		$this->db->join('user d', 'a.input_by = d.id');
		$this->db->join('bank e', 'b.id_bank = e.id', 'left');
		$this->db->join('bank f', 'c.id_bank = f.id', 'left');
        $this->db->order_by('a.tanggal', 'desc');
		$result = $this->db->get('arus_kas_mutasi a');
		// dump($this->db->last_query());
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_detail_mutasi($id = null){
		$this->db->where('sumber', 'mutasi-akun');
		$this->db->where('id_sumber', $id);
		$result = $this->db->get('arus_kas');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}