<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arus_kas_model extends MY_Model {
	public $_table = 'arus_kas';
	private $kolom = array('label','pegawai.nama');

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_detail($id = null)
	{
		if(!empty($id)){
			$this->db->select('*, DATE_FORMAT(tgl_transaksi, "%Y-%m-%d") as tgl_transaksi');
			$this->db->where('id', $id);
			return $this->db->get('arus_kas')->row_array();
		}
	}

	public function get_jumlah_pengajuan($param)
	{
		$this->db->select('ifnull(count(*), 0) as total');
		$this->db->where('is_approval', 0);
		$this->db->where('id_perusahaan', $param['perusahaan']);
		return $this->db->get('arus_kas')->row_array();
	}

	public function cek_validasi($id = null){
		if(!empty($id)){
			$this->db->where('id', $id);
			$result = $this->db->get('arus_kas');
			if($result->num_rows() > 0){
				$result = $result->row_array();
				if($result['is_approval'] != 1){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function get_data_persetujuan($param)
	{
		$this->db->select('a.*, if(b.is_rekening = 1, concat(e.bank, " | ", b.no_rekening, " | ", b.nama), label) as kas, c.username as username_input, ifnull(d.username, "-") as username_approval');
		$this->db->where('a.id_perusahaan', $param['id_perusahaan']);

		if(isset($param['kas']) && $param['kas'] != 'all'){
            $this->db->where('a.id_kas', $param['kas']);
        }
        if(isset($param['start']) && $param['start'] != ''){
            $this->db->where('a.tgl_transaksi >=', $param['start']);
        }
        if(isset($param['end']) && $param['end'] != ''){
            $this->db->where('a.tgl_transaksi <=', $param['end']);
        }
        if(isset($param['arus']) && $param['arus'] != 'all'){
            $this->db->where('a.arus', $param['arus']);
        }

        if(isset($param['status']) && $param['status'] != 'all'){
        	if($param['status'] == 1){
        		# Waiting
	            $this->db->where('a.is_approval', 0);
        	}else if($param['status'] == 2){
        		# Approved
        		$this->db->where('a.is_approval', 1);
        	}else if($param['status'] == 3){
        		# Rejected
        		$this->db->where('a.is_approval', 2);
        	}
        }
		$this->db->join('kas b', 'a.id_kas = b.id');
		$this->db->join('user c', 'a.input_by = c.id');
		$this->db->join('user d', 'a.approval_by = d.id', 'left');
		$this->db->join('bank e', 'b.id_bank = e.id', 'left');
        $this->db->order_by('is_approval');
        $this->db->order_by('tgl_transaksi', 'desc');
        $this->db->order_by('a.id_kas');
		$result = $this->db->get('arus_kas a');
		// dump($this->db->last_query());
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_data_rekap($param)
	{
		$this->db->select('a.*, if(b.is_rekening = 1, concat(e.bank, " | ", b.no_rekening, " | ", b.nama), label) as kas, c.username as username_input, ifnull(d.username, "-") as username_approval');
		$this->db->where('a.id_perusahaan', $param['id_perusahaan']);
		$this->db->where('a.is_approval', 1);
		if(isset($param['kas']) && $param['kas'] != 'all'){
            $this->db->where('a.id_kas', $param['kas']);
        }
        if(isset($param['start']) && $param['start'] != ''){
            $this->db->where('a.tgl_transaksi >=', $param['start']);
        }
        if(isset($param['end']) && $param['end'] != ''){
            $this->db->where('a.tgl_transaksi <=', $param['end']);
        }
        if(isset($param['arus']) && $param['arus'] != 'all'){
            $this->db->where('a.arus', $param['arus']);
        }
		$this->db->join('kas b', 'a.id_kas = b.id');
		$this->db->join('user c', 'a.input_by = c.id');
		$this->db->join('user d', 'a.approval_by = d.id', 'left');
		$this->db->join('bank e', 'b.id_bank = e.id', 'left');
        $this->db->order_by('is_approval');
        $this->db->order_by('tgl_transaksi', 'desc');
        $this->db->order_by('a.id_kas');
		$result = $this->db->get('arus_kas a');
		// dump($this->db->last_query());
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_saldo_awal($param){
		$this->db->select('a.*, if(b.is_rekening = 1, concat(e.bank, " | ", b.no_rekening, " | ", b.nama), label) as kas, ifnull(sum(if(a.arus="in",nominal,0)),0) as total_in, ifnull(sum(if(a.arus="out",nominal,0)),0) as total_out');
		$this->db->where('a.id_perusahaan', $param['id_perusahaan']);
		$this->db->where('a.is_approval', 1);
		if(isset($param['kas']) && $param['kas'] != 'all'){
            $this->db->where('a.id_kas', $param['kas']);
            $this->db->group_by('a.id_kas');
        }
        if(isset($param['start']) && $param['start'] != ''){
            $this->db->where('a.tgl_transaksi <', $param['start']);
        }else{
        	$this->db->where('a.tgl_transaksi', '0000-00-00');
        }

		$this->db->join('kas b', 'a.id_kas = b.id');
		$this->db->join('user c', 'a.input_by = c.id');
		$this->db->join('user d', 'a.approval_by = d.id', 'left');
		$this->db->join('bank e', 'b.id_bank = e.id', 'left');
        $this->db->order_by('tgl_transaksi', 'desc');
        $this->db->order_by('a.id_kas');
		$result = $this->db->get('arus_kas a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_saldo_awal_export($param){
		$this->db->select('a.*, if(b.is_rekening = 1, concat(e.bank, " | ", b.no_rekening, " | ", b.nama), label) as kas, ifnull(sum(if(a.arus="in",nominal,0)),0) as total_in, ifnull(sum(if(a.arus="out",nominal,0)),0) as total_out');
		$this->db->where('a.id_perusahaan', $param['id_perusahaan']);
		$this->db->where('a.is_approval', 1);
		if(isset($param['kas']) && $param['kas'] != 'all'){
            $this->db->where('a.id_kas', $param['kas']);
            $this->db->group_by('a.id_kas');
        }
        if(isset($param['start']) && $param['start'] != ''){
            $this->db->where('a.tgl_transaksi <', $param['start']);
        }else{
        	$this->db->where('a.tgl_transaksi', '0000-00-00');
        }

		$this->db->join('kas b', 'a.id_kas = b.id');
		$this->db->join('user c', 'a.input_by = c.id');
		$this->db->join('user d', 'a.approval_by = d.id', 'left');
		$this->db->join('bank e', 'b.id_bank = e.id', 'left');
        $this->db->order_by('tgl_transaksi', 'desc');
        $this->db->order_by('a.id_kas');
        $this->db->group_by('a.id_kas');
		$result = $this->db->get('arus_kas a');
		// dump($this->db->last_query());
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}