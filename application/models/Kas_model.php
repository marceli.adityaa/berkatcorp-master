<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kas_model extends MY_Model {
	public $_table = 'kas';
	private $kolom = array('label','pegawai.nama');

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_active($param){
		$this->db->where('is_disabled', 0);
		$this->db->where('is_rekening', 0);
		$this->db->where('id_perusahaan', $param['id_perusahaan']);
		$this->db->order_by('label');
		$result = $this->db->get('kas');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_rekening($param){
		$this->db->select('a.*, b.bank');
		$this->db->where('a.is_disabled', 0);
		$this->db->where('a.is_rekening', 1);
		$this->db->where('a.id_perusahaan', $param['id_perusahaan']);
		$this->db->order_by('b.bank');
		$this->db->join('bank b', 'a.id_bank = b.id', 'left');
		$result = $this->db->get('kas a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_active_all($param){
		$this->db->where('is_disabled', 0);
		$this->db->where('is_rekening', 0);
		$this->db->order_by('label');
		$result = $this->db->get('kas');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_rekening_all($param){
		$this->db->select('a.*, b.bank');
		$this->db->where('a.is_disabled', 0);
		$this->db->where('a.is_rekening', 1);
		$this->db->order_by('b.bank');
		$this->db->join('bank b', 'a.id_bank = b.id', 'left');
		$result = $this->db->get('kas a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
    
	public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit'])?$filter['limit']:'';
		$offset = isset($filter['offset'])?$filter['offset']:'';
		$sort   = isset($filter['sort'])?$filter['sort']:'update_time';
		$order  = isset($filter['sort'])?$filter['order']:'desc';

		# WHERE
        $where['id'] = isset($filter['id'])?$filter['id']:'';
        
        # SELECT
        $this->select('kas.*, b.nama as pic, c.nama_singkat as perusahaan, d.bank');

        # JOIN
		$this->join('pegawai b', 'pegawai_id = b.id');
		$this->join('perusahaan c', 'id_perusahaan = c.id');
		$this->db->join('bank d', 'id_bank = d.id', 'left');

		# Exclude
		$excludes = isset($filter['excludes'])?$filter['excludes']:array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		if (!empty($limit) OR !empty($offset)) {
			$clone = clone($this->db);
			$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}
		$results['rows'] = $this->get_many_by($where);
		return isset($results['total'])?$results:$results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'],$filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes']);
		return trim_array($filter);
	}
}