<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime_model extends MY_Model
{
    public $_table = 'overtime';
    private $kolom = array('pegawai.nama', 'keterangan');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($filter = array())
    {
        $filter = trim_array($filter);
        # LIMIT, OFFSET, AND SORT
        $limit  = isset($filter['limit']) ? $filter['limit'] : '';
        $offset = isset($filter['offset']) ? $filter['offset'] : '';
        $sort   = isset($filter['sort']) ? $filter['sort'] : 'insert_time';
        $order  = isset($filter['sort']) && isset($filter['order']) ? $filter['order'] : 'DESC';

        # SELECT
        $this->select($this->_table . '.*, pegawai.nama AS nama');

        # WHERE
        $where['id'] = isset($filter['id']) ? $filter['id'] : '';
        $where['tanggal_pengajuan >=']  = isset($filter['tanggal_pengajuan_from']) ? $filter['tanggal_pengajuan_from'] : '';
        $where['tanggal_pengajuan <='] = isset($filter['tanggal_pengajuan_to']) ? $filter['tanggal_pengajuan_to'] : '';
        $where['overtime_pengajuan.status'] = isset($filter['status']) ? $filter['status'] : '';

        # JOIN
        $this->join('pegawai', 'pegawai_id = pegawai.id');

        # EXCLUDE
        $excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

        # UNSET FILTER

        $filter = $this->unsetFilter($this->setFilter($filter));
        $where  = trim_array($where);

        # SET LIKE
        if (count($filter) > 0) {
            $this->group_start();
            $this->or_like($filter);
            $this->group_end();
        }

        # SET WHERE NOT IN
        if (count($excludes) > 0) {
            foreach ($excludes as $key => $value) {
                $this->where_not_in($key, $value);
            }
        }

        if (!empty($limit) or !empty($offset)) {
            $clone = clone ($this->db);
            $results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

            $this->limit($limit, $offset);
        }

        # SET SORT
        if (!empty($sort)) {
            $this->order_by($sort, $order);
        }

        $results['rows'] = $this->get_many_by($where);
        return isset($results['total']) ? $results : $results['rows'];
    }

    private function setFilter($filter = array())
    {
        if (isset($filter['search'])) {
            foreach ($this->kolom as $k) {
                $filter[$k] = $filter['search'];
            }
        }

        return $filter;
    }

    private function unsetFilter($filter)
    {
        unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
        unset($filter['id'], $filter['search'], $filter['excludes']);
        unset($filter['status'], $filter['tanggal_pengajuan_from'], $filter['tanggal_pengajuan_to']);
        return trim_array($filter);
    }

    public function get_saldo_by($id)
    {
        $jumlah = $this->count_by(array('pegawai_id' => $id, 'arus' => 'tambah'));
        $terpakai = $this->count_by(array('pegawai_id' => $id, 'arus' => 'kurang'));
        return $jumlah - $terpakai;
    }

    public function get_widget()
	{
		$this->select("SUM(CASE WHEN arus = 'in' THEN durasi ELSE 0 END) AS tambah, SUM(CASE WHEN arus = 'out' THEN durasi ELSE 0 END) AS kurang");
		$this->where('MONTH(waktu_mulai)', date('m'))->where('YEAR(waktu_mulai)', date('Y'));
		return $this->get_all()[0];
    }
    
    public function get_graph()
    {
        $year = date('Y');
        $tambah = []; $kurang = [];
        
        for($i = 1; $i <= 12; $i++) {
            $this->select("SUM(CASE WHEN arus = 'in' THEN durasi ELSE 0 END) AS tambah, SUM(CASE WHEN arus = 'out' THEN durasi ELSE 0 END) AS kurang");
            $data = $this->get_by(['MONTH(waktu_mulai)'=>$i, 'YEAR(waktu_mulai)'=>$year]);
            $tambah[] = $data->tambah;
            $kurang[] = $data->kurang;
        }
        
        return ['tambah'=>$tambah, 'kurang'=>$kurang];
    }
    
    public function get_today_overtime()
    {
        $this->join('pegawai','pegawai_id=pegawai.id');
        $this->order_by('waktu_mulai,waktu_selesai');
        return $this->get_many_by(array('DATE(waktu_mulai)'=>date('Y-m-d')));
    }

    public function get_data_cetak_absen($start, $end)
    {
        $this->db->select("pegawai_id, IFNULL(SUM(IF(arus='in', durasi, 0)),0) AS lembur, IFNULL(SUM(IF(arus='out', durasi, 0)),0) AS minus");
        $this->db->where('DATE(waktu_mulai) >=', $start);
        $this->db->where('DATE(waktu_mulai) <=', $end);
        $this->db->group_by('pegawai_id');
        $this->db->order_by('pegawai_id');
        $result = $this->db->get('overtime');
        if($result->num_rows() > 1){
            return $result->result_array();
        }else{
            return false;
        }
    }
}
