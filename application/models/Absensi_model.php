<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

defined('BASEPATH') or exit('No direct script access allowed');

class Absensi_model extends MY_Model
{
	public $_table = 'absensi';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data_absensi($tanggal = '')
	{
		return $this
			->select('absensi.*, pegawai.nama, pegawai.jenis_pegawai, pegawai_kelompok.nama AS kelompok, nominal, kas_id, fingerprint_id')
			->join('tabungan', 'tabungan.ref_id = tabungan_ref_id', 'left')
			->join('pegawai', 'absensi.pegawai_id = pegawai.id')
			->join('pegawai_kelompok', 'kelompok_id = pegawai_kelompok.id')
			->order_by('pegawai_kelompok.nama, pegawai.nama')
			->get_many_by('tanggal', $tanggal);
	}

	public function get_data_absensi_group($filter = array())
	{
		$filter = trim_array($filter);
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit']) ? $filter['limit'] : '';
		$offset = isset($filter['offset']) ? $filter['offset'] : '';
		$sort   = isset($filter['sort']) ? $filter['sort'] : 'tanggal';
		$order  = isset($filter['sort']) && isset($filter['order']) ? $filter['order'] : 'DESC';

		# SELECT
		$this->select("tanggal, SUM(CASE WHEN kehadiran = 'hadir' OR kehadiran = 'libur' THEN 1 ELSE 0 END) AS jumlah_hadir, SUM(CASE WHEN kehadiran <> 'hadir' AND kehadiran <> 'libur' AND kehadiran <> '' THEN 1 ELSE 0 END) AS jumlah_absen, SUM(nominal) AS tabungan, SUM(CASE WHEN kehadiran = 'hadir' AND (scanlog_masuk is null OR scanlog_masuk = '00:00:00') THEN 1 ELSE 0 END) AS scanlog_masuk, SUM(CASE WHEN kehadiran = 'hadir' AND (scanlog_pulang is null OR scanlog_pulang = '00:00:00') THEN 1 ELSE 0 END) AS scanlog_pulang");

		# WHERE
		$where['is_deleted'] = 0;
		$where['tanggal <='] = isset($filter['tanggal_to']) ? $filter['tanggal_to'] : date('Y-m-d', strtotime('-1 days'));
		$where['tanggal >='] = isset($filter['tanggal_from']) ? $filter['tanggal_from'] : date('Y-m-d', strtotime('-7 days'));

		# JOIN
		$this->join('tabungan', 'tabungan_ref_id=tabungan.ref_id', 'LEFT');
		# GROUP
		$this->group_by('tanggal');

		# EXCLUDE
		$excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		if (!empty($limit) or !empty($offset)) {
			$clone = clone ($this->db);
			$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total']) ? $results : $results['rows'];
	}

	public function get_data($filter = array())
	{
		$filter = trim_array($filter);
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit']) ? $filter['limit'] : '';
		$offset = isset($filter['offset']) ? $filter['offset'] : '';
		$sort   = isset($filter['sort']) ? $filter['sort'] : 'insert_time';
		$order  = isset($filter['sort']) ? $filter['order'] : 'desc';

		# SELECT
		$this->select("absensi.*,pegawai.nama,pegawai_kelompok.nama AS kelompok,fingerprint_id");

		# WHERE
		$where['id'] = isset($filter['id']) ? $filter['id'] : '';

		# JOIN
		$this->join('pegawai', 'pegawai.id=pegawai_id');
		$this->join('pegawai_kelompok', 'pegawai_kelompok.id=kelompok_id');

		# GROUP

		# EXCLUDE
		$excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

		# UNSET FILTER
		$this->kolom = array('pegawai.nama', 'kehadiran', 'tanggal', 'pegawai_kelompok.nama', 'keterlambatan');
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		if (!empty($limit) or !empty($offset)) {
			$clone = clone ($this->db);
			$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total']) ? $results : $results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes']);
		unset($filter['tanggal_from'], $filter['tanggal_to']);
		return trim_array($filter);
	}

	public function get_widget()
	{
		$m = date('m');
		$y = date('Y');

		$this->select("COUNT(id) AS total, SUM(CASE WHEN kehadiran <> 'hadir' THEN 1 ELSE 0 END) AS absen, AVG(CASE WHEN keterlambatan > 0 THEN keterlambatan ELSE null END) AS rata");
		$result = $this->get_by(array('MONTH(tanggal)' => $m, 'YEAR(tanggal)' => $y));

		$result->total = empty($result->total) ? 0 : $result->total;
		$result->absen = empty($result->absen) ? 0 : $result->absen;
		$result->rata = empty($result->rata) ? 0 : $result->rata;

		return $result;
	}

	public function get_graph()
	{
		$result = array();
		# Keterlambatan
		$now = date('Y-m-d');
		$result['keterlambatan'] = array();
		$result['hari'] = array();
		for($i = 7; $i > 0; $i--) {
			$this->select("AVG(CASE WHEN keterlambatan > 0 THEN keterlambatan ELSE null END) AS rata");
			$this->where("tanggal = '{$now}' - INTERVAL {$i} DAY");
			$result['keterlambatan'][] = $this->get_by([])->rata;
			$result['hari'][] = date('d/m', strtotime($now." - {$i} DAYS"));
		}

		# Kehadiran
		$result['kehadiran'] = array();
		$result['bulan'] = array();
		for($i=5; $i>=0; $i--) {
			$year  = date('Y', strtotime(date('Y-m-d')." -{$i} MONTHS"));
			$month = date('m', strtotime(date('Y-m-d')." -{$i} MONTHS"));

			$this->select("SUM(CASE WHEN (kehadiran = 'hadir' AND keterlambatan = 0) OR kehadiran = 'libur' THEN 1 ELSE 0 END) AS hadir");
			$this->select("SUM(CASE WHEN kehadiran <> 'hadir' AND kehadiran <> 'alpa' THEN 1 ELSE 0 END) AS ijin");
			$this->select("SUM(CASE WHEN kehadiran = 'alpa' THEN 1 ELSE 0 END) AS absen");
			$data = $this->get_by(['MONTH(tanggal)'=>$month,'YEAR(tanggal)'=>$year]);

			$result['kehadiran']['hadir'][] = $data->hadir;
			$result['kehadiran']['ijin'][] = $data->ijin;
			$result['kehadiran']['absen'][] = $data->absen;
			$result ['bulan'][] = datify("{$year}-{$month}-1", 'F Y');
		}

		return $result;
	}

	public function cetak($filter = [])
	{
		# MODEL
		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Overtime_model', 'overtime');
		# VAR
		$kolom = 1;
		$baris = 1;
		$jumlahKolom = count($filter['kolom']);

		# INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		# PROSES - HEADER
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'No');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pegawai');
		$sheet->mergeCells('A1:A2')->mergeCells('B1:B2');

		# HEADER - TANGGAL
		$jangka = $this->getDateRange($filter['tanggal_awal'], $filter['tanggal_akhir']);
		foreach ($jangka as $j) {
			$c1 = $sheet->getCellByColumnAndRow($kolom, $baris)->getCoordinate();
			$c2 = $sheet->getCellByColumnAndRow($kolom + $jumlahKolom - 1, $baris)->getCoordinate();
			$c3 = $sheet->getCellByColumnAndRow($kolom, $baris + 1)->getCoordinate();
			$sheet->mergeCells($c1 . ':' . $c2);
			$sheet->setCellValue($c1, $j);
			$sheet->fromArray($filter['kolom'], NULL, $c3);
			$kolom += $jumlahKolom;
		}

		# HEADER REKAP
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'MASUK');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'ABSEN');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'CUTI');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'SAKIT');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'LIBUR');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'KETERLAMBATAN');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'LEMBUR');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'OVERTIME AKHIR');
		
		# TULIS PEGAWAI
		$kolom = 1;
		$baris = 3;
		$pegawaiRaw = $this->pegawai->select('pegawai.id, pegawai.nama, pegawai_kelompok.nama AS kelompok, pegawai.is_disabled')->join('pegawai_kelompok','kelompok_id=pegawai_kelompok.id')->order_by('pegawai_kelompok.nama')->order_by('pegawai.nama')->get_many_by(['tanggal_masuk<=' => $filter['tanggal_akhir'], 'pegawai.is_disabled' => 0]);
		$pegawai = [];
		
		$kelompokNow = "";
		foreach ($pegawaiRaw as $index => $value) {
			if($value->kelompok != $kelompokNow) {
				$sheet->setCellValueByColumnAndRow($kolom, $baris++, $value->kelompok);
				$kelompokNow = $value->kelompok;
				$counter = 1;
			}
			
			$sheet->setCellValueByColumnAndRow($kolom, $baris, $counter++);
			$sheet->setCellValueByColumnAndRow($kolom + 1, $baris, strtoupper($value->nama));
			$pegawai[$value->id] = ['nama' => $value->nama, 'baris' => $baris++, 'hadir' => 0, 'absen' => 0, 'sakit' => 0, 'ijin'=>0, 'libur'=>0, 'lembur' => 0, 'keterlambatan' => 0, 'minus' => 0];
		}
		
		# HITUNG OVERTIME
		$overtime = $this->overtime->get_data_cetak_absen($filter['tanggal_awal'], $filter['tanggal_akhir']);
		foreach($pegawai as $key => $pg){
			foreach($overtime as $ot){
				if($key == $ot['pegawai_id']){
					$pegawai[$key]['lembur'] += $ot['lembur'];
					$pegawai[$key]['minus'] += $ot['minus'];
				break;			
				}
			}
		}
		# TULIS ABSENSI
		$kolom = 3;
		foreach ($jangka as $tgl) {
			$absensi = $this->get_many_by(['tanggal' => $tgl]);
			foreach ($absensi as $ab) {
				if(array_key_exists($ab->pegawai_id, $pegawai)){
					$baris = $pegawai[$ab->pegawai_id]['baris'];
					foreach ($filter['kolom'] as $i => $kol) {
						$sheet->setCellValueByColumnAndRow($kolom + $i, $baris, $ab->{$kol});
					}
					# HITUNG
					$pegawai[$ab->pegawai_id] = $this->hitungAbsensi($pegawai[$ab->pegawai_id], $ab);
				}
			}
			$kolom += $jumlahKolom;
		}
		
		foreach($pegawai AS $p) {
			$sheet->setCellValueByColumnAndRow($kolom, $p['baris'], $p['hadir']);
			$sheet->setCellValueByColumnAndRow($kolom+1, $p['baris'], $p['absen']);
			$sheet->setCellValueByColumnAndRow($kolom+2, $p['baris'], $p['ijin']);
			$sheet->setCellValueByColumnAndRow($kolom+3, $p['baris'], $p['sakit']);
			$sheet->setCellValueByColumnAndRow($kolom+4, $p['baris'], $p['libur']);
			$sheet->setCellValueByColumnAndRow($kolom+5, $p['baris'], $p['minus']);
			$sheet->setCellValueByColumnAndRow($kolom+6, $p['baris'], $p['lembur']);
			$sheet->setCellValueByColumnAndRow($kolom+7, $p['baris'], $p['lembur']-$p['minus']);
		}

		# STYLING
		$lastCell = $sheet->getHighestColumn() . '' . $sheet->getHighestRow();
		$sheet->getStyle('A1:' . $lastCell)->getAlignment()->setHorizontal('center')->setVertical('center');
		$sheet->getColumnDimension('B')->setAutoSize(true);
		# SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="laporan_absen.xlsx"');
		$writer->save("php://output");
	}

	private function getDateRange($from, $to)
	{
		$result = [];
		$period = new DatePeriod(
			new DateTime($from),
			new DateInterval('P1D'),
			new DateTime($to)
		);

		foreach ($period as $key => $value) {
			$result[] = $value->format('Y-m-d');
		}

		return $result;
	}

	private function hitungAbsensi($pegawai = [], $absensi = [])
	{
		# KETERLAMBATAN
		$pegawai['keterlambatan'] += $absensi->keterlambatan;
		# KEHADIRAN
		switch ($absensi->kehadiran) {
			case 'libur':
				$pegawai['libur']++;
				break;
			case 'hadir':
				$pegawai['hadir']++;
				break;
			case 'alpa':
				$pegawai['absen']++;
				break;
			case 'sakit':
				$pegawai['sakit']++;
				break;	
			default:
				$pegawai['ijin']++;
				break;
		}

		# LEMBUR
		return $pegawai;
	}

	public function get_rekap_tahun($year = null){
		$this->db->select('year(tanggal) as tahun, month(tanggal) as bulan, pegawai_id, sum(if(kehadiran="hadir", 1,0)) as hadir, sum(if(kehadiran="alpa", 1,0)) as alpa, sum(if(kehadiran="cuti", 1,0)) as cuti, sum(if(kehadiran="sakit", 1,0)) as sakit, sum(if(kehadiran="libur", 1,0)) as libur');
		$this->db->where('year(tanggal)', $year);
		$this->db->group_by('year(tanggal)');
		$this->db->group_by('month(tanggal)');
		$this->db->group_by('pegawai_id');
		$this->db->order_by('month(tanggal)');
		$this->db->order_by('pegawai_id');
		return $this->db->get('absensi')->result();

	}
}
