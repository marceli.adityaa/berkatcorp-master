<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Credential_model extends MY_Model {
	public $_table = 'credential';

	public function __construct()
	{
		parent::__construct();
	}
}