<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends MY_Model
{
	public $_table = 'menu';
	public $appid = 'mstr';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_all_menu()
	{
		# TRANSPORTASI
		$tran_menu = new stdClass();
		$tran_addr = $this->config->item('transport_address').'/api/external/menu/get_active/'.$this->session->auth['token'];
		$tran_res = xcurl(array(CURLOPT_URL=>$tran_addr));
		if(!$tran_res['err']) {
			$tran_menu = json_decode($tran_res['response']);
		}

		# BAS
		$bas_menu = new stdClass();
		$bas_addr = $this->config->item('bas_address').'/api/external/menu/get_active/'.$this->session->auth['token'];
		$bas_res = xcurl(array(CURLOPT_URL=>$bas_addr));
		if(!$bas_res['err']) {
			$bas_menu = json_decode($bas_res['response']);
		}

 		$data[] = (object) array('uuid' => 'mstr','label' => 'Menu Master','menu' => $this->get_data());
		$data[] = (object) array('uuid' => 'tran','label' => 'Menu Transportasi','menu' => $tran_menu);
		$data[] = (object) array('uuid' => 'bas','label' => 'Menu PT.BAS','menu' => $bas_menu);
		return $data;
	}

	public function get_data()
	{ 
		$mParent = $this->order_by('urutan')->get_many_by(array('child_of' => 0));
		foreach ($mParent as $mp) {
			$mChild = $this->order_by('urutan')->get_many_by(array('child_of' => $mp->id));
			if ($mChild) {
				$mp->child = $mChild;
			}
		}
		return $mParent;
	}

	public function get_sidebar_menu()
	{
		$akses = json_decode($this->session->auth['hak_akses']);
		# Generate menu
		if (!isset($akses->{$this->appid}))
			return new stdClass();

		$akses = $akses->{$this->appid};
		$mParent = $this->where_in('uuid', $akses)->order_by('urutan')->get_many_by(array('is_disabled' => 0, 'child_of' => 0));
		foreach ($mParent as $mp) {
			$mChild = $this->where_in('uuid', $akses)->order_by('urutan')->get_many_by(array('is_disabled' => 0, 'child_of' => $mp->id));
			if ($mChild) {
				$mp->child = $mChild;
			}
		}
		return $mParent;
	}
}
