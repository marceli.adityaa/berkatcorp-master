<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends MY_Model {
	public $_table = 'pegawai';
	private $kolom = array('pegawai.nama','perusahaan_divisi.nama','perusahaan_jabatan.nama', 'pegawai_kelompok.nama');

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit'])?$filter['limit']:'';
		$offset = isset($filter['offset'])?$filter['offset']:'';
		$sort   = isset($filter['sort'])?$filter['sort']:'';
		$order  = isset($filter['order'])?$filter['order']:'';

		# SELECT
		$this->select('pegawai.*, perusahaan_divisi.nama AS divisi, perusahaan_jabatan.nama AS jabatan, pegawai_kelompok.nama AS kelompok');

		# WHERE
		$where['id'] = isset($filter['id'])?$filter['id']:'';
		$where['kelompok_id'] = isset($filter['kelompok_id'])?$filter['kelompok_id']:'';
		$where['pegawai.is_disabled'] = isset($filter['is_disabled'])?$filter['is_disabled']:'';

		# JOIN
		$this->join('perusahaan_divisi', 'perusahaan_divisi.id = divisi_id');
		$this->join('perusahaan_jabatan', 'perusahaan_jabatan.id = jabatan_id');
		$this->join('pegawai_kelompok', 'pegawai_kelompok.id = kelompok_id');

		# EXCLUDE
		$excludes = isset($filter['excludes'])?$filter['excludes']:array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		# Count Total
		$clone = clone($this->db);
		$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

		if (!empty($limit) OR !empty($offset)) {
			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return $results;
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'],$filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['search'], $filter['excludes']);
		unset($filter['id'], $filter['kelompok_id'], $filter['is_disabled']);
		return trim_array($filter);
	}

	public function get_active_driver(){
		$this->db->select('id, nama');
		$this->db->where('kelompok_id', 2);
		$this->db->where('is_disabled', 0);
		$this->db->where('is_deleted', 0);
		$this->db->order_by('nama');
		$result['rows'] = $this->db->get('pegawai')->result();
		return $result;
	}
}