<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

defined('BASEPATH') or exit('No direct script access allowed');

class Cuti_model extends MY_Model
{
	public $_table = 'cuti';
	private $kolom = array('pegawai.nama', 'keterangan', 'kategori');

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($filter = array())
	{
		$filter = trim_array($filter);
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit']) ? $filter['limit'] : '';
		$offset = isset($filter['offset']) ? $filter['offset'] : '';
		$sort   = isset($filter['sort']) ? $filter['sort'] : $this->_table.'.insert_time';
		$order  = isset($filter['sort']) && isset($filter['order']) ? $filter['order'] : 'DESC';

		# SELECT
		$this->select($this->_table . '.*, tanggal_cuti, pegawai.nama AS pegawai');

		# WHERE
		$where['id'] = isset($filter['id']) ? $filter['id'] : '';
		$where['arus'] = isset($filter['arus']) ? $filter['arus'] : '';
		$where['tanggal_cuti >=']  = isset($filter['tanggal_cuti_from']) ? $filter['tanggal_cuti_from'] : '';
		$where['tanggal_cuti <='] = isset($filter['tanggal_cuti_to']) ? $filter['tanggal_cuti_to'] : '';
		$where['tanggal_pengajuan >=']  = isset($filter['tanggal_pengajuan_from']) ? $filter['tanggal_pengajuan_from'] : '';
		$where['tanggal_pengajuan <='] = isset($filter['tanggal_pengajuan_to']) ? $filter['tanggal_pengajuan_to'] : '';
		$where['cuti_pengajuan.status'] = isset($filter['status']) ? $filter['status'] : '';

		# JOIN
		$this->join('pegawai', 'pegawai_id = pegawai.id');

		# EXCLUDE
		$excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

		# UNSET FILTER

		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		if (!empty($limit) or !empty($offset)) {
			$clone = clone ($this->db);
			$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total']) ? $results : $results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes']);
		unset($filter['tanggal_cuti_from'], $filter['tanggal_cuti_to'], $filter['status'], $filter['tanggal_pengajuan_from'], $filter['tanggal_pengajuan_to']);
		return trim_array($filter);
	}

	public function get_saldo($id)
	{
		$jumlah = $this->count_by(array('pegawai_id' => $id, 'arus' => 'tambah'));
		$terpakai = $this->count_by(array('pegawai_id' => $id, 'arus' => 'kurang'));
		return $jumlah - $terpakai;
	}

	public function get_widget()
	{
		$m = date('m');
		$y = date('Y');

		$query = "SUM(CASE WHEN MONTH(tanggal_cuti)='{$m}' AND YEAR(tanggal_cuti)='{$y}' THEN 1 ELSE 0 END) AS cuti_sekarang,";
		$query .= "SUM(CASE WHEN MONTH(tanggal_cuti)='" . ($m + 1) . "' AND YEAR(tanggal_cuti)='{$y}' THEN 1 ELSE 0 END) AS cuti_depan";

		$this->select($query);
		$result = $this->as_array()->get_all()[0];
		$result['cuti_menunggu'] = $this->db->where(array('status' => 0))->from('cuti_pengajuan')->count_all_results();

		return $result;
	}

	public function get_graph()
	{
		$result = [];
		$year = date('Y');

		for ($i = 1; $i <= 12; $i++) {
			$result[$i - 1] = $this->select("COUNT(id) AS jumlah")->get_by(["MONTH(tanggal_cuti)" => $i, "YEAR(tanggal_cuti)" => $year])->jumlah;
		}

		return $result;
	}

	public function cetak()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		# Some init
		$kolom = 1;
		$baris = 1;
		$bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
		# Tulis Header
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'No');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Nama');
		for ($i = 0; $i < 12; $i++) {
			$sheet->setCellValueByColumnAndRow($kolom + $i, $baris, $bulan[$i]);
		}
		$kolom += 12;
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Cuti');
		$sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sisa Cuti');

		# Tulis data
		$baris++;
		$year = date('Y');
		$dataCuti = $this->getRekapCuti($year);
		$kelompok = "";

		foreach ($dataCuti as $index => $p) {
			$kolom = 1;
			$terpakai = 0;
			# Cetak label kelompok
			if($p->kelompok != $kelompok) {
				$kelompok = $p->kelompok;
				$sheet->setCellValueByColumnAndRow($kolom, $baris, strtoupper($kelompok));
				$koor1 = $sheet->getCellByColumnAndRow($kolom, $baris)->getCoordinate();;
				$koor2 = $sheet->getCellByColumnAndRow($kolom + 15, $baris)->getCoordinate();
				$sheet->mergeCells($koor1 . ':' . $koor2);
				$sheet->getStyle($koor1)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e6e6e6');
				$baris++;
			}

			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $index + 1);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, strtoupper($p->nama));
			for ($i = 0; $i < 12; $i++) {
				$sheet->setCellValueByColumnAndRow($kolom + $i, $baris, $p->cuti[$i]);
				$terpakai += $p->cuti[$i];
				# Fill
				if (!empty($p->cuti[$i])) {
					$koor = $sheet->getCellByColumnAndRow($kolom + $i, $baris)->getCoordinate();
					$sheet->getStyle($koor)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffff00');
				}
			}
			$kolom += 12;
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $terpakai);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris++, 12 - $terpakai);
		}

		# STYLE
		$lastCell = $sheet->getHighestColumn() . '' . $sheet->getHighestRow();
		$sheet->getStyle('A1:' . $lastCell)->getAlignment()->setHorizontal('center')->setVertical('center');

		# SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="laporan_cuti_' . (date('Y')) . '.xlsx"');
		$writer->save("php://output");
	}

	private function getRekapCuti($year = '')
	{
		$this->load->model('Pegawai_model', 'pegawai');
		$pegawai = $this->pegawai->select('pegawai.id, pegawai.nama, pegawai_kelompok.nama AS kelompok')->join('pegawai_kelompok', 'kelompok_id = pegawai_kelompok.id')->order_by('kelompok_id')->get_many_by(['pegawai.is_disabled' => 0]);
		for ($i = 1; $i <= 12; $i++) {
			$cuti = $this->select("COUNT(id) AS total_cuti, pegawai_id")->group_by('pegawai_id')->get_many_by(['YEAR(tanggal_cuti)' => $year, 'MONTH(tanggal_cuti)' => $i]);
			$cuti = array_column($cuti, 'total_cuti', 'pegawai_id');
			# Tulis pada data pegawai
			foreach ($pegawai as $index => $value) {
				if (!isset($value->cuti)) {
					$pegawai[$index]->cuti = array();
				}
				array_push($pegawai[$index]->cuti, array_key_exists($value->id, $cuti) ? $cuti[$value->id] : 0);
			}
		}

		return $pegawai;
	}

	public function cetak_rekap(){
		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Absensi_model', 'absensi');
		$year = $this->input->get('year');
		$pegawai = $this->pegawai->select('pegawai.id, pegawai.nama, pegawai_kelompok.nama AS kelompok')->join('pegawai_kelompok', 'kelompok_id = pegawai_kelompok.id')->order_by('kelompok_id')->get_many_by(['pegawai.is_disabled' => 0]);
		$absen = $this->absensi->get_rekap_tahun($year);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		# Some init
		$kolom = 1;
		$baris = 1;
		$bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
		# Tulis Header
		$sheet->mergeCellsByColumnAndRow($kolom, $baris, $kolom, ($baris+1))->setCellValueByColumnAndRow($kolom++, $baris, 'No');
		$sheet->mergeCellsByColumnAndRow($kolom, $baris, $kolom, ($baris+1))->setCellValueByColumnAndRow($kolom++, $baris, 'Lingkup');
		$sheet->mergeCellsByColumnAndRow($kolom, $baris, $kolom, ($baris+1))->setCellValueByColumnAndRow($kolom++, $baris, 'Nama');
		$subkolom = $kolom;
		for ($i = 0; $i < 12; $i++) {
			$sheet->setCellValueByColumnAndRow($subkolom, $baris, $bulan[$i])->mergeCellsByColumnAndRow($subkolom, $baris, $subkolom+3, $baris);
			$sheet->setCellValueByColumnAndRow($kolom++, ($baris+1), 'Hadir');
			$sheet->setCellValueByColumnAndRow($kolom++, ($baris+1), 'Cuti');
			$sheet->setCellValueByColumnAndRow($kolom++, ($baris+1), 'Alpa');
			$sheet->setCellValueByColumnAndRow($kolom++, ($baris+1), 'Sakit');
			$subkolom+=4;
		}
		$sheet->mergeCellsByColumnAndRow($kolom, $baris, $kolom, ($baris+1))->setCellValueByColumnAndRow($kolom++, $baris, 'Total Hadir');
		$sheet->mergeCellsByColumnAndRow($kolom, $baris, $kolom, ($baris+1))->setCellValueByColumnAndRow($kolom++, $baris, 'Total Cuti');
		$sheet->mergeCellsByColumnAndRow($kolom, $baris, $kolom, ($baris+1))->setCellValueByColumnAndRow($kolom++, $baris, 'Total Alpa');
		$sheet->mergeCellsByColumnAndRow($kolom, $baris, $kolom, ($baris+1))->setCellValueByColumnAndRow($kolom++, $baris, 'Total Sakit');


		# Tulis data
		$baris+=2;
		$kelompok = "";
		$index = 1;
		foreach($pegawai as $peg){
			$kolom = 1;
			$total_cuti = 0;
			$total_hadir = 0;
			$total_alpa = 0;
			$total_sakit = 0;

			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $index ++);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, strtoupper($peg->kelompok));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, strtoupper($peg->nama));
			for($bulan=1; $bulan <= 12; $bulan++){
				$found = false;
				foreach($absen as $ab){
					if($peg->id == $ab->pegawai_id && $bulan == $ab->bulan){
						$sheet->setCellValueByColumnAndRow($kolom++, $baris, $ab->hadir);
						$sheet->setCellValueByColumnAndRow($kolom++, $baris, $ab->cuti);
						$sheet->setCellValueByColumnAndRow($kolom++, $baris, $ab->alpa);
						$sheet->setCellValueByColumnAndRow($kolom++, $baris, $ab->sakit);
						$total_hadir += $ab->hadir;
						$total_cuti += $ab->cuti;
						$total_alpa += $ab->alpa;
						$total_sakit += $ab->sakit;
						$found = true;
						break;
					}
				}
				if(!$found){
					$sheet->setCellValueByColumnAndRow($kolom++, $baris, 0);
					$sheet->setCellValueByColumnAndRow($kolom++, $baris, 0);
					$sheet->setCellValueByColumnAndRow($kolom++, $baris, 0);
					$sheet->setCellValueByColumnAndRow($kolom++, $baris, 0);
				}

			}
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $total_hadir);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $total_cuti);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $total_alpa);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $total_sakit);
			$baris++;
		}

		# STYLE
		$lastCell = $sheet->getHighestColumn() . '' . $sheet->getHighestRow();
		$sheet->getStyle('D1:' . $lastCell)->getAlignment()->setHorizontal('center')->setVertical('center');

		# SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="laporan_kehadiran_' . $year . '.xlsx"');
		$writer->save("php://output");
	}
}
