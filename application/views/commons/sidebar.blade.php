<div class="am-sideleft">
	<ul class="nav am-sideleft-menu">
		@foreach($menu AS $m)
		<li class="nav-item">
			<a href="{{!empty($m->tautan)?site_url($m->tautan):'#'}}" class="nav-link {{isset($m->child)?'with-sub':''}} {{$m->class}}">
				<i class="{{$m->icon}}"></i><span>{{ucwords($m->label)}}</span>
			</a>
			@if(isset($m->child))
			<ul class="nav-sub">
				@foreach($m->child AS $c)
				<li class="nav-item"><a href="{{site_url($c->tautan)}}" class="nav-link"><i class="{{$c->icon}} mr-2"></i>{{ucwords($c->label)}}</a></li>
				@endforeach
			</ul>
			@endif
		</li>
		@endforeach
	</ul>
</div>