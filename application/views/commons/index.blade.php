<!DOCTYPE html>
<html lang="en">

<head>
    @include('commons/head')
</head>

<body>
    <div class="preloader">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner.gif')?>" class="img-fluid">
        </div>
    </div>
    <div class="ajaxloader" style="display:none;">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner2.gif')?>" class="img-fluid"> Loading...
        </div>
    </div>

    @include('commons/topbar')
    @include('commons/sidebar')

    <div class="am-mainpanel">
        <div class="am-pagebody">
            @yield('breadcrumb')
            @yield('content')
        </div>
    </div>
    @yield('modal')
    @include('commons/js')
</body>
</html>
