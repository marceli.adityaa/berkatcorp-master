@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item">Kamus Data</a></li>
		<li class="breadcrumb-item"><a href="{{site_url('perusahaan')}}">Perusahaan</a></li>
		<li class="breadcrumb-item active">Divisi</li>
	</ol>
</nav>
@end

@section('content')
<div class="card">
	<div class="card-header card-header-default bg-brown">
		<h6 class="mg-b-0">Divisi Perusahaan</h6>
	</div>
	<div class="card-body">
		<div id="toolbar">
			<button class="btn btn-primary" data-toggle="modal" data-target="#m-form">+ tambah</button>
		</div>
		<table class="table table-striped table-bordered table-js" data-search="true" data-toolbar="#toolbar"
		data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true"
		data-url="{{site_url('api/internal/divisi/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
		<thead>
			<tr>
				<th class="text-center" data-formatter="formatNomor">No.</th>
				<th class="text-center" data-field="aksi">Aksi</th>
				<th data-field="nama">Nama</th>
				<th data-field="perusahaan">Perusahaan</th>
				<th data-field="deskripsi">Deskripsi</th>
			</tr>
		</thead>
	</table>
</div>
</div>
@end

@section('modal')
<div class="modal" tabindex="-1" role="dialog" id="m-form">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<span class="modal-title">Formulir Data Divisi</span>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#">
					<input type="hidden" name="id" value="">
					<div class="form-row">
						<div class="col-12 col-lg-6">
							<div class="form-group">
								<label>Nama Perusahaan (wajib diisi)</label>
								<input type="text" class="form-control form-control-lg tx-16" name="nama" placeholder="Nama Divisi" required="">
								<p class="form-text"></p>
							</div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="form-group">
								<label>Divisi Perusahaan</label>
								<select name="perusahaan_id" class="form-control form-control-lg tx-16">
									<option value="">- Pilih Perusahaan -</option>
									@foreach($perusahaan AS $p)
									<option value="{{$p->id}}">{{$p->nama}}</option>
									@endforeach
								</select>
								<p class="form-text"></p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Deskripsi</label>
						<textarea name="deskripsi" rows="3" class="form-control form-control-lg tx-16" placeholder="Deskripsi Devisi"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="save()"><i class="fa fa-save mr-2"></i>Simpan</button>
				<button class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
			</div>
		</div>
	</div>
</div>
@end

@section('js')
<script type="text/javascript">
	// VARS
	var url = "{{site_url('api/internal/divisi')}}";
	var token = "{{$this->session->auth['token']}}";
	
	// EVENTS
	$('#m-form').on('shown.bs.modal', function (e) {
		$("[name=nama]").focus();
	});

	// FUNCTINOS
	function save() {
		//Validate
		var valid = true;
		var att = ['nama','perusahaan_id'];
		$.each(att, function(i,v){
			if ($("[name="+v+"]").val() == '') {
				$("[name="+v+"]").addClass('is-invalid').focus().parent().find('.form-text').text('Data harus diisi.');
				valid = false;
			}
		});
		if (!valid) {
			return;
		}
		//Save
		var data = $('#m-form form').serializeArray();
		$.ajax({
			url:url+'/save/'+token,
			method:'POST',
			data:data,
			dataType:'json',
			success:function(result,status,xhr){
				if (result.status === 'success') {
					Toast.fire('Sukses!',result.message,'success');
					$('.modal').modal('hide');
					$('.table-js').bootstrapTable('refresh');
				} else {
					Toast.fire('Error!',result.error,'error');
				}
			},
			error:function(xhr,status,error){
				Toast.fire('Error!','Terjadi kesalahan pada sistem!','error');
			}
		});
	}

	function edit(id) {
		$.getJSON(url+'/get/'+token+'?id='+id, function(result){
			if (result) {
				$('[name=id]').val(result.id);
				$('[name=nama]').val(result.nama);
				$('[name=perusahaan_id] option[value='+result.perusahaan_id+']').prop('selected', true);
				$('[name=deskripsi]').val(result.deskripsi);
				$('#m-form').modal('show');
			}
		});
	}

	function disable(id, $isDisabled) {
		$.getJSON(url+'/disable/'+token+'?id='+id+'&par='+!$isDisabled,function(result){
			if(result.status == 'success'){
				Toast.fire('Sukses!', result.message,'success');
				$('.table-js').bootstrapTable('refresh');
			}
		});
	}
</script>
@end