@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('absensi')}}">Absensi</a></li>
        <li class="breadcrumb-item active">Data</li>
    </ol>
</nav>
@end

@section('content')
<table class="table table-striped table-bordered table-sm table-js" data-search="true" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/absensi/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
    <thead>
        <tr>
            <th class="text-center" data-formatter="formatNomor">No.</th>
            <th class="text-center" data-field="aksi">Aksi</th>
            <th data-field="nama">Nama</th>
            <th class="text-center tx-uppercase" data-field="kelompok" data-sortable="true">Kelompok</th>
            <th class="text-center" data-field="tanggal" data-formatter="formatDate" data-sortable="true">Tanggal</th>
            <th class="text-center tx-uppercase" data-field="kehadiran" data-sortable="true">Kehadiran</th>
            <th class="text-center" data-field="scanlog_masuk" data-formatter="formatTime" data-sortable="true">Scanlog Masuk</th>
            <th class="text-center" data-field="scanlog_pulang" data-formatter="formatTime" data-sortable="true">Scanlog Pulang</th>
            <th class="text-center" data-field="keterlambatan" data-formatter="formatTerlambat">Keterlambatan</th>
            <th data-field="keterangan">Keterangan</th>
        </tr>
    </thead>
</table>
@end

@section('modal')
<div id="mLog" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header"></div>
            <div class="modal-body">
                <table class="table table-stripped table-bordered"></table>
            </div>
            <div class="modal-footer"><button class="btn btn-block btn-sm btn-warning" data-dismiss="modal">tutup</button></div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
// FUNCTIONS
function getScanlog(pin, tanggal) {
    $("#mLog .modal-header").text("Scanlog Fingerprint Tanggal "+moment(tanggal).format('DD/MM/YYYY'));
    $.getJSON("http://local.berkatcorp.com/fingerprint/api/get_scanlog_detail?sn=61627018330950&date="+tanggal+"&pin="+pin, function(result){
        $("#mLog table").empty();
        html = "<tr><td>Tidak ada data</td></tr>";
        if($.isEmptyObject(result) == false) {
            html = '';
            $.each(result[pin], function(i,v){
                html += "<tr><td>"+v+"</td></tr>";
            });
        }
        $("#mLog table").html(html);
        $("#mLog").modal("show");
    });
}

function formatTerlambat(value,row,index,field) {
    return value > 0 ?value+' Menit':'-';
}
</script>
@end