@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Absensi</li>
    </ol>
</nav>
@end

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Persentase Kehadiran Bulan Ini</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget->total == 0 ? 100 : round(($widget->total-$widget->absen)/$widget->total * 100, 2)}}%</h3>
                <i class="fa fa-percent widget-icon"></i>
            </div>
        </div>
    </div>  
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Tidak Hadir</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget->absen}}</h3>
                <i class="fa fa-times-circle widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Rata-Rata Keterlambatan</span>
                <h3 class="mt-2 tx-white tx-bold">{{floor($widget->rata)}} Menit</h3>
                <i class="fa fa-clock-o widget-icon"></i>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body py-3 px-3">
        <a href="{{site_url('absensi/buku')}}" class="btn btn-primary mr-2"><i class="fa fa-book mr-2"></i>Buku Absensi</a>
        <a href="{{site_url('absensi/data')}}" class="btn btn-primary mr-2"><i class="fa fa-calendar mr-2"></i>Data Absensi</a>
        <!-- <a href="#" class="btn btn-primary"><i class="fa fa-file-o mr-2"></i>Laporan</a> -->
    </div>
</div>

<div class="card mt-3">
    <div class="card-body px-5 py-5">
        <span class="tx-bold tx-uppercase">Grafik Rata Keterlambatan 7 Hari Terakhir</span>
        <canvas id="chart2" width="100%" height="35px"></canvas>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body px-5 py-5">
        <span class="tx-bold tx-uppercase">Grafik Kehadiran 6 bulan terakhir</span>
        <canvas id="chart1" width="100%" height="35px"></canvas>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/chartjs/chart.min.css')}}">
<style>
    td {
        vertical-align: middle !important;
    }

    .widget-icon {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 7em;
    }
</style>
@end

@section('js')
<script src="{{base_url('assets/plugins/chartjs/chart.min.js')}}"></script>
<script>
    var myLineChart = new Chart($("#chart1"), {
        type: 'bar',
        data: {
            labels: "{{implode(',',$graph['bulan'])}}".split(','),
            datasets: [{
                label: 'Hadir',
                data: "{{implode(',',$graph['kehadiran']['hadir'])}}".split(','),
                backgroundColor: 'rgba(107, 186, 255,.4)',
                pointBackgroundColor: 'rgba(107, 186, 255)'
            },
            {
                label: 'Ijin',
                data: "{{implode(',',$graph['kehadiran']['ijin'])}}".split(','),
                backgroundColor: 'rgba(237, 201, 21,.4)',
                pointBackgroundColor: 'rgba(237, 201, 21)'
            },
            {
                label: 'Absen',
                data: "{{implode(',',$graph['kehadiran']['absen'])}}".split(','),
                backgroundColor: 'rgba(240, 22, 54,.4)',
                pointBackgroundColor: 'rgba(240, 22, 54)'
            }]
        }
    });
    var myLineChart = new Chart($("#chart2"), {
        type: 'line',
        data: {
            labels: "{{implode(',',$graph['hari'])}}".split(','),
            datasets: [{
                label: 'Rata-rata keterlambatan (menit)',
                data: "{{implode(',',$graph['keterlambatan'])}}".split(','),
                fill:'false',
                borderColor: 'rgba(107, 186, 255)',
                pointBackgroundColor: 'rgba(107, 186, 255)'
            }]
        }
    });
</script>
@end