@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('absensi')}}">Absensi</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('absensi/buku')}}">Buku</a></li>
        <li class="breadcrumb-item active">Baru</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">
        <h5>Tambah Baru</h5>
    </div>
    <div class="card-body">
        <form action="{{site_url('absensi/insert')}}" method="post">
            <div class="form-row">
                <div class="form-group col-12 col-lg-4">
                    <label>Tanggal</label>
                    <div class="input-group">
                        <input class="form-control text-left" type="date" name="tanggal" max="{{date('Y-m-d')}}" onchange="getPegawai(this)" required>
                    </div>
                </div>
                <div class="form-group col-12 col-lg-8 d-flex align-items-end justify-content-end">
                    <button class="btn btn-dark invisible" type="button" data-toggle="modal" data-target="#mDefault" title="Nilai Default"><i class="icon-settings"></i></button>
                    <div class="btn-group invisible bSync">
                        <button type="button" class="btn btn-dark" title="Sinkronasi fingerprint"><i class="fa fa-retweet"></i></button>
                        <!-- <button type="button" class="btn btn-outline-dark" onclick="helpScanlog()"><i class="fa fa-question-circle"></i></button> -->
                    </div>
                    <button type="button" class="btn btn btn-dark bScanlog invisible mr-2" title="Ambil scanlog fingerprint"><i class="fa fa-clock-o"></i></button>
                    <button class="btn btn-dark"><i class="fa fa-save mr-2"></i>Simpan</button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-stripped table-bordered" id="tPegawai">
                    <thead>
                        <tr>
                            <th class="tx-11 text-center" rowspan="2">No.</th>
                            <th class="tx-11 text-center" rowspan="2">Nama Pegawai</th>
                            <th class="tx-11 text-center" width="1%" rowspan="2">Kehadiran</th>
                            <th class="text-center" colspan="2">Jadwal Kerja</th>
                            <th class="text-center" colspan="2">Scan Fingerprint</th>
                            <th class="tx-11 text-center" width="1%" rowspan="2">Pengurangan<br>Overtime</th>
                            <th class="tx-11 text-center" rowspan="2">Keterangan</th>
                            <th class="text-center" colspan="2">Tabungan</th>

                        </tr>
                        <tr>
                            <th class="tx-11 text-center" width="1%">Masuk</th>
                            <th class="tx-11 text-center" width="1%">Pulang</th>
                            <th class="tx-11 text-center" width="1%">Masuk</th>
                            <th class="tx-11 text-center" width="1%">Pulang</th>
                            <th class="tx-11 text-center" width="10%">Tabungan</th>
                            <th class="tx-11 text-center">Kas</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </form>
    </div>
</div>
@end

@section('modal')
<div id="mDefault" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">Nilai Default</div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Kehadiran :</label>
                        <select class="form-control" id="iKehadiran">
                            <option value="">Tidak Berubah</option>
                            <option value="hadir">Hadir</option>
                            <option value="sakit">Sakit</option>
                            <option value="alpa">Alpa</option>
                            <option value="libur">Libur</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Jadwal Kantor :</label>
                        <div class="input-group">
                            <input type="text" class="form-control clockpicker" id="iJadwalM" placeholder="Jam Masuk" readonly>
                            <input type="text" class="form-control clockpicker" id="iJadwalP" placeholder="Jam Pulang" readonly>
                        </div>
                        <p class="form-text">* Kosongkan jika tidak berubah</p>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12 col-md-6">
                            <label for="">Pengurangan Overtime :</label>
                            <input type="number" class="form-control text-left" id="iKeterlambatan" placeholder="Pengurangan Overtime (menit)">
                            <p class="form-text">* Kosongkan jika tidak berubah</p>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="">Kas :</label>
                            <select id="iKas" class="form-control">{{$kasOption}}</select>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="">Terapkan Pada :</label>
                        <select id="iTerapkan" class="form-control">
                            <option value="semua">Semua</option>
                            @foreach($kelompok AS $k)
                            <option value="kel{{$k->id}}">{{ucwords($k->nama)}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mt-2">
                        <button class="btn btn-primary" type="button" onclick="setDefault()">Terapkan</button>
                        <button class="btn btn-warning" type="button" data-dismiss="modal">Batalkan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{site_url('assets/plugins/clockpicker/clockpicker.min.css')}}">
<style type="text/css">
    .table {
        overflow-x: scroll;
        width: 1200px;
    }

    td {
        padding: 0 !important;
    }

    td,
    th {
        white-space: nowrap;
        text-align: center;
        border: 1px solid white !important;
    }

    input {
        text-align: center;
    }

    .iJadwalM,
    .iJadwalP {
        color: teal;
    }

    .iScanlogM,
    .iScanlogP {
        color: indigo;
    }

    .iKeterlambatan,
    .iJadwalM,
    .iJadwalP,
    .iScanlogM,
    .iScanlogP {
        text-align: center;
        font-weight: bold;
    }
</style>
@end

@section('js')
<script src="{{site_url('assets/plugins/clockpicker/clockpicker.min.js')}}"></script>
<script type="text/javascript">
    var url = "{{site_url('api/internal/absensi')}}";
    var token = "{{$this->session->auth['token']}}";
    var kasOption = "{{$kasOption}}";

    // INIT ===================
    $(".clockpicker").clockpicker({
        donetext: 'Pilih',
        autoclose: true
    });
    $(document).ready(function() {
        $("#mDefault #iKas option[value='']").text("Tidak Berubah");
    });
    // EVENTS =================
    $("#tPegawai").on('change', '.iKehadiran', function(e) {
        if ($(e.currentTarget).val() == 'hadir') {
            $(e.currentTarget).parent().parent().find('.iKeterlambatan, .iTabungan').prop('readonly', false);
            $(e.currentTarget).parent().parent().find('.iKas').prop('disabled', false);
        } else {
            $(e.currentTarget).parent().parent().find('.iKeterlambatan, .iTabungan').prop('readonly', true);
            $(e.currentTarget).parent().parent().find('.iKas').prop('disabled', true);
        }
    }).on("change", ".cTabungan", function() {
        var is_checked = !$(this).is(':checked');
        $(this).parent().parent().parent().find('.iTabungan, .iKas').prop('disabled', is_checked);
    }).on("blur", ".iJadwalM, .iJadwalP", function(e) {
        hitungOvertime($(e.currentTarget).parent().parent());
    }).on("focus", ".iJadwalM, .iJadwalP", function(e) {
        $(e.currentTarget).clockpicker({
            donetext: 'Pilih',
            autoclose: true
        }).clockpicker('show');
    });

    // Scanlog
    $(".bScanlog").on("click", function(e) {
        // Ambil tanggal
        var tanggal = moment($("[name=tanggal").val()).format('YYYY-MM-DD');
        // Ambil scanlog
        $.getJSON("http://local.berkatcorp.com/fingerprint/api/get_scanlog?sn=61627018330950&date=" + tanggal, function(result) {
            // Cek apa data sudah ada
            if (Object.keys(result).length > 0) {
                // Cetak
                $(".baris").each(function() {
                    // Variabel
                    var pin = $(this).data('pin');
                    if (result[pin] !== undefined) {
                        $(this).find('.iScanlogM').val(result[pin].first_scan);
                        $(this).find('.iScanlogP').val(result[pin].last_scan);
                        hitungOvertime(this);
                    } else {
                        $(this).find('.iKehadiran option[value="alpa"]').prop('selected', true).trigger('change');
                    }
                });
                Toast.fire('Pesan', 'Scanlog berhasil diambil', 'success');
            } else {
                Swal.fire('Info', 'Data tidak ditemukan. Coba untuk menyinkronisai mesin fingerprint.', 'info');
            }
        });
    });

    $(".bSync").on("click", function() {
        $.getJSON("http://local.berkatcorp.com/fingerprint/api/download_scanlog?sn=61627018330950", function(result) {
            var status = (result.status == 'true') ? 'Berhasil' : 'Gagal';
            var type = (result.status == 'true') ? 'success' : 'error';
            Toast.fire(status, result.message, type);
        });
    });

    // FUNCTIONS ==============
    function getPegawai(e) {
        $("#tPegawai tbody").empty();
        var tanggal = $(e).val();
        $.getJSON(url + '/has_absensi/' + token + '?tanggal=' + $(e).val(), function(result) {
            if (result.jumlah > 0) {
                // Jika absensi sudah terisi
                Swal.fire('Error!', 'Tanggal tersebut telah terisi', 'error');
                $("[name=tanggal]").val('');
                return;
            } else {
                // Jika absensi belum
                $.ajax({
                    url: url + '/get_daftar_pegawai/' + token,
                    method: 'POST',
                    data: {
                        tanggal: tanggal
                    },
                    dataType: 'json',
                    success: function(result) {
                        var formIndex = 0;
                        var html = "";
                        $.each(result, function(key, kelompok) {
                            html += "<tr class='bg-dark'><td colspan='11' class='text-left text-white px-2 py-1'><b>" + key + "</b></td></tr>";
                            $.each(kelompok, function(index, pegawai) {
                                html += "<tr class='baris' data-pin='" + pegawai.fingerprint_id + "'>";
                                // Nomor
                                html += "<td>" + (index + 1) + "</td>";
                                // Nama
                                html += "<td><select name='data[" + formIndex + "][pegawai_id]' class='form-control'><option value='" + pegawai.id + "'>" + pegawai.nama + "</option></select></td>";
                                // Jika punya Cuti
                                if (pegawai.has_cuti) {
                                    // Kehadiran
                                    html += "<td><select name='data[" + formIndex + "][kehadiran]' class='form-control iKehadiran " + pegawai.kelompok + "'><option value='cuti'>Cuti</option></select>";
                                    // Jadwal
                                    html += "<td><input type='text' name='data[" + formIndex + "][jam_masuk]' class='time form-control iJadwalM " + pegawai.kelompok + "' value='" + pegawai.jam_masuk + "' readonly></td>";
                                    html += "<td><input type='text' name='data[" + formIndex + "][jam_pulang]' class='time form-control iJadwalP " + pegawai.kelompok + "' value='" + pegawai.jam_pulang + "' readonly></td>";
                                    // scanlog
                                    html += "<td><input type='text' class='form-control iScanlogM' name='data[" + formIndex + "][scanlog_masuk]' value='-' readonly></td>";
                                    html += "<td><input type='text' class='form-control iScanlogP' name='data[" + formIndex + "][scanlog_pulang]' value='-' readonly></td>";
                                    // Keterlambatan
                                    html += "<td><input type='number' name='data[" + formIndex + "][keterlambatan]' class='form-control iKeterlambatan " + pegawai.kelompok + "' min='0' placeholder='Menit' value='0' readonly></td>";
                                    // Keterangan
                                    html += "<td colspan='3'><input type='text' name='data[" + (formIndex) + "][keterangan]' class='text-left form-control' value='" + pegawai.keterangan + "' readonly></td>";
                                } else {
                                    // Kehadiran
                                    html += "<td><select name='data[" + formIndex + "][kehadiran]' class='form-control iKehadiran " + pegawai.kelompok + "'>";
                                    html += "<option value='hadir' selected>Hadir</option><option value='sakit'>Sakit</option><option value='alpa'>Alpa</option><option value='libur'>Libur</option>";
                                    html += "</select></td>";
                                    // Jadwal
                                    html += "<td><input type='text' name='data[" + formIndex + "][jam_masuk]' class='time form-control iJadwalM " + pegawai.kelompok + "' value='" + pegawai.jam_masuk + "' readonly></td>";
                                    html += "<td><input type='text' name='data[" + formIndex + "][jam_pulang]' class='time form-control iJadwalP " + pegawai.kelompok + "' value='" + pegawai.jam_pulang + "' readonly></td>";
                                    // scanlog
                                    html += "<td><input type='text' class='form-control iScanlogM' name='data[" + formIndex + "][scanlog_masuk]' value='-' readonly></td>";
                                    html += "<td><input type='text' class='form-control iScanlogP' name='data[" + formIndex + "][scanlog_pulang]' value='-' readonly></td>";
                                    // Keterlambatan
                                    html += "<td><input type='number' name='data[" + formIndex + "][keterlambatan]' class='form-control iKeterlambatan " + pegawai.kelompok + "' min='0' placeholder='Menit' value='0'></td>";
                                    // Tabungan
                                    if (pegawai.jenis_pegawai == 'harian') {
                                        // Keterangan
                                        html += "<td><input type='text' name='data[" + (formIndex) + "][keterangan]' class='text-left form-control' placeholder='Keterangan (opsional)'></td>";
                                        // Tabungan
                                        html += "<td><div class='input-group'><div class='input-group-prepend'><span class='input-group-text'><input type='checkbox' class='cTabungan' checked></span></div>";
                                        html += "<input type='number' name='data[" + formIndex + "][nominal_tabungan]' class='form-control iTabungan' min='0' placeholder='Nominal tabungan' value='" + pegawai.nominal_tabungan + "'></div></td>";
                                        // Kas
                                        html += "<td><select name='data[" + (formIndex) + "][kas_id]' class='form-control iKas'>" + kasOption + "</select></td>";
                                    } else {
                                        html += "<td colspan='3'><input type='text' name='data[" + (formIndex) + "][keterangan]' class='form-control' placeholder='Keterangan (opsional)'></td>";
                                    }
                                }
                                html += "</tr>";
                                formIndex++;
                            });
                        });
                        $("#tPegawai tbody").html(html);
                        $(".invisible").removeClass('invisible');
                        $("#tPegawai tbody .form-control").addClass('form-control-sm');
                    }
                });
            }
        });
    }

    function hitungOvertime(baris) {
        var scanM = $(baris).find(".iScanlogM").val();
        var scanP = $(baris).find(".iScanlogP").val();
        var selisih = 0;

        if (scanM != '-' && scanM != '') {
            jadwal = $(baris).find('.iJadwalM').val();
            diff = parseInt(moment(scanM, 'H:m').diff(moment(jadwal, 'H:m'), 'minutes'));
            diff = (diff < 0) ? 0 : diff;
            selisih += diff;
        }

        if (scanP != '-' && scanP != '') {
            jadwal = $(baris).find('.iJadwalP').val();
            diff = parseInt(moment(jadwal, 'H:m').diff(moment(scanP, 'H:m'), 'minutes'));
            diff = (diff < 0) ? 0 : diff;
            selisih += diff;
        }

        $(baris).find('.iKeterlambatan').val(selisih);
    }

    function helpScanlog() {
        Swal.fire('Info', 'Sinkronisasi Fingerprint akan mengambil data scanlog dari mesin fingerprint dan menyimpannya ke dalam database server.');
    }

    function setDefault() {
        var terap = $("#mDefault #iTerapkan").val();
        terap = (terap == 'semua') ? '' : '.' + terap;

        // Kehadiran
        var val = $("#mDefault #iKehadiran").val();
        if (val != '') {
            $(terap + ".iKehadiran option[value='" + val + "']").prop("selected", true).trigger('change');
        }

        // Jam Masuk
        var val = $("#mDefault #iJadwalM").val();
        if (val != '') {
            $(terap + ".iJadwalM").val(val).trigger('blur');
        }

        // Jam Pulang
        var val = $("#mDefault #iJadwalP").val();
        if (val != '') {
            $(terap + ".iJadwalP").val(val).trigger('blur');
        }

        // Overtime
        var val = $("#mDefault #iKeterlambatan").val();
        if (val != '') {
            $(terap + ".iKeterlambatan").val(val);
        }

        // Kas
        var val = $("#mDefault #iKas").val();
        if (val != '') {
            $(terap + ".iKas option[value='" + val + "']").prop("selected", true).trigger('change');
        }

        $("#mDefault").modal('hide');
    }
</script>
@end