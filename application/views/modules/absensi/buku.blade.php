@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('absensi')}}">Absensi</a></li>
        <li class="breadcrumb-item active">Buku</li>
    </ol>
</nav>
@end

@section('content')
<div id="toolbar">
    <a href="{{site_url('absensi/add')}}" target="_BLANK" class="btn btn-primary"><i class="fa fa-plus mr-2"></i>Absen baru</a>
    <button class="btn btn-dark bFilter" data-toggle="modal" data-target="#mFilter"><i class="fa fa-filter tx-danger mr-2"></i>Filter: <span class="indikator">mati</span></button>
    <button class="btn btn-warning" data-toggle="modal" data-target="#mPrint"><i class="fa fa-print"></i></button>
</div>
<table class="table table-js table-striped" data-pagination="true" data-toolbar="#toolbar"
data-pagination="true" data-side-pagination="server"
data-url="{{site_url('api/internal/absensi/get_absensi_group/'.$this->session->auth['token'])}}">
    <thead>
        <tr>
            <th class="text-center" data-formatter="formatNomor">No</th>
            <th class="text-center" data-field="aksi">Aksi</th>
            <th class="text-center" data-field="tanggal">Tanggal</th>
            <th class="text-center" data-formatter="formatAbsen">Total Pegawai / <span class="text-primary">Masuk</span> / <span class="text-danger">Absen</span></th>
            <th class="text-center" data-formatter="formatScanlog">Scanlog Kosong</th>
            <th class="text-right" data-field="tabungan">Total Tabungan</th>
        </tr>
    </thead>
</table>
@end

@section('modal')
<div class="modal fade modal-protected-on-close" id="mFilter" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><h5 class="modal-title">Filter</h5></div>
      <div class="modal-body">
          <form>
              <div class="form-group">
                  <label>Rentang Waktu</label>
                  <div class="input-group">
                      <input type="date" class="form-control" name="tanggal_from">
                      <div class="input-group-append">
                          <span class="input-group-text">s/d</span>
                      </div>
                      <input type="date" class="form-control" name="tanggal_to">
                  </div>
              </div>
              <div class="form-group">
                  <button type="button" class="btn btn-primary" onclick="setFilter(true)"><i class="fa fa-filter mr-2"></i>Filter</button>
                  <button type="button" class="btn btn-warning" onclick="setFilter(false)"><i class="fa fa-refresh mr-2"></i>Bersihkan</button>
              </div>
          </form>
      </div>
    </div>
  </div>
</div>

<div id="mPrint" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">Cetak Absensi</div>
            <div class="modal-body">
                <form action="{{site_url('absensi/cetak')}}" method="POST">
                    <div class="form-group">
                        <label>Rentang Waktu</label>
                        <div class="input-group">
                            <input class="form-control" type="date" name="tanggal_awal">
                            <div class="input-group-append"><span class="input-group-text">s/d</span></div>
                            <input class="form-control" type="date" name="tanggal_akhir">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Kolom Yang ditampilkan</label>
                        <select name="kolom[]" class="form-control select2" multiple>
                            <option value="kehadiran">Kehadiran</option>
                            <option value="jam_masuk">Jam Masuk</option>
                            <option value="jam_pulang">Jam Pulang</option>
                            <option value="scanlog_masuk">Scanlog Masuk</option>
                            <option value="scanlog_pulang">Scanlog Pulang</option>
                            <option value="keterlambatan">Keterlambatan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Cetak</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
    var url = "{{site_url('api/internal/absensi')}}";
    var token = "{{$this->session->auth['token']}}";

    // INIT ===================
    $(".select2").select2({
        dropdownParent: $('#mPrint'),
        placeholder:'Pilih Kolom'
    });
    // FUNCTIONS ==============
    function setFilter(status) {
        var obj = $("#mFilter form").serializeArray().filter(function(el) {
            return el.value != null && el.value != '';
        });
        if (status && obj.length > 0) {
            var par = $("#mFilter form").serialize();
            $(".table-js").bootstrapTable('refresh', {
                url:url+'/get_absensi_group/'+token+'?'+par
            });
            $(".bFilter .fa").removeClass('tx-danger').addClass('tx-warning');
            $('.indikator').text('hidup');
        } else {
            $(".table-js").bootstrapTable('refresh', {
                url:url+'/get_absensi_group/'+token
            });
            $(".bFilter .fa").removeClass('tx-warning').addClass('tx-danger');
            $('.indikator').text('mati');
        }

        $("#mFilter").modal('hide');
    }

    function formatAbsen(v,r,i,f) {
        var t = parseInt(r.jumlah_hadir) + parseInt(r.jumlah_absen);
        return '<b>'+t+' / <span class="text-primary">'+r.jumlah_hadir+'</span> / <span class="text-danger">'+r.jumlah_absen+'</span><b>';
    }

    function formatScanlog(v,r,i,f) {
        return parseInt(r.scanlog_masuk) + parseInt(r.scanlog_pulang);
    }
</script>
@end