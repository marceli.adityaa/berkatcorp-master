@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('absensi')}}">Absensi</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('absensi/buku')}}">Buku</a></li>
        <li class="breadcrumb-item active">Sunting</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">
        <h5>Sunting Data</h5>
    </div>
    <div class="card-body">
        <form action="{{site_url('absensi/update')}}" method="post">
            <div class="form-row">
                <div class="form-group col-12 col-lg-4">
                    <label>Tanggal</label>
                    <div class="input-group">
                        <input class="form-control text-left" type="date" name="tanggal" max="{{date('Y-m-d')}}" value="{{$tanggal}}" required readonly>
                    </div>
                </div>
                <div class="form-group col-12 col-lg-8 d-flex align-items-end justify-content-end">
                    <button type="button" class="btn btn btn-dark bScanlog mr-2"><i class="fa fa-clock-o mr-2"></i>Ambil Scanlog</button>
                    <button class="btn btn-dark"><i class="fa fa-save mr-2"></i>Simpan</button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-stripped table-bordered" id="tPegawai">
                    <thead>
                        <tr>
                            <th class="tx-11 text-center" rowspan="2">Aksi</th>
                            <th class="tx-11 text-center" rowspan="2">No.</th>
                            <th class="tx-11 text-center" rowspan="2">Nama Pegawai</th>
                            <th class="tx-11 text-center" width="1%" rowspan="2">Kehadiran</th>
                            <th class="text-center" colspan="2">Jadwal Kerja</th>
                            <th class="text-center" colspan="2">Scan Fingerprint</th>
                            <th class="tx-11 text-center" width="1%" rowspan="2">Pengurangan<br>Overtime</th>
                            <th class="tx-11 text-center" rowspan="2">Keterangan</th>
                            <th class="text-center" colspan="2">Tabungan</th>

                        </tr>
                        <tr>
                            <th class="tx-11 text-center" width="1%">Masuk</th>
                            <th class="tx-11 text-center" width="1%">Pulang</th>
                            <th class="tx-11 text-center" width="1%">Masuk</th>
                            <th class="tx-11 text-center" width="1%">Pulang</th>
                            <th class="tx-11 text-center" width="10%">Tabungan</th>
                            <th class="tx-11 text-center">Kas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $currentKel = ''; ?>
                        @foreach($absensi AS $index=>$ab)
                        @if($ab->kelompok !== $currentKel)
                        <tr>
                            <td colspan="12" class="bg-dark tx-bold text-left px-2 py-2" style="color:white">{{$ab->kelompok}}</td>
                        </tr>
                        <?php $currentKel = $ab->kelompok;
                        $no = 1; ?>
                        @endif
                        <tr class="baris" data-pin="{{$ab->fingerprint_id}}">
                            <td class="text-center" width="5%"><button type="button" class="btn btn-primary btn-sm btn-block" onclick="protection(this)"><i class="fa fa-lock"></i></button></td>
                            <td class="text-center">{{$no++}}</td>
                            <td class="text-center">
                                <select name="data[{{$index}}][pegawai_id]" class="form-control" disabled>
                                    <option value="{{$ab->pegawai_id}}">{{$ab->nama}}</option>
                                </select>
                            </td>
                            <td class="text-center">
                                <select name="data[{{$index}}][kehadiran]" class="form-control sKehadiran" disabled>
                                    <option value="alpa" {{$ab->kehadiran=='alpa'?'selected':''}}>Alpa</option>
                                    <option value="cuti" {{$ab->kehadiran=='cuti'?'selected':''}}>Cuti</option>
                                    <option value="hadir" {{$ab->kehadiran=='hadir'?'selected':''}}>Hadir</option>
                                    <option value="libur" {{$ab->kehadiran=='libur'?'selected':''}}>Libur</option>
                                    <option value="sakit" {{$ab->kehadiran=='sakit'?'selected':''}}>Sakit</option>
                                </select>
                            </td>
                            <td class="text-center"><input type="text" class="form-control iJadwalM" value="{{datify($ab->jam_masuk,'H:i')}}" readonly disabled></td>
                            <td class="text-center"><input type="text" class="form-control iJadwalP" value="{{datify($ab->jam_pulang,'H:i')}}" readonly disabled></td>
                            <td class="text-center"><input type="text" name="data[{{$index}}][scanlog_masuk]" class="form-control iScanlogM" value="{{$ab->scanlog_masuk?datify($ab->scanlog_masuk,'H:i'):'-'}}" readonly disabled></td>
                            <td class="text-center"><input type="text" name="data[{{$index}}][scanlog_pulang]" class="form-control iScanlogP" value="{{$ab->scanlog_pulang?datify($ab->scanlog_pulang,'H:i'):'-'}}" readonly disabled></td>
                            <td class="text-center"><input type="number" min="0" name="data[{{$index}}][keterlambatan]" class="form-control iKeterlambatan" value="{{$ab->keterlambatan}}" {{$ab->kehadiran!='hadir'?'readonly':''}} disabled></td>
                            @if($ab->jenis_pegawai=='harian')
                            <td class="text-center"><input type="text" name="data[{{$index}}][keterangan]" class="form-control text-left" value="{{$ab->keterangan}}" placeholder="Keterangan" disabled></td>
                            <td class="text-center"><input type="number" min="0" name="data[{{$index}}][nominal_tabungan]" class="form-control iTabungan" value="{{empty($ab->nominal)?0:$ab->nominal}}" {{$ab->kehadiran!='hadir'?'readonly':''}} disabled></td>
                            <td class="text-center">
                                <select name="data[{{$index}}][kas_id]" class="form-control iKas" disabled>
                                    <option value="">Pilih Kas</option>
                                    @foreach($kas AS $k)
                                    <option value="{{$k->id}}" {{$k->id == $ab->kas_id?'selected':''}}>{{$k->label}}</option>
                                    @endforeach
                                </select>
                            </td>
                            @else
                            <td colspan="2" class="text-center"><input type="text" name="data[{{$index}}][keterangan]" class="form-control text-left" value="{{$ab->keterangan}}" placeholder="Keterangan" disabled></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{site_url('assets/plugins/clockpicker/clockpicker.min.css')}}">
<style type="text/css">
    .table {
        overflow-x: scroll;
        width: 1200px;
    }

    td {
        padding: 0 !important;
    }

    td,
    th {
        white-space: nowrap;
        text-align: center;
        border: 1px solid white !important;
    }

    input {
        text-align: center;
    }

    .iJadwalM,
    .iJadwalP {
        color: teal;
    }

    .iScanlogM,
    .iScanlogP {
        color: indigo;
    }

    .iKeterlambatan,
    .iJadwalM,
    .iJadwalP,
    .iScanlogM,
    .iScanlogP {
        text-align: center;
        font-weight: bold;
    }
</style>
@end

@section('js')
<script src="{{site_url('assets/plugins/clockpicker/clockpicker.min.js')}}"></script>
<script type="text/javascript">
    var url = "{{site_url('api/internal/absensi')}}";
    var token = "{{$this->session->auth['token']}}";

    // INIT ===================
    // EVENTS =================
    $("#tPegawai").on('change', '.sKehadiran', function(e) {
        if ($(e.currentTarget).val() == 'hadir') {
            $(e.currentTarget).parent().parent().find('.iKeterlambatan').prop('readonly', false);
            $(e.currentTarget).parent().parent().find('.iTabungan').prop('readonly', false);
            $(e.currentTarget).parent().parent().find('.iKas').prop('disabled', false);
        } else {
            $(e.currentTarget).parent().parent().find('.iKeterlambatan').prop('readonly', true);
            $(e.currentTarget).parent().parent().find('.iTabungan').prop('readonly', true);
            $(e.currentTarget).parent().parent().find('.iKas').prop('disabled', true);
        }
    }).on("blur", ".iJadwalM, .iJadwalP", function(e) {
        hitungOvertime($(e.currentTarget).parent().parent());
    }).on("focus", ".iJadwalM, .iJadwalP", function(e) {
        $(e.currentTarget).clockpicker({
            donetext: 'Pilih',
            autoclose: true
        }).clockpicker('show');
    });

    // Scanlog
    $(".bScanlog").on("click", function(e) {
        // Ambil tanggal
        var tanggal = moment($("[name=tanggal").val()).format('YYYY-MM-DD');
        // Ambil scanlog
        $.getJSON("http://local.berkatcorp.com/fingerprint/api/get_scanlog?sn=61627018330950&date=" + tanggal, function(result) {
            // Cek apa data sudah ada
            if (Object.keys(result).length > 0) {
                // Cetak
                $(".baris").each(function() {
                    // Variabel
                    var pin = $(this).data('pin');
                    if (result[pin] !== undefined) {
                        $(this).find('.iScanlogM').val(result[pin].first_scan);
                        $(this).find('.iScanlogP').val(result[pin].last_scan);
                        hitungOvertime(this);
                    } else {
                        $(this).find('.iKehadiran option[value="alpa"]').prop('selected', true).trigger('change');
                    }
                });
                Toast.fire('Pesan', 'Scanlog berhasil diambil', 'success');
            } else {
                Swal.fire('Info', 'Data tidak ditemukan. Coba untuk menyinkronisai mesin fingerprint.', 'info');
            }
        });
    });

    // FUNCTIONS ==============
    function protection(el) {
        if ($(el).hasClass('btn-primary')) {
            $(el).removeClass('btn-primary').addClass('btn-danger').find('i').removeClass('fa-lock').addClass('fa-unlock');
            $(el).parent().parent().find('input, select').prop('disabled', false);
            if ($(el).parent().parent().find('.sKehadiran').val() == 'cuti') {
                $(el).parent().parent().find('.iKas').prop('disabled', true);
            }
        } else {
            $(el).removeClass('btn-danger').addClass('btn-primary').find('i').removeClass('fa-unlock').addClass('fa-lock');
            $(el).parent().parent().find('input, select').prop('disabled', true);
        }
    }

    function hitungOvertime(baris) {
        var scanM = $(baris).find(".iScanlogM").val();
        var scanP = $(baris).find(".iScanlogP").val();
        var selisih = 0;

        if (scanM != '-' && scanM != '') {
            jadwal = $(baris).find('.iJadwalM').val();
            diff = parseInt(moment(scanM, 'H:m').diff(moment(jadwal, 'H:m'), 'minutes'));
            diff = diff < 0 ? 0 : diff;
            selisih += diff;
        }

        if (scanP != '-' && scanP != '') {
            jadwal = $(baris).find('.iJadwalP').val();
            diff = parseInt(moment(jadwal, 'H:m').diff(moment(scanP, 'H:m'), 'minutes'));
            diff = diff < 0 ? 0 : diff;
            selisih += diff;
        }

        $(baris).find('.iKeterlambatan').val(selisih);
    }
</script>
@end