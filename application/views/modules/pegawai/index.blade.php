@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item active">Pegawai</li>
	</ol>
</nav>
@end

@section('content')
<div class="card">
	<div class="card-header card-header-default bg-brown">
		<h6 class="mg-b-0">Pegawai</h6>
	</div>
	<div class="card-body">
		<div id="toolbar">
			<button class="btn btn-primary" data-toggle="modal" data-target="#m-form"><i class="fa fa-plus mr-2"></i>Tambah Pegawai</button>
		</div>
		<table class="table table-striped table-bordered table-js" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/pegawai/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
			<thead>
				<tr>
					<th class="text-center" data-formatter="formatNomor">No.</th>
					<th class="text-center" data-field="aksi">Aksi</th>
					<th data-field="nama">Nama</th>
					<th class="text-center" data-field="jenis_kelamin">Jenis Kelamin</th>
					<th data-field="jabatan">Jabatan</th>
					<th data-field="divisi">Divisi</th>
					<th data-field="kelompok">Kelompok Kerja</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
@end

@section('modal')
<div class="modal" tabindex="-1" role="dialog" id="m-form">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<!-- <div class="modal-header">
				<span class="modal-title">Formulir Data Jabatan</span>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> -->
			<div class="modal-body">
				<form action="#">
					<input type="hidden" name="id">
					<div class="form-row">
						<div class="form-group col-12 col-lg-4">
							<label>Nama lengkap</label>
							<input type="text" class="form-control tx-16" name="nama" placeholder="Nama Lengkap" required="">
							<p class="form-text"></p>
						</div>
						<div class="form-group col-12 col-lg-4">
							<label>NIK</label>
							<input type="text" class="form-control tx-16" name="nik" placeholder="Nomor Induk Kependudukan">
						</div>
						<div class="form-group col-12 col-lg-4">
							<label>Fingerprint Pin</label>
							<input type="number" class="form-control tx-16" name="fingerprint_id" placeholder="Fingerprint Pin">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12 col-md-6 col-lg-3">
							<label>Tempat Lahir</label>
							<input type="text" class="form-control tx-16" name="tempat_lahir" placeholder="Tempat Lahir">
						</div>
						<div class="form-group col-12 col-md-6 col-lg-3">
							<label>Tanggal Lahir</label>
							<input type="date" class="form-control tx-16" name="tanggal_lahir" placeholder="Tanggal Lahir">
						</div>
						<div class="form-group col-12 col-lg-6">
							<label>Alamat</label>
							<input type="text" class="form-control tx-16" name="alamat" placeholder="Alamat Tinggal">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12 col-md-6 col-lg-4">
							<label>Jenis Kelamin</label>
							<select name="jenis_kelamin" class="form-control">
								<option value="L">Laki-laki</option>
								<option value="P">Perempuan</option>
							</select>
						</div>
						<div class="form-group col-12 col-md-6 col-lg-4">
							<label>Agama</label>
							<select name="agama" class="form-control">
								<option value="tidak_diisi">Tidak Diisi</option>
								<option value="islam">Islam</option>
								<option value="katolik">Katolik</option>
								<option value="protestan">Protestan</option>
								<option value="hindu">Hindu</option>
								<option value="budha">Budha</option>
								<option value="konghucu">Konghucu</option>
							</select>
						</div>
						<div class="form-group col-12 col-md-6 col-lg-4">
							<label>Status Kawin</label>
							<select name="status_kawin" class="form-control">
								<option value="">- Tidak disi -</option>
								<option value="belum_menikah">Belum Menikah</option>
								<option value="menikah">Menikah</option>
								<option value="duda">Duda</option>
								<option value="janda">Janda</option>
							</select>
						</div>
					</div>
					<hr>
					<div class="form-row">
						<div class="form-group col-12 col-md-6 col-lg-3">
							<label>Perusahaan</label>
							<select id="s-perusahaan" class="form-control">
								<option value="">- Pilih Perusahaan -</option>
								@foreach($perusahaan AS $p)
								<option value="{{$p->id}}">{{$p->nama}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-12 col-md-6 col-lg-3">
							<label>Divisi</label>
							<select name="divisi_id" class="form-control" required="">
								<option value="">- Pilih Divisi -</option>
							</select>
							<p class="form-text"></p>
						</div>
						<div class="form-group col-12 col-md-6 col-lg-3">
							<label>Jabatan</label>
							<select name="jabatan_id" class="form-control" required="">
								<option value="">- Pilih Jabatan -</option>
								@foreach($jabatan AS $j)
								<option value="{{$j->id}}">{{$j->nama}}</option>
								@endforeach
							</select>
							<p class="form-text"></p>
						</div>
						<div class="form-group col-12 col-md-6 col-lg-3">
							<label>Kelompok</label>
							<select name="kelompok_id" class="form-control" required="">
								<option value="">- Pilih Kelompok -</option>
								@foreach($kelompok AS $k)
								<option value="{{$k->id}}">{{$k->nama}}</option>
								@endforeach
							</select>
							<p class="form-text"></p>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12 col-md-6 col-lg-4">
							<label>Jenis Pegawai</label>
							<select name="jenis_pegawai" class="form-control">
								<option value="">- Pilih Jenis Pegawai</option>
								<option value="harian">Harian</option>
								<option value="mingguan">Mingguan</option>
								<option value="bulanan">Bulanan</option>
							</select>
						</div>
						<div class="form-group col-12 col-md-6 col-lg-4">
							<label>Gaji</label>
							<div class="input-group">
								<div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
								<input type="number" class="form-control tx-16" name="gaji" placeholder="Gaji pegawai">
							</div>
						</div>
						<div class="form-group col-12 col-md-6 col-lg-4">
							<label>Nominal Tabungan</label>
							<div class="input-group">
								<div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
								<input type="number" class="form-control tx-16" name="nominal_tabungan" value="0" placeholder="Nominal Tabungan" readonly>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12 col-lg-8">
							<label>Jam Kerja</label>
							<select name="jamker_id" class="form-control">
								<option value="">- Pilih Jam Kerja</option>
								@foreach($jamker AS $in=>$j)
								<option value="{{$j->id}}" data-jam="{{datify($j->jam_masuk,'H:i').' - '.datify($j->jam_pulang,'H:i')}}" data-hari='{{$j->hari}}'>{{ucwords($j->nama)}}</option>
								@endforeach
							</select>
							<p class="form-text"></p>
						</div>
						<div class="form-group col-12 col-lg-4">
							<label>Tanggal Masuk</label>
							<input type="date" name="tanggal_masuk" class="form-control" value="{{date('Y-m-d')}}" placeholder="Tanggal Mulai Kerja">
							<p class="form-text"></p>
						</div>
					</div>
				</form>
				<div class="modal-footer">
					<button class="btn btn-primary" onclick="save()"><i class="fa fa-save mr-2"></i>Simpan</button>
					<button class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
				</div>
			</div>
		</div>
	</div>
</div>
@end

@section('js')
<script type="text/javascript">
	var url = "{{site_url('api/internal/pegawai')}}";
	var token = "{{$this->session->auth['token']}}";
	// EVENTS
	$('#m-form').on('shown.bs.modal', function(e) {
		$("[name=nama]").focus();
	}).on('hide.bs.modal', function(e) {
		$('[name=divisi_id]').empty().append('<option value="">- Pilih Divisi -</option>');
		$(".form-text").empty();
	});

	$('#s-perusahaan').on('change', function() {
		var id = $(this).val();
		setDivisi(id);
	});

	$("[name=jenis_pegawai]").on("change", function() {
		if ($(this).val() == 'harian') {
			$("[name=nominal_tabungan]").prop('readonly', false);
		} else {
			$('[name=nominal_tabungan]').val('0');
			$("[name=nominal_tabungan]").prop('readonly', true);
		}
	});

	$("[name=jamker_id]").on("change", function(e) {
		var el = $(this).find("option:selected");
		$(this).parent().find(".form-text").text('Keterangan: ' + el.data('jam') + ' ~ ' + el.data('hari'))
	});

	// FUNCTIONS
	function save() {
		//Validate
		var valid = true;
		$('[required]').each(function() {
			if (!$(this).val()) {
				$(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.');
				valid = false;
			} else {
				$(this).removeClass('is-invalid').parent().find('.form-text').text('');
			}
		});
		if (!valid) {
			return;
		}
		//Save
		var data = $('#m-form form').serializeArray();
		$.ajax({
			url: url + '/save/' + token,
			method: 'POST',
			data: data,
			dataType: 'json',
			success: function(result, status, xhr) {
				if (result.status === 'success') {
					Toast.fire('Sukses!', result.message, 'success');
					$('.modal').modal('hide');
					$('.table-js').bootstrapTable('refresh');
				} else {
					Toast.fire('Error!', result.error, 'error');
				}
			},
			error: function(xhr, status, error) {
				Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
			}
		});
	}

	function edit(id) {
		$.getJSON(url + '/get/' + token + '?callback=callback_get&id=' + id, function(r) {
			if (r) {
				$('[name=id]').val(r.id).focus();
				$('[name=nama]').val(r.nama).focus();
				$('[name=nik]').val(r.nik);
				$('[name=fingerprint_id]').val(r.fingerprint_id);
				$('[name=tempat_lahir]').val(r.tempat_lahir);
				$('[name=tanggal_lahir]').val(r.tanggal_lahir);
				$('[name=jenis_kelamin] option[value="' + r.jenis_kelamin + '"]').prop('selected', true);
				$('[name=agama] option[value="' + r.agama + '"]').prop('selected', true);
				$('[name=alamat]').val(r.alamat);
				$('[name=status_kawin] option[value="' + r.status_kawin + '"]').prop('selected', true);
				$('[name=gaji]').val(r.gaji);
				$('[name=tanggal_masuk]').val(r.tanggal_masuk);
				$('[name=jabatan_id] option[value="' + r.jabatan_id + '"]').prop('selected', true);
				$('[name=kelompok_id] option[value="' + r.kelompok_id + '"]').prop('selected', true);
				$('[name=jamker_id] option[value="' + r.jamker_id + '"]').prop('selected', true);
				$('[name=jenis_pegawai] option[value="' + r.jenis_pegawai + '"]').prop('selected', true);
				if (r.jenis_pegawai == 'harian') {
					$('[name=nominal_tabungan]').val(r.nominal_tabungan);
					$("[name=nominal_tabungan]").prop('readonly', false);
				} else {
					$('[name=nominal_tabungan]').val('0');
					$("[name=nominal_tabungan]").prop('readonly', true);
				}

				// hadehh
				$('#s-perusahaan option[value="' + r.perusahaan_id + '"]').prop('selected', true);
				$.getJSON("{{site_url('api/internal/divisi/get_many/'.$this->session->auth['token'].'?perusahaan_id=')}}" + r.perusahaan_id, function(result) {
					$('[name=divisi_id]').empty().append('<option value="">- Pilih Divisi -</option>');
					$.each(result, function(i, v) {
						$('[name=divisi_id]').append('<option value="' + v.id + '">' + v.nama + '</option>');
					});
					$('[name=divisi_id] option[value="' + r.divisi_id + '"]').prop('selected', true);
				});
				$('#m-form').modal('show');
			}
		});
	}

	// Get Divisi
	function setDivisi(id) {
		$.getJSON("{{site_url('api/internal/divisi/get_many/'.$this->session->auth['token'].'?is_disabled=0&perusahaan_id=')}}" + id, function(result) {
			$('[name=divisi_id]').empty().append('<option value="">- Pilih Divisi -</option>');
			$.each(result, function(i, v) {
				$('[name=divisi_id]').append('<option value="' + v.id + '">' + v.nama + '</option>');
			});
		});
	}

	function disable(id, $isDisabled) {
		$.getJSON(url + '/disable/' + token + '?id=' + id + '&par=' + !$isDisabled, function(result) {
			if (result.status == 'success') {
				Toast.fire('Sukses!', result.message, 'success');
				$('.table-js').bootstrapTable('refresh');
			}
		});
	}
</script>
@end