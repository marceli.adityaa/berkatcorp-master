@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('user')}}">User</a></li>
        <li class="breadcrumb-item active">Sunting User</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">Formulir Isian User</div>
    <div class="card-body">
        <form action="#">
            <input type="hidden" name="id" value="{{$user->id}}">
            <div class="form-group" id="dPegawai">
                <label for="">Pegawai</label>
                <select name="pegawai_id" class="form-control select2-pegawai" required>
                    <option value="{{$user->pegawai_id}}">{{$user->nama}}</option>
                </select>
                <p class="form-text"></p>
            </div>
            <div class="form-group">
                <label>Username</label>
                <div class="input-group">
                    <input class="form-control" type="text" name="username" data-default="{{$user->username}}" value="{{$user->username}}" placeholder="Masukkan username">
                    <div class="input-group-append">
                        <div class="input-group-text"><i class="fa fa-minus text-secondary"></i></div>
                    </div>
                </div>
                <p class="form-text">* Username <b>TIDAK BOLEH</b> mengandung spasi. Huruf besar-kecil tidak berpengaruh.</p>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-md-6">
                    <label>Kata Sandi</label>
                    <input class="form-control" type="password" name="password" placeholder="Masukkan Kata Sandi">
                    <p class="form-text">* Kosongkan jika password tidak berubah.</p>
                </div>
                <div class="form-group col-12 col-md-6">
                    <label>Tulis Ulang Kata Sandi</label>
                    <input class="form-control" type="password" name="password_re" placeholder="Tulis Ulang Kata Sandi">
                </div>
            </div>
            <div class="form-group">
                <label>Hak Akses</label>
                <table class="table table-bordered table-light">
                    <tbody>
                        @foreach($menu_data AS $app)
                        <?php $hak = isset($user->hak_akses->{$app->uuid}) && !empty($user->hak_akses->{$app->uuid})?$user->hak_akses->{$app->uuid}:null; ?>
                        <tr class="bg-dark tx-white">
                            <td class="fit"><input type="checkbox" class="app{{$app->uuid}}" data-child=".child{{$app->uuid}}" {{$hak?'checked':''}}></td>
                            <td class="fit"><i class="fa fa-cog"></i></td>
                            <td style="color:#fff !important"><b>{{strtoupper($app->label)}}</b></td>
                            <td class="tx-right" colspan="2">
                                <select class="form-control" name="role_akses[{{$app->uuid}}][]"> 
                                    <option value="">- Pilih Role -</option>
                                    @foreach($role as $rl)
                                        @if(json_decode($user->role)->{$app->uuid}[0] == $rl)
                                            <option value="{{$rl}}" selected>{{strtoupper($rl)}}</option>
                                        @else 
                                            <option value="{{$rl}}">{{strtoupper($rl)}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                
                            </td>
                        </tr>
                        <?php $counter = 0; ?>
                        @if($app->menu)
                            @foreach($app->menu AS $index=>$m)
                            <tr>
                                <td class="fit"><input type="checkbox" name="hak_akses[{{$app->uuid}}][]" class="parent{{$app->uuid}}{{$m->uuid}} child{{$app->uuid}}" data-child=".child{{$app->uuid}}{{$m->uuid}}" data-app=".app{{$app->uuid}}" value="{{$m->uuid}}" {{$hak && in_array($m->uuid, (Array)$hak)?'checked':''}}></td>
                                <td class="fit"><i class="icon {{$m->icon}}"></i></td>
                                <td>{{ucwords($m->label)}}</td>
                                <td>{{empty($m->deskripsi)?'<i>Tidak ada deskripsi</i>':$m->deskripsi}}</td>
                                <td class="fit">
                                    @if(isset($m->child))
                                    <i class="fa fa-angle-down tx-bold" data-toggle="mcollapse" data-target=".{{$app->uuid}}m{{$m->uuid}}"></i>
                                    @endif
                                </td>
                            </tr>
                            @if(isset($m->child))
                            @foreach($m->child AS $mc)
                            <tr class="bg-gray mcollapse {{$app->uuid}}m{{$m->uuid}}">
                                <td class="fit"><input type="checkbox" name="hak_akses[{{$app->uuid}}][]" class="child{{$app->uuid}} child{{$app->uuid}}{{$m->uuid}}" data-parent=".parent{{$app->uuid}}{{$m->uuid}}" data-app=".app{{$app->uuid}}" value="{{$mc->uuid}}" {{$hak && in_array($mc->uuid, (Array)$hak)?'checked':''}}></td>
                                <td class="fit"><i class="icon {{$mc->icon}}"></i></td>
                                <td>{{ucwords($mc->label)}}</td>
                                <td colspan="2">{{empty($mc->deskripsi)?'<i>Tidak ada deskripsi</i>':$m->deskripsi}}</td>
                            </tr>
                            @endforeach
                            @endif
                            @endforeach
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="button" onclick="save()"><i class="fa fa-save mr-2"></i>Simpan</button>
            </div>
        </form>
    </div>
</div>
@end

@section('style')
<style>
    .table td.fit,
    .table th.fit {
        white-space: nowrap;
        width: 1%;
        text-align: center;
    }

    .bg-gray {
        background-color: #e0e0e0;
    }

    [data-toggle=mcollapse] {
        cursor: pointer;
    }
</style>
@end

@section('js')
<script>
    var url = "{{site_url('api/internal/user')}}";
    var token = "{{$this->session->auth['token']}}";
    // INIT
    $(document).ready(function() {
        $(".mcollapse").hide();
    });
    $("[data-toggle=mcollapse]").on('click', function() {
        target = $(this).data('target');
        $(target).fadeToggle('fast');
        if ($(this).hasClass('fa-angle-down')) {
            $(this).removeClass('fa-angle-down').addClass('fa-angle-up');
        } else {
            $(this).removeClass('fa-angle-up').addClass('fa-angle-down');
        }
    });
    $(".select2-pegawai").select2({
        placeholder: "Pilih Pegawai",
        minimumInputLength: 3,
        ajax: {
            url: "{{site_url('api/internal/pegawai/get_select2_data/'.$this->session->auth['token'])}}",
            dataType: "json",
            delay: 600
        }
    });
    // EVENTS
    $("[name=pegawai_id]").on("change", function(e) {
        $.getJSON(url + '/get_by/' + token + '?pegawai_id=' + $(this).val(), function(result) {
            if (result != null) {
                $("#mPegawai").text('Pegawai ini sudah memiliki akun.');
                $("#bSimpan").prop('disabled', true);
            } else {
                $("#mPegawai").empty();
                $("#bSimpan").prop('disabled', false);
            }
        });
    });

    $("[name=username]").on("blur", function(e) {
        var valNow = $(this).val();
        var valDefault = $(this).data('default');

        if (valNow != '' && valNow != valDefault) {
            if (/\s/.test(valNow)) {
                $("[name=username]").parent().find('.input-group-text').text('Username mengandung spasi');
                $("#bSimpan").prop('disabled', true);
            } else {
                $.getJSON(url + '/get_by/' + token + '?username=' + valNow, function(result) {
                    if (result != null) {
                        $("[name=username]").parent().find('.input-group-text').text('Username tidak tersedia');
                        $("#bSimpan").prop('disabled', true);
                    } else {
                        $("[name=username]").parent().find('.input-group-text').html('<i class="fa fa-check text-success"></i>');;
                        $("#bSimpan").prop('disabled', false);
                    }
                });
            }
        }
    });

    $("[type=password]").on("blur", function(e) {
        var pass = $("[name=password]").val();
        var repass = $("[name=password_re]").val();
        if (pass !== repass) {
            $("[type=password]").addClass("is-invalid");
            $("#bSimpan").prop('disabled', true);
        } else {
            $("[type=password]").removeClass("is-invalid");
            $("#bSimpan").prop('disabled', false);
        }
    });

    $("[data-app]").on("click change", function(e) {
        if ($(e.currentTarget).is(":checked")) {
            $($(e.currentTarget).data('app')).prop('checked', true);
        } else {
            var counter = 0;
            $("[data-app]").each(function() {
                if ($(this).is(":checked"))
                    counter++;
            });

            if (counter == 0)
                $($(e.currentTarget).data('app')).prop('checked', false);
        }
    });

    $("[data-parent]").on("click change", function(e) {
        if ($(e.currentTarget).is(":checked")) {
            $($(e.currentTarget).data('parent')).prop('checked', true);
        } else {
            var counter = 0;
            $("[data-parent]").each(function() {
                if ($(this).is(":checked"))
                    counter++;
            });

            if (counter == 0)
                $($(e.currentTarget).data('parent')).prop('checked', false);
        }
    });

    $("[data-child]").on("click", function(e){
        $($(this).data('child')).prop('checked', $(this).is(":checked"));
    });

    // FUNCTIONS
    var url = "{{site_url('api/internal/user')}}";
    var token = "{{$this->session->auth['token']}}";
    function save() {
        //Validate
        var valid = true;
        var att = ['pegawai_id', 'username'];
        $.each(att, function(i, v) {
            if ($("[name=" + v + "]").val() == '') {
                $("[name=" + v + "]").addClass('is-invalid').focus().parent().find('.form-text').text('Data harus diisi.');
                valid = false;
            }
        });
        if (!valid) {
            return;
        }
        //Save
        var data = $('form').serializeArray();
        $.ajax({
            url: url + '/save/' + token,
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', result.message, 'success');
                } else {
                    Toast.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }
</script>
@end