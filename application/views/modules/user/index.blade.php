@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">User</li>
    </ol>
</nav>
@end

@section('content')
<div id="toolbar"><a href="{{site_url('User/add')}}" class="btn btn-primary"><i class="fa fa-plus mr-2"></i>Tambah Baru</a></div>
<div class="card">
    <div class="card-header">Daftar Pengguna</div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-js" data-toolbar="#toolbar" data-search="true" data-pagination="true" data-serach-on-enter="true" data-side-pagination="server" data-url="{{site_url('api/internal/user/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
            <thead>
                <tr>
                    <th data-formatter="formatNomor" class="text-center">No.</th>
                    <th data-field="aksi" class="text-left">Aksi</th>
                    <th data-field="username" class="text-left">Username</th>
                    <th data-field="nama" class="text-left">Nama Pegawai</th>
                    <th data-field="last_login" class="text-center">Terakhir Masuk</th>
                    <th data-field="insert_time" class="text-center">Waktu Entri</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('js')
<script>
    var url = "{{site_url('api/internal/user')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS
    function disable(id, $isDisabled) {
        $.getJSON(url + '/disable/' + token + '?id=' + id + '&par=' + !$isDisabled, function(result) {
            if (result.status == 'success') {
                Toast.fire('Sukses!', result.message, 'success');
                $('.table-js').bootstrapTable('refresh');
            }
        });
    }
</script>
@end