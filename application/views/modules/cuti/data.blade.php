@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{site_url('cuti')}}">Cuti</a></li>
		<li class="breadcrumb-item active">Manajemen Data</li>
	</ol>
</nav>
@end

@section('content')
<!-- TABEL DISINI -->
<div class="card">
	<div class="card-header card-header-default justify-content-between bg-brown">
		<h6 class="mg-b-0">Cuti Pegawai</h6>
		<button class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#cForm">+ Formulir Isian</button>
	</div>
	<!-- FORMULIR -->
	<div class="collapse" id="cForm">
		<div class="card-body bg-gray-200" style="border-bottom: solid 1px black">
			<h5><i class="fa fa-plane mr-2"></i><span class="label-form"></span> Cuti</h5>
			<hr>
			<form action="#">
				<input type="hidden" name="id" value="">
				<!-- 01 -->
				<input type="hidden" name="id" value="">
				<div class="form-row">
					<!-- 02 -->
					<div class="form-group col-12 col-lg-8">
						<label for="">Pegawai</label>
						<select name="pegawai_id[]" class="form-control select2-pegawai" required></select>
						<p class="form-text"></p>
					</div>
					<!-- 03 -->
					<div class="form-group col-12 col-lg-4">
						<label for="">Kategori Cuti</label>
						<select name="kategori" class="form-control">
							<!-- <option value="saldo">Redeem Saldo</option> -->
							<option value="">- Pilih Kategori Cuti -</option>
							<option value="ijin">Ijin</option>
							<option value="sakit">Sakit</option>
							<option value="lain" selected>Lain-lain</option>
						</select>
					</div>
				</div>

				<div class="form-row">
					<!-- 05 -->
					<div class="form-group col-12 col-lg-4">
						<label for="">Tanggal Cuti</label>
						<input type="date" class="form-control" name="tanggal_cuti" placeholder="Tanggal Cuti" required>
						<p class="form-text"></p>
					</div>
					<!-- 06 -->
					<div class="form-group col-12 col-lg-8">
						<label for="">Keterangan (opsional)</label>
						<input type="text" class="form-control" name="keterangan" placeholder="Keterangan Overtime">
						<p class="form-text"></p>
					</div>
				</div>
				<!-- 07-->
				<div class="form-group d-flex">
					<div class="input-group">
						<button class="btn btn-primary" id="#bSimpan" type="button" onclick="save()"><i class="fa fa-save mr-2"></i>Simpan Data</button>
						<button class="btn btn-warning" type="reset"><i class="fa fa-refresh mr-2"></i>Bersihkan</button>
					</div>
					<button class="btn btn-secondary float-right" type="button" data-toggle="collapse" data-target="#cForm"><i class="fa fa-times mr-2"></i>Tutup</button>
				</div>
			</form>
		</div>
	</div>
	<!-- TABLE DISNI -->
	<div class="card-body">
		<div id="toolbar" class="form-inline">
			<button class="btn btn-secondary" data-toggle="modal" data-target="#m-filter"><i class="fa fa-filter text-danger mr-2"></i>Filter: <b id="s-indicator">off</b></button>
			<button type="button" onclick="cetak()" class="btn btn-warning ml-1"><i class="fa fa-print"></i></a>
		</div>
		<table class="table table-striped table-bordered table-js table-white" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/cuti/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
			<thead>
				<tr>
					<th class="text-center" data-formatter="formatNomor">No.</th>
					<th class="text-center" data-field="aksi">Aksi</th>
					<th data-field="pegawai" data-sortable="true">Pegawai</th>
					<th data-field="tanggal_cuti" class="text-center" data-sortable="true" data-formatter="formatDate">Tanggal Cuti</th>
					<th data-field="keterangan">Keterangan</th>
					<th data-field="kategori" class="text-center">Kategori</th>
					<th data-formatter="formatSumber">Detail Entri</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
@end

@section('modal')
<div id="m-filter" class="modal modal-protected-on-close fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="my-modal-title">Filter Data</h5>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#">
					<div class="form-group">
						<label for="i-tanggal-cuti">Rentang Waktu</label>
						<div class="input-group">
							<input name="tanggal_cuti_from" class="form-control" type="date" placeholder="Masukkan tanggal awal">
							<div class="input-group-append"><span class="input-group-text">s/d</span></div>
							<input name="tanggal_cuti_to" class="form-control" type="date" placeholder="Masukkan tanggal akhir">
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-primary" type="button" onclick="filter()">Filter</button>
						<button class="btn btn-warning" type="button" data-dismiss="modal">Tutup</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="m-cetak" class="modal modal-protected-on-close fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="my-modal-title">Rekap Data Cuti</h5>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{base_url('cuti/cetak')}}" method="get">
					<div class="form-group">
						<label for="i-tanggal-cuti">Tahun</label>
						<div class="input-group">
							<select class="form-control" name="year">
							<?php 
								$y = 2019;
								for($i = date('Y'); $i >= $y; $i--){
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
							?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Cetak</button>
						<button class="btn btn-warning" type="button" data-dismiss="modal">Tutup</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@end

@section('style')
<style>
	th,
	td {
		vertical-align: middle !important
	}
</style>
@end

@section('js')
<script type="text/javascript">
	// VAR
	var urlCuti = "{{site_url('api/internal/cuti')}}";
	var urlPegawai = "{{site_url('api/internal/pegawai')}}";
	var token = "{{$this->session->auth['token']}}";
	// INIT =================================
	$(".select2-pegawai").prop("multiple", "multiple").select2({
		placeholder: "Pilih Pegawai",
		minimumInputLength: 3,
		allowClear: true,
		ajax: {
			url: urlPegawai+'/get_select2_data/'+token,
			dataType: "json",
			delay: 600
		}
	});
	// EVENTS =============================== //

	$('#cForm').on('hidden.bs.collapse', function() {
		$("#cForm form").trigger('reset');
		$("[name=id]").val('');
		$(".select2-pegawai").val(null).trigger('change').prop('disabled', false);
		$('[name=tanggal_cuti], [name=durasi]').prop('disabled', false);
	}).on('shown.bs.collapse', function() {
		$(".select2-pegawai").focus();
	})

	$('[name=tanggal_cuti_from').on('change', function(e) {
		$('[name=tanggal_cuti_to').val('').prop('min', $(this).val());
	});

	// FUNCTIONS ============================ //
	function help() {
		Swal.fire('Info!', 'Data cuti yang diinput lewat pengajuan hanya dapat dihapus melalui menu pengajuan.', 'info');
	}

	function save() {
		//Validate
		var valid = true;
		$('[required]').each(function() {
			if (!$(this).val() || $(this).val() === null) {
				$(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
				valid = false;
			} else {
				$(this).removeClass('is-invalid').parent().find('.form-text').text('');
			}
		});

		if (!valid) {
			return;
		}

		//Save
		var data = $('#cForm form').serializeArray();
		$.ajax({
			url: urlCuti + '/save/' + token,
			method: 'POST',
			data: data,
			dataType: 'json',
			success: function(result, status, xhr) {
				if (result.status === 'success') {
					Toast.fire('Sukses!', result.message, 'success');
					$('.collapse').collapse('hide');
					$('.table-js').bootstrapTable('refresh');
				} else {
					Swal.fire('Error!', result.message, 'error');
				}
			},
			error: function(xhr, status, error) {
				Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
			}
		});
	}

	function edit(id) {
		$.getJSON(urlCuti + '/get/' + token + '?callback=callback_get&id=' + id, function(result) {
			if (result) {
				var opt = '<option value="' + result.id + '" selected>' + result.nama + '</option>';
				$('[name=id]').val(id);
				$('.select2-pegawai').append(opt).trigger('change');
				$('[name=kategori] option[value=' + result.kategori + ']').prop("selected", true);
				$('[name=tanggal_cuti]').val(result.tanggal_cuti);
				$('[name=keterangan]').val(result.keterangan);
				$('.select2-pegawai').prop('disabled', true);
				$('#cForm').collapse('show');
			}
		});
	}

	function remove(id) {
		Swal.fire({
			title: 'Apakah anda yakin?',
			text: "Aksi ini tidak dapat diurungkan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya, hapus data!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: urlCuti + '/delete/' + token,
					method: 'POST',
					data: {
						id: id
					},
					dataType: 'json',
					success: function(result, status, xhr) {
						if (result.status === 'success') {
							Toast.fire('Sukses!', result.message, 'success');
							$('.table-js').bootstrapTable('refresh');
						} else {
							Swal.fire('Error!', result.message, 'error');
						}
					},
					error: function(xhr, status, error) {
						Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
					}
				});
			}
		});
	}

	function cetak(){
		$('#m-cetak').modal('show');	
	}

	function filter() {
		var param = $("#m-filter form").serialize();
		$("#s-indicator").text('off');
		$('.table-js').bootstrapTable('refresh', {
			url: urlCuti+'/get_many/'+token+'?callback=callback_table&'+param
		});
		$(".modal").modal("hide");
		// Indikator
		var filterData = $("#m-filter form").serializeArray().filter(function(el) {
			return el.value != null && el.value != '';
		});
		if (filterData.length > 0) {
			$(".fa-filter").removeClass('text-danger').addClass('text-primary');
			$("#s-indicator").text('on');
		} else {
			$(".fa-filter").removeClass('text-primary').addClass('text-danger');
		}
	}

	function formatSumber(v, r) {
		sumber = r.pengajuan_id == '' || r.pengajuan_id == null ? 'Entri' : 'Pengajuan';
		return "<b>" + sumber + "</b><br>" + moment(r.insert_time).format("DD/MM/YY, hh:mm");
	}
</script>
@end