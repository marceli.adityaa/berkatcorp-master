@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Cuti</li>
    </ol>
</nav>
@end

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Cuti Bulan Ini</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget['cuti_sekarang']}} Cuti</h3>
                <i class="fa fa-calendar-o widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Cuti Bulan Depan</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget['cuti_depan']}} Cuti</h3>
                <i class="fa fa-calendar-plus-o widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Menunggu Persetujuan</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget['cuti_menunggu']}} Cuti</h3>
                <i class="fa fa-calendar-check-o widget-icon"></i>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body py-3 px-3">
        <a href="{{site_url('cuti/data')}}" class="btn btn-primary mr-2"><i class="icon-plane mr-2"></i>Data Cuti</a>
        <a href="{{site_url('cuti/pengajuan')}}" class="btn btn-primary mr-2"><i class="fa fa-share-square-o mr-2"></i>Pengajuan<span class="badge badge-light ml-3">{{$notif}}</span></a>
        <!-- <a href="#" class="btn btn-primary"><i class="fa fa-file-o mr-2"></i>Laporan</a> -->
    </div>
</div>

<div class="card mt-3">
    <div class="card-body px-5 py-5">
        <span class="tx-bold tx-uppercase">Grafik Cuti Tahun 2019</span>
        <canvas id="chart1" width="100%" height="35px"></canvas>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body px-4 py-4">
        <h6 class="tx-uppercase tx-bold">Jadwal Cuti Bulan Ini</h6>
        <table class="table table-sm mt-3">
            @if($schedule)
            <?php $d = ''; ?>
            @foreach($schedule AS $s)
            @if($s->tanggal_cuti != $d)
            <?php $d = $s->tanggal_cuti; ?>
            <tr>
                <td colspan="2" class="tx-bold px-4 py-3 tx-uppercase bg-dark tx-white">{{datify($s->tanggal_cuti,'d F Y')}}</td>
            </tr>
            @endif
            <tr>
                <td class="px-4 tx-19" width="30%">{{$s->nama}}</td>
                <td class="pt-3">
                    <span class="tx-bold tx-uppercase">{{$s->kategori}}</span>
                    <p>{{$s->keterangan}}</p>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td>Tidak ada cuti bulan ini</td>
            </tr>
            @endif
        </table>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/chartjs/chart.min.css')}}">
<style>
    td {
        vertical-align: middle !important;
    }

    .widget-icon {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 7em;
    }
</style>
@end

@section('js')
<script src="{{base_url('assets/plugins/chartjs/chart.min.js')}}"></script>
<script>
    var graphData = "{{implode(',', $graph)}}".split(",");
    var myLineChart = new Chart($("#chart1"), {
        type: 'bar',
        data: {
            labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            datasets: [{
                label: 'jumlah cuti',
                data: graphData,
                backgroundColor: 'rgba(107, 186, 255,.2)',
                borderColor: 'rgba(107, 186, 255)',
                pointBackgroundColor: 'rgba(107, 186, 255)'
            }]
        },
        options:{scales:{yAxes:[{ticks:{precision:0}}]}}
    });
</script>
@end