@layout('commons/index')

@section('content')
<ul class="block">
    <li data-index="1">List-1</li>
    <li data-index="2">List-2</li>
    <li data-index="3">List-3</li>
</ul>
<p class="form-text">Urutan = </p>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/jquery-ui/jquery-ui.min.css')}}">
<style>
    ul.block{padding-left: 0;}
    ul.block li {
        display: block;
        padding: 15px 30px;
        background-color: gray;
        margin-bottom: 5px;
        cursor: pointer;
        color: white;
    }
</style>
@end

@section('js')
<script src="{{base_url('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script>
$(".block").sortable({
    stop: function( event, ui ) {
        var str = "Urutan :";
        $("ul.block").find('li').each(function(){
            str += " "+$(this).data('index');
        });
        $(".form-text").text(str);
    }
});

</script>
@end