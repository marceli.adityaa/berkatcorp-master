@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('overtime')}}">Overtime</a></li>
        <li class="breadcrumb-item active">Manajemen Data</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header card-header-default justify-content-between bg-brown">
        <h6 class="mg-b-0">Overtime Pegawai</h6>
        <div class="btn-group">
            <button class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#cForm"><i class="fa fa-plus mr-2"></i>Formulir Isian</button>
        </div>
    </div>
    <!-- FORMULIR TAMBAH -->
    <div id="cForm" class="collapse">
        <div class="card-body bg-gray-200">
            <h5><i class="fa fa-clock-o mr-2"></i><span class="label-form"></span> Overtime</h5>
            <hr>
            <form action="#">
                <!-- 01 -->
                <input type="hidden" name="id" value="">
                <div class="form-row">
                    <!-- 02 -->
                    <div class="form-group col-12 col-lg-4">
                        <label for="">Jenis Entri</label>
                        <select name="arus" class="form-control">
                            <option value="in">Tambah Overtime</option>
                            <option value="out">Kurangi Overtime</option>
                        </select>
                    </div>
                    <!-- 03 -->
                    <div class="form-group col-12 col-lg-8">
                        <label for="">Kategori Overtime</label>
                        <select name="kategori" class="form-control">
                            <option value="lembur" selected>Lembur</option>
                            <option value="lain">Lain-lain</option>
                        </select>
                    </div>
                </div>
                <!-- 04 -->
                <div class="form-group" id="dPegawai">
                    <label for="">Pegawai</label>
                    <select name="pegawai_id[]" class="form-control select2-pegawai" required></select>
                </div>
                <div class="form-row">
                    <div class="form-group col-12 col-lg-4">
                        <!-- 05 -->
                        <label for="">Waktu Mulai</label>
                        <input type="text" class="form-control datetime" name="waktu_mulai" placeholder="Waktu Mulai Overtime" onchange="calTime(this)" readonly required>
                        <p class="form-text"></p>
                    </div>
                    <!-- 06 -->
                    <div class="form-group col-12 col-lg-4">
                        <label for="">Waktu Selesai</label>
                        <input type="text" class="form-control datetime" name="waktu_selesai" placeholder="Waktu Selesai Overtime" onchange="calTime(this)" readonly required>
                        <p class="form-text"></p>
                    </div>
                    <!-- 07 -->
                    <div class="form-group col-12 col-lg-4">
                        <label for="">Durasi</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="durasi" min="0" onblur="calTime(this)" required>
                            <div class="input-group-append"><span class="input-group-text">Menit</span></div>
                        </div>
                        <p class="form-text"></p>
                    </div>
                </div>
                <!-- 08 -->
                <div class="form-group">
                    <label for="">Keterangan (opsional)</label>
                    <input type="text" class="form-control" name="keterangan" placeholder="Keterangan Overtime">
                    <p class="form-text"></p>
                </div>
                <!-- 09 -->
                <div class="form-group d-flex">
                    <div class="input-group">
                        <button class="btn btn-primary" id="#bSimpan" type="button" onclick="save()"><i class="fa fa-save mr-2"></i>Simpan Data</button>
                        <button class="btn btn-warning" type="reset"><i class="fa fa-refresh mr-2"></i>Bersihkan</button>
                    </div>
                    <button class="btn btn-secondary float-right" type="button" data-toggle="collapse" data-target="#cForm"><i class="fa fa-times mr-2"></i>Tutup</button>
                </div>
            </form>
        </div>
    </div>
    <!-- TABEL -->
    <div class="card-body">
        <table class="table table-js table-white" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/overtime/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
            <thead>
                <tr>
                    <th data-formatter="formatNomor" class="text-center">No.</th>
                    <th data-field="aksi" class="text-center">Aksi</th>
                    <th data-field="nama" data-sortable="true">Nama Pegawai</th>
                    <th data-field="waktu_mulai" data-formatter="formatDateTime" class="tx-14 text-center" data-sortable="true">Mulai</th>
                    <th data-field="waktu_selesai" data-formatter="formatDateTime" class="tx-14 text-center" data-sortable="true">Selesai</th>
                    <th data-field="durasi" data-formatter="formatDurasi" class="tx-14 text-center" data-sortable="true">Durasi</th>
                    <th data-field="keterangan">Keterangan</th>
                    <th data-formatter="formatArus" class="text-center">Kategori</th>
                    <th data-formatter="formatSumber" class="tx-14">Detail Entri</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('style')
<style>
    th,
    td {
        vertical-align: middle !important;
    }
</style>
@end


@section('js')
<script type="text/javascript">
    var url = "{{site_url('api/internal/overtime')}}";
    var token = "{{$this->session->auth['token']}}";

    // INITs //=======================================================
    $(".select2-pegawai").prop("multiple", "multiple").select2({
        placeholder: "Pilih Pegawai",
        minimumInputLength: 3,
        allowClear: true,
        ajax: {
            url: "{{site_url('api/internal/pegawai/get_select2_data/'.$this->session->auth['token'])}}",
            dataType: "json",
            delay: 600
        }
    });

    // EVENTS //=======================================================
    $("#cForm")
        .on("hidden.bs.collapse", function(e) {
            $(this).find("form").trigger("reset");
            $(".select2-pegawai").val(null).prop("multiple", true).prop('disabled', false).trigger("change");
        })
        .on("shown.bs.collapse", function(e) {
            $(".select2-pegawai").focus();
        });

    // FUNCTIONS //====================================================
    function calTime(e) {
        var d1 = $("[name=waktu_mulai]").val();
        var d2 = $("[name=waktu_selesai]").val();
        var durasi = $("[name=durasi]").val();

        if ($(e).attr('name') != 'durasi' && d1 != '' && d2 != '') {
            $("[name=durasi]").val(Math.abs(moment(d2).diff(moment(d1), 'minutes')));
        }

        if ($(e).attr('name') == 'durasi' && d1 != '' && durasi != '') {
            $("[name=waktu_selesai]").val(moment(d1).add(durasi, 'minutes').format('YYYY-MM-DD HH:mm'));
        }
    }

    function save() {
        // Validate
        var valid = true;
        $('[required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }

        // Save
        var data = $('#cForm form').serializeArray();
        $.ajax({
            url: '{{site_url("api/internal/overtime/save/".$this->session->auth["token"])}}',
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', result.message, 'success');
                    $('.table-js').bootstrapTable('refresh');
                    $("#cForm").collapse("hide");
                } else {
                    Swal.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function edit(id) {
        $.getJSON("{{site_url('api/internal/overtime/get/'.$this->session->auth['token'])}}?callback=callback_get&id="+id, function(result) {
            if (result) {
                var opt = '<option selected>' + result.nama + '</option>';
                $('[name=id]').val(id);
                $('.select2-pegawai').val(null).append(opt).trigger('change').prop('disabled', true);
                $('[name=keterangan]').val(result.keterangan);
                $('[name=waktu_mulai]').val(moment(result.waktu_mulai).format('YYYY-MM-DD HH:mm'));
                $('[name=waktu_selesai]').val(moment(result.waktu_selesai).format('YYYY-MM-DD HH:mm'));
                $('[name=durasi]').val(result.durasi);
                $('[name=arus] option[value=' + result.arus + ']').prop('selected', true);
                $('[name=kategori] option[value="' + result.kategori + '"]').prop('selected', true);
                $('#cForm').collapse('show');
            }
        });
    }

    function remove(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Aksi ini tidak dapat diurungkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus data!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{site_url("api/internal/overtime/delete/".$this->session->auth["token"])}}',
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', result.message, 'success');
                            $("#cForm").collapse("hide");
                            $('.table-js').bootstrapTable('refresh');
                        } else {
                            Toast.fire('Error!', result.error, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        })
    }

    function help() {
        Swal.fire('Info!', 'Data overtime yang diinput lewat pengajuan atau absensi hanya dapat dihapus melalui menu pengajuan atau absensi.', 'info');
    }

    function formatArus(v, r) {
        return (r.arus == 'in') ? '<span class="badge badge-primary"><i class="fa fa-plus mr-2"></i>'+r.kategori+'</span>' : '<span class="badge badge-danger"><i class="fa fa-minus mr-2"></i>'+r.kategori+'</span>';
    }

    function formatSumber(v, r) {
        return "<b>" + r.sumber + "</b><br>" + moment(r.insert_time).format("DD/MM/YY, hh:mm");
    }
</script>
@end