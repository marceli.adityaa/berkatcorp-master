@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Tabungan</a></li>
    </ol>
</nav>
@end

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Saldo Bulan Ini</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{monefy($widget->tambah-$widget->kurang,false)}} Menit</h3>
                <i class="fa fa-clock-o widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Saldo Masuk</span>
                <h3 class="mt-2 tx-white tx-bold">{{monefy($widget->tambah,false)}} Menit</h3>
                <i class="fa fa-plus-circle widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Saldo Keluar</span>
                <h3 class="mt-2 tx-white tx-bold">{{monefy($widget->kurang,false)}} Menit</h3>
                <i class="fa fa-minus-circle widget-icon"></i>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body py-3 px-3">
        <a href="{{site_url('overtime/data')}}" class="btn btn-primary mr-2"><i class="icon-book-open mr-2"></i>Data Overtime</a>
        <a href="{{site_url('overtime/pengajuan')}}" class="btn btn-primary mr-2"><i class="fa fa-share-square-o mr-2"></i>Pengajuan<span class="badge badge-light ml-3">{{$notif}}</span></a>
        <!-- <a href="#" class="btn btn-primary"><i class="fa fa-file-o mr-2"></i>Laporan</a> -->
    </div>
</div>

<div class="card mt-3">
    <div class="card-body px-5 py-5">
        <span class="tx-bold tx-uppercase">Grafik Overtime Tahun 2019</span>
        <canvas id="chart1" width="100%" height="35px"></canvas>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body">
        <span class="tx-bold tx-uppercase">Overtime Hari Ini</span>
        @if($today)
        <table class="table table-striped table-sm mt-3">
            @foreach($today AS $t)
            <tr>
                <td class="text-center" width="15%">{{datify($t->waktu_mulai,'H:i').' - '.datify($t->waktu_selesai,'H:i')}}</td>
                <td class="text-center" width="10%" style="background-color: rgba(0,0,0,.1)">
                    <h4 class="mb-0 pb-0">{{$t->durasi}}</h4>menit
                </td>
                <td><span class="tx-15 pl-2">{{$t->nama}}</span></td>
                <td>
                    <span class="tx-11 tx-bold tx-uppercase">{{$t->kategori}}</span>
                    <br>
                    <span class="">{{$t->keterangan?:'-'}}</span>
                </td>
                <td width="5%">{{$t->arus=='in'?'<i class="fa fa-plus-circle tx-20 tx-success"></i>':'<i class="fa fa-minus-circle tx-20 tx-danger"></i>'}}</td>
            </tr>
            @endforeach
        </table>
        @else
        <p class="mt-3">Tidak ada overtime hari ini.</p>
        @endif
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/chartjs/chart.min.css')}}">
<style>
    td {
        vertical-align: middle
    }

    .widget-icon {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 7em;
    }
</style>
@end

@section('js')
<script src="{{base_url('assets/plugins/chartjs/chart.min.js')}}"></script>
<script>
    var graphDataTambah = "{{implode(',', $graph['tambah'])}}".split(",");
    var graphDataKurang = "{{implode(',', $graph['kurang'])}}".split(",");

    var myLineChart = new Chart($("#chart1"), {
        type: 'line',
        data: {
            labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            datasets: [{
                    label: 'Penambahan',
                    data: graphDataTambah,
                    backgroundColor: 'rgba(107, 186, 255,.2)',
                    borderColor: 'rgba(107, 186, 255)',
                    lineTension: '0.1',
                    pointBackgroundColor: 'rgba(107, 186, 255)'
                },
                {
                    label: 'Pengurangan',
                    data: graphDataKurang,
                    backgroundColor: 'rgba(255, 107, 107,.2)',
                    borderColor: 'rgba(255, 107, 107)',
                    lineTension: '0.1',
                    pointBackgroundColor: 'rgba(255, 107, 107)'
                }
            ]
        },
        options:{scales:{yAxes:[{ticks:{precision:0}}]}}
    });
</script>
@end