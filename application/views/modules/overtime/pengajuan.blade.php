@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('overtime')}}">Overtime</a></li>
        <li class="breadcrumb-item active">Pengajuan</li>
    </ol>
</nav>
@end

@section('content')
<div class="card card-secondary">
    <div class="card-header card-header-default bg-brown">
        <h6>Data Pengajuan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="form-inline">
            <button class="btn btn-warning" data-toggle="modal" data-target="#m-filter"><i class="fa fa-filter text-danger mr-2"></i>Filter: <b id="s-indicator">off</b></button>
            <select id="s-status" class="form-control">
                <option value="0" selected>Menunggu</option>
                <option value="1">Disetujui</option>
                <option value="2">Ditolak</option>
                <option value="">Semua</option>
            </select>
        </div>
        <table class="table table-sm table-striped table-js table-white" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/overtime_pengajuan/get_many/'.$this->session->auth['token'])}}?callback=callback_table&status=0">
            <thead>
                <tr>
                    <th class="text-center" data-field="nomor">No.</th>
                    <th class="text-center text-nowrap" data-field="aksi">Aksi</th>
                    <th data-field="nama" class="text-center" data-sortable="true">Pegawai</th>
                    <th data-field="waktu_mulai" class="text-center" data-formatter="formatDateTime" data-sortable="true">Waktu Mulai</th>
                    <th data-field="waktu_selesai" class="text-center" data-formatter="formatDateTime" data-sortable="true">Waktu Selesai</th>
                    <th data-field="durasi" data-formatter="formatDurasi" class="text-center" data-sortable="true">Durasi</th>
                    <th data-field="keterangan">Keterangan</th>
                    <th data-field="status" class="text-center">Status</th>
                    <th data-field="tanggal_pengajuan" class="text-center" data-sortable="true" data-formatter="formatDateTime">Tgl. Pengajuan</th>
                    <th data-field="tanggal_verifikasi" class="text-center" data-sortable="true" data-formatter="formatDateTime">Tgl. Verifikasi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="m-filter" class="modal modal-protected-on-close fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">Filter Data</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#">
                    <div class="form-group">
                        <label for="i-tanggal-cuti">Rentang Waktu</label>
                        <div class="input-group">
                            <input name="tanggal_cuti_from" class="form-control date-from" type="date">
                            <div class="input-group-append"><span class="input-group-text">s/d</span></div>
                            <input name="tanggal_cuti_to" class="form-control date-to" type="date">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="i-tanggal-cuti">Tanggal Pengajuan</label>
                        <div class="input-group">
                            <input name="tanggal_pengajuan_from" class="form-control date-from" type="date">
                            <div class="input-group-append"><span class="input-group-text">s/d</span></div>
                            <input name="tanggal_pengajuan_to" class="form-control date-to" type="date">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="button" onclick="filter()">Filter</button>
                        <button class="btn btn-warning" type="reset">Bersihkan</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="m-konfirmasi" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-light">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">Konfirmasil <span id="s-jenis-konfirmasi"></span></h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-light">
                <form action="#">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="status" value="">
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea class="form-control" name="pesan" placeholder="Isi keterangan (opsional)" rows="5" cols="30"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-dark" type="button" onclick="konfirmasi()">Simpan</button>
                        <button class="btn btn-dark" type="button" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
    var token = "{{$this->session->auth['token']}}";
    var url = "{{site_url('api/internal/overtime_pengajuan')}}";

    // Filter by status
    $("#s-status").on('change', function(e) {
        $(".table-js").bootstrapTable('refresh', {
            url: url + '/get_many/' + token + '?callback=callback_table&status=' + $(this).val()
        });
        $("#m-filter form").trigger('reset');
        $(".fa-filter").removeClass('text-primary').addClass('text-danger');
        $("#s-indicator").text('off');
    });

    // Set min dan max tentang waktu
    $('.date-from').on('change', function(e) {
        $(this).parent().find('.date-to').val('').prop('min', $(this).val());
    });

    $("#m-konfirmasi").on("shown.bs.modal", function(e) {
        $("#m-konfirmasi form textarea").focus();
    });

    // Filter lengkap
    function filter() {
        // Ambil data dan jadikan parameter
        var param = $("#m-filter form").serialize();
        // Indikator filter
        $("#s-indicator").text('off');
        // Refresh data
        $('.table-js').bootstrapTable('refresh', {
            url: url + "/get_table_data/" + token + '?' + param + '&status=' + $("#s-status").val()
        });
        // Tutup modal filter
        $(".modal").modal("hide");
        // Indikator
        var filterData = $("#m-filter form").serializeArray().filter(function(el) {
            return el.value != null && el.value != '';
        });
        if (filterData.length > 0) {
            $(".fa-filter").removeClass('text-danger').addClass('text-primary');
            $("#s-indicator").text('on');
        } else {
            $(".fa-filter").removeClass('text-primary').addClass('text-danger');
        }
    }

    function setuju(id) {
        $("#s-jenis-konfirmasi").text('Setuju');
        $("[name=id]").val(id);
        $("[name=status]").val(1);
        $("#m-konfirmasi .modal-content, #m-konfirmasi .modal-body").removeClass("bg-danger").addClass("bg-success");
        $("#m-konfirmasi").modal("show");
    }

    function tolak(id) {
        $("#s-jenis-konfirmasi").text('Tolak');
        $("[name=id]").val(id);
        $("[name=status]").val(2);
        $("#m-konfirmasi .modal-content, #m-konfirmasi .modal-body").removeClass("bg-success").addClass("bg-danger");
        $("#m-konfirmasi").modal("show");
    }

    function konfirmasi() {
        $.ajax({
            url: url + '/save/' + token,
            method: 'POST',
            data: $("#m-konfirmasi form").serializeArray(),
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', 'pengajuan berhasil dikonfirmasi', 'success');
                    $('.modal').modal('hide');
                    $('.table-js').bootstrapTable('refresh');
                } else {
                    Toast.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Swal.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function batal(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Aksi ini tidak dapat diurungkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, batalkan pengajuan!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url + '/cancel/' + token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', result.message, 'success');
                            $('.table-js').bootstrapTable('refresh');
                        } else {
                            Toast.fire('Error!', result.error, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        })
    }

    function pesan(id) {
        $.getJSON(url + '/get/' + token + '?id=' + id, function(result) {
            var msgType = ['', 'success', 'error'];
            Swal.fire('Pesan Konfirmasi', result.pesan, msgType[result.status]);
        });
    }
</script>
@end