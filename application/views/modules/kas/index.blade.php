@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Kas</li>
    </ol>
</nav>
@end

@section('content')
<div id="toolbar">
    <button class="btn btn-primary" data-toggle="modal" data-target="#mForm"><i class="fa fa-plus mr-2"></i>Tambah Kas</button>
</div>
<div class="card">
    <div class="card-header">Data Kas</div>
    <div class="card-body">
        <table class="table table-js table-white" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/kas/get_data_table')}}">
            <thead>
                <tr>
                    <th data-formatter="formatNomor" class="text-center">No.</th>
                    <th data-field="aksi" class="text-center">Aksi</th>
                    <th data-sortable="true" data-field="perusahaan">Perusahaan</th>
                    <th data-sortable="true" data-field="is_rekening">Jenis</th>
                    <th data-field="label">Label Kas</th>
                    <th data-field="keterangan">Keterangan</th>
                    <th data-field="pic">Penanggung Jawab</th>
                    <th data-formatter="formatSumber" class="tx-14">Detail Entri</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="mForm" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header tx-bold">Formulir Isian Kas</div>
            <div class="modal-body">
                <form action="#">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label for="">Perusahaan</label>
                        <select name="id_perusahaan" class="form-control" required>
                            <option value="">- Pilih Salah Satu -</option>
                            @foreach ($perusahaan as $row)
                                <option value="{{$row['id']}}">{{$row['nama_singkat']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Penanggung Jawab</label>
                        <select name="pegawai_id" class="form-control" required></select>
                        <p class="form-text"></p>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis</label>
                        <div class="col-12">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="jenis-kas" name="is_rekening" class="custom-control-input" value="0">
                                <label class="custom-control-label" for="jenis-kas">Kas</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="jenis-rekening" name="is_rekening" class="custom-control-input" value="1">
                                <label class="custom-control-label" for="jenis-rekening">Rekening</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group kas">
                        <label for="">Label</label>
                        <input type="text" class="form-control" name="label">
                    </div>
                    <div class="form-group kas">
                        <label for="">Keterangan</label>
                        <input type="text" class="form-control" name="keterangan">
                    </div>
                    <div class="form-group rekening">
                        <label for="">Bank</label>
                        <select name="id_bank" class="form-control select2">
                            <option value="">- Pilih Salah Satu -</option>
                            @foreach ($bank as $row)
                                <option value="{{$row['id']}}">{{$row['bank']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group rekening">
                        <label for="">No Rekening</label>
                        <input type="text" class="form-control" name="no_rekening">
                    </div>
                    <div class="form-group rekening">
                        <label for="">Nama Pemilik</label>
                        <input type="text" class="form-control" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <input type="text" class="form-control" name="keterangan">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="save()">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    url = "{{site_url('api/internal/kas')}}";
    token = "{{$this->session->auth['token']}}";

    // INITS
    $(document).ready(function(){
        cek_jenis_kas();
    })
    
    $("[name=pegawai_id]").select2({
        placeholder: "Pilih Pegawai",
        minimumInputLength: 3,
        dropdownParent: $('#mForm'),
        ajax: {
            url: "{{site_url('api/internal/pegawai/get_select2_data/'.$this->session->auth['token'])}}",
            dataType: "json",
            delay: 600
        }
    });

    // EVENTS
    $("#mForm").on("hidden.bs.modal", function() {
        $("#mForm form").trigger("reset");
        $("[name=pegawai_id]").val(null).trigger("change").prop('disabled', false);
        cek_jenis_kas();
    });

    $("[name=is_rekening").change(function(){
        cek_jenis_kas();
    })

    // FUNCTIONS
    function save() {
        // Validate
        var valid = true;
        $('[required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }

        // Save
        $.ajax({
            url: url+'/save/'+token,
            method: 'POST',
            data: $('#mForm form').serializeArray(),
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', result.message, 'success');
                    $('.table-js').bootstrapTable('refresh');
                    $("#mForm").modal("hide");
                } else {
                    Swal.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function edit(id) {
        $.getJSON(url+'/get/'+token+'?callback=callback_get&id='+id, function(result) {
            if (result) {
                var opt = '<option value="'+result.pegawai_id+'" selected>' + result.pic + '</option>';
                $('[name=pegawai_id]').html(opt).trigger('change');
                $('[name=id]').val(id);
                $('[name=label]').val(result.label);
                $('[name=id_perusahaan]').val(result.id_perusahaan).change();
                $('[name=is_rekening][value='+result.is_rekening+']').prop('checked', true);
                if(result.id_bank != "0"){
                    $('[name=id_bank]').val(result.id_bank).change();
                }else{
                    $('[name=id_bank]').val('').change();
                }
                $('[name=no_rekening]').val(result.no_rekening);
                $('[name=nama]').val(result.nama);
                $('[name=keterangan]').val(result.keterangan);
                $('#mForm').modal('show');
                cek_jenis_kas();
            }
        });
    }

    function remove(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Aksi ini tidak dapat diurungkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus data!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url+'/delete/'+token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', result.message, 'success');
                            $("#mForm").collapse("hide");
                            $('.table-js').bootstrapTable('refresh');
                        } else {
                            Toast.fire('Error!', result.error, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        })
    }

    function cek_jenis_kas(){
        let is_rekening = $('[name=is_rekening]:checked').val();
        if(is_rekening == 1){
            $('.rekening').fadeIn();
            $('.kas').hide();
        }else if(is_rekening == 0){
            $('.rekening').hide();
            $('.kas').fadeIn();
        }else{
            $('.rekening').hide();
            $('.kas').hide();
        }
    }

    function disable(id, $isDisabled) {
		$.getJSON(url+'/disable/'+token+'?id='+id+'&par='+!$isDisabled,function(result){
			if(result.status == 'success'){
				Toast.fire('Sukses!', result.message,'success');
				$('.table-js').bootstrapTable('refresh');
			}
		});
	}
    
    function formatSumber(value, row, index, field) {
        return "<b>Entri</b><br>" + (row.insert_time);
    }
</script>
@end