@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Rekening</li>
    </ol>
</nav>
@end

@section('content')
<div id="toolbar">
    <button class="btn btn-primary" data-toggle="modal" data-target="#mForm"><i class="fa fa-plus mr-2"></i>Tambah Data</button>
</div>
<div class="card">
    <div class="card-header">Data Rekening</div>
    <div class="card-body">
        <table class="table table-js table-white" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/rekening/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
            <thead>
                <tr>
                    <th data-formatter="formatNomor" class="text-center">No.</th>
                    <th data-field="aksi" class="text-center">Aksi</th>
                    <th data-field="perusahaan">Perusahaan</th>
                    <th data-field="bank">Bank</th>
                    <th data-field="no_rekening">No Rekening</th>
                    <th data-field="nama">Nama</th>
                    <th data-field="keterangan">Keterangan</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="mForm" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header tx-bold">Formulir Isian Data Rekening</div>
            <div class="modal-body">
                <form action="#">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label for="">Perusahaan</label>
                        <select name="id_perusahaan" class="form-control" required>
                            <option value="">- Pilih Salah Satu -</option>
                            @foreach ($perusahaan as $row)
                                <option value="{{$row['id']}}">{{$row['nama_singkat']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Bank</label>
                        <select name="id_bank" class="form-control select2" required>
                            <option value="">- Pilih Salah Satu -</option>
                            @foreach ($bank as $row)
                                <option value="{{$row['id']}}">{{$row['bank']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">No Rekening</label>
                        <input type="text" class="form-control" name="no_rekening" required>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Pemilik</label>
                        <input type="text" class="form-control" name="nama" required>
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <input type="text" class="form-control" name="keterangan">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="save()">Simpan Transaksi</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    url = "{{site_url('api/internal/rekening')}}";
    token = "{{$this->session->auth['token']}}";

    $('.select2').select2({
        dropdownParent: $('#mForm')
    });

    // FUNCTIONS
    function save() {
        // Validate
        var valid = true;
        $('[required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }
        
        // Save
        $.ajax({
            url: url+'/save/'+token,
            method: 'POST',
            data: $('#mForm form').serializeArray(),
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', result.message, 'success');
                    $('.table-js').bootstrapTable('refresh');
                    $("#mForm").modal("hide");
                } else {
                    Swal.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function edit(id) {
        $.getJSON(url+'/get/'+token+'?id='+id, function(result) {
            if (result) {
                var opt = '<option value="'+result.pegawai_id+'" selected>' + result.nama + '</option>';
                $('[name=id]').val(id);
                $('[name=id_perusahaan]').val(result.id_perusahaan).change();
                $('[name=id_bank]').val(result.id_bank).change();
                $('[name=no_rekening]').val(result.no_rekening);
                $('[name=nama]').val(result.nama);
                $('[name=keterangan]').val(result.keterangan);
                $('#mForm').modal('show');
            }
        });
    }

    function remove(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Aksi ini tidak dapat diurungkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus data!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url+'/delete/'+token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', result.message, 'success');
                            $("#mForm").collapse("hide");
                            $('.table-js').bootstrapTable('refresh');
                        } else {
                            Toast.fire('Error!', result.error, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        })
    }

    function disable(id, $isDisabled) {
		$.getJSON(url+'/disable/'+token+'?id='+id+'&par='+!$isDisabled,function(result){
			if(result.status == 'success'){
				Toast.fire('Sukses!', result.message,'success');
				$('.table-js').bootstrapTable('refresh');
			}
		});
	}
    
    function formatSumber(value, row, index, field) {
        return "<b>Entri</b><br>" + (row.insert_time);
    }
</script>
@end