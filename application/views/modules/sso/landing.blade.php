<!DOCTYPE html>
<html>

<head>
    <title>Halaman Masuk | </title>
    <!-- Bootstrap -->
    <link href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Fontawesome -->
    <link href="<?= base_url('assets/plugins/fontawesome/css/fontawesome.min.css') ?>" rel="stylesheet">
    <!-- css -->
    <link href="<?= base_url('assets/styles/login.css') ?>" rel="stylesheet">
    <style type="text/css">
        .tx-white{
            color: #fff;
        }
        .mg-t-2{
            margin-top:10px;
            margin-left:10px;
        }
        a{
            text-decoration: none;
            color:#333;
        }
        a:hover{
            text-decoration: none;
            color:brown;
        }
    </style>
</head>

<body>
    <div class="ajaxloader" style="display:none;">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner2.gif') ?>" class="img-fluid"> Loading...
        </div>
    </div>
    <div class="wrapper">
        <div class="row">
            <div class="col-12 col-lg-6 px-3 py-3">
                <ul class="list-group w-100">
                    <a href="https://master.berkatcorp.com/dashboard">
                        <li class="list-group-item mg-t-2">
                            <h3>MASTER</h3>
                            <p>Aplikasi untuk mengelola data master.</p>
                        </li>
                    </a>
                    <a href="https://master.berkatcorp.com/sso/server/signin?source=https%3A%2F%2Fpegawai.berkatcorp.com%2Fdashboard">
                        <li class="list-group-item mg-t-2">
                            <h3>PEGAWAI</h3>
                            <p>Aplikasi untuk mengelola data memonitoring kepegawaian, diantaranya : absensi, cuti, overtime.</p>
                        </li>
                    </a>
                    <a href="https://master.berkatcorp.com/sso/server/signin?source=https%3A%2F%transport.berkatcorp.com%2Fdashboard">
                        <li class="list-group-item mg-t-2">
                            <h3>TRANSPORTASI</h3>
                            <p>Aplikasi untuk mengelola data transportasi.</p>
                        </li>
                    </a>
                    <a href="https://master.berkatcorp.com/sso/server/signin?source=https%3A%2F%bas.berkatcorp.com%2Fdashboard">
                        <li class="list-group-item mg-t-2">
                            <h3>PT. BAS</h3>
                            <p>Aplikasi untuk mengelola segala macam transaksi yang ada di PT. BAS</p>
                        </li>
                    </a>
                </ul>
            </div>
            <div class="col-12 col-lg-6" id="right-side">
                <img src="{{base_url('assets/img/logo/berkat-group-white.png')}}" alt="Berkat Group" class="img-fluid" width="50%">
                <h3 class="mb-3 mt-3 tx-white">Tentang Berkat Group</h3>
                <p class="tx-white">Berkat Group adalah Perusahaan yang berdiri sejak tahun 1988.
                    Dasar visi Perusahaan Berkat Group adalah menjadi Perusahaan yang
                    berkembang besar dan dapat menjadi berkat bagi orang lain melalui kualitas
                    yang baik dari setiap produk yang disajikan.</p>
                <p class="text-small tx-white">______<br><br>Copyright &copy {{date('Y')}}. Berkat Group
                    <br>All Rights Reserved.</p>
            </div>
        </div>
    </div>

    <!-- jquery -->
    <script src="<?= base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url('assets/plugins/bootstrap/js/popper.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js') ?>"></script>
    <!-- js -->
    <script src="<?= base_url('assets/scripts/app.js') ?>"></script>
    <script src="<?= base_url('assets/scripts/login.js') ?>"></script>
</body>

</html>