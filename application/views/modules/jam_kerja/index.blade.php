@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item">Kamus Data</a></li>
        <li class="breadcrumb-item active">Jam Kerja</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">Profil Jam Kerja</div>
    <div class="card-body">
        <div id="toolbar">
            <button class="btn btn-primary" data-toggle="modal" data-target="#mForm"><i class="fa fa-plus mr-2"></i>Tambah</button>
        </div>
        <table class="table table-js table-striped table-bordered" data-toolbar="#toolbar">
            <thead>
                <tr>
                    <th class="text-center" rowspan="2">No.</th>
                    <th class="text-center" rowspan="2">Aksi</th>
                    <th class="text-center" rowspan="2">Nama Profil</th>
                    <th class="text-center" rowspan="2">Jam Kerja</th>
                    <th class="text-center" colspan="7">Hari</th>
                </tr>
                <tr>
                    <?php $hari = array('sen', 'sel', 'rab', 'kam', 'jum', 'sab', 'min'); ?>
                    @foreach($hari AS $h)
                    <th class="text-center">{{$h}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($data AS $index=>$value)
                <tr>
                    <td class="text-center">{{$index+1}}</td>
                    <td class="text-center">
                        <button class="btn btn-sm btn-warning" title="Sunting" onclick="edit({{$value->id}})"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-sm btn-danger" title="Hapus" onclick="remove({{$value->id}})"><i class="fa fa-trash"></i></button>
                        @if($value->is_disabled == '1')
                        <button class="btn btn-sm btn-danger bDisable" title="Aktifkan" onclick="disable({{$value->id}}, true)"><i class="fa fa-times"></i></button>
                        @else
                        <button class="btn btn-sm btn-success bDisable" title="Nonaktifkan" onclick="disable({{$value->id}}, false)"><i class="fa fa-check"></i></button>
                        @endif
                    </td>
                    <td class="text-center">{{ucwords($value->nama)}}</td>
                    <td class="text-center">{{datify($value->jam_masuk,'H:i')}} - {{datify($value->jam_pulang,'H:i')}}</td>
                    <td class="text-center">{{in_array('sen', json_decode($value->hari)) ? '<i class="fa fa-check"></i>' : '-'}}</td>
                    <td class="text-center">{{in_array('sel', json_decode($value->hari)) ? '<i class="fa fa-check"></i>' : '-'}}</td>
                    <td class="text-center">{{in_array('rab', json_decode($value->hari)) ? '<i class="fa fa-check"></i>' : '-'}}</td>
                    <td class="text-center">{{in_array('kam', json_decode($value->hari)) ? '<i class="fa fa-check"></i>' : '-'}}</td>
                    <td class="text-center">{{in_array('jum', json_decode($value->hari)) ? '<i class="fa fa-check"></i>' : '-'}}</td>
                    <td class="text-center">{{in_array('sab', json_decode($value->hari)) ? '<i class="fa fa-check"></i>' : '-'}}</td>
                    <td class="text-center">{{in_array('min', json_decode($value->hari)) ? '<i class="fa fa-check"></i>' : '-'}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="mForm" class="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">Form Isian</div>
            <div class="modal-body">
                <form action="#">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label>Nama Profil</label>
                        <input class="form-control" type="text" name="nama" placeholder="Nama profil" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12 col-md-6">
                            <label>Jam Masuk</label>
                            <input class="form-control clockpicker" type="text" name="jam_masuk" placeholder="Jam Masuk" required readonly>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label>Jam Pulang</label>
                            <input class="form-control clockpicker" type="text" name="jam_pulang" placeholder="Jam Pulang" required readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Hari</label><br>
                        <div class="d-flex">
                            @foreach($hari AS $h)
                            <div class="form-check flex-fill mr-1">
                                <input class="form-check-input ck{{$h}}" id="cb{{$h}}" type="checkbox" name="hari[]" value="{{$h}}">
                                <label class="form-check-label pl-0" for="cb{{$h}}"><b>{{ucwords($h)}}</b></label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    <div class="form-group text-center">
                        <button type=button class="btn btn-primary" onclick="save()"><i class="fa fa-save mr-2"></i>Simpan</button>
                        <button type=button class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{site_url('assets/plugins/clockpicker/clockpicker.min.css')}}">
<style>
    th,
    tr {
        vertical-align: middle !important;
    }
</style>
@end

@section('js')
<script src="{{site_url('assets/plugins/clockpicker/clockpicker.min.js')}}"></script>
<script>
    // DECLARATION
    var url = "{{site_url('api/internal/jam_kerja')}}";
    var token = "{{$this->session->auth['token']}}";
    // INIT
    $(".clockpicker").clockpicker({
        donetext: 'Pilih',
        autoclose: true
    });
    // EVENTS
    $("#mForm").on('hidden.bs.modal', function() {
        $(this).trigger('reset').find("[name=id]").val('');
    }).on('shown.bs.modal', function() {
        $("[name=nama]").focus()
    });
    // FUNCTIONS
    function save() {
        var valid = true;
        $('[required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid');
                valid = false;
            } else {
                $(this).removeClass('is-invalid');
            }
        });

        if (!valid) {
            return;
        }

        $.ajax({
            url: url + '/save/' + token,
            method: 'POST',
            dataType: 'json',
            data: $("#mForm form").serializeArray(),
            success: function(result) {
                if (result.status == 'success') {
                    location.reload();
                }
            },
            error: function(xhr, status, err) {

            }
        });
    }

    function edit(id) {
		$.getJSON(url+'/get/'+token+'?id='+id, function(result){
			if (result) {
				$('[name=id]').val(result.id);
				$('[name=nama]').val(result.nama);
				$('[name=jam_masuk]').val(result.jam_masuk);
                $('[name=jam_pulang]').val(result.jam_pulang);
                $.each(JSON.parse(result.hari), function(i,v){
                    $('.ck'+v).prop('checked', true);
                });
				$('#mForm').modal('show');
			}
		});
	}

    function disable(id, $isDisabled) {
		$.getJSON(url+'/disable/'+token+'?id='+id+'&par='+!$isDisabled,function(result){
			if(result.status == 'success'){
				Toast.fire('Sukses!', result.message,'success');
				location.reload();
			}
		});
	}
</script>
@end