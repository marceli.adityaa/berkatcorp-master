@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('tabungan')}}">Tabungan</a></li>
        <li class="breadcrumb-item active">Manajemen Data</a></li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header tx-bold">Tabungan Pegawai</div>
    <div class="card-body">
        <!-- TOOLBAR -->
        <div id="toolbar">
            <button class="btn btn-primary" data-toggle="modal" data-target="#mForm"><i class="fa fa-plus mr-2"></i>Tambah Transaksi</button>
        </div>
        <table class="table table-light table-js table-stripped" data-toolbar="#toolbar" data-pagination="true" data-side-pagination="server" data-search="true" data-search-on-enter-key="true" data-show-refresh="true" data-url="{{site_url('api/internal/tabungan/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
            <thead>
                <tr>
                    <th class="text-center" data-formatter="formatNomor">No.</th>
                    <th class="text-center" data-field="aksi">Aksi</th>
                    <th class="text-center" data-field="nama">Pegawai</th>
                    <th class="text-center" data-field="nominal">Nominal</th>
                    <th class="text-center" data-field="jenis">Jenis Tabungan</th>
                    <th class="text-center" data-field="tanggal_transaksi" data-formatter="formatDate">Tanggal Transaksi</th>
                    <th class="text-center" data-field="label">Kas</th>
                    <th class="text-center" data-formatter="formatArus">Arus</th>
                    <th class="text-left" data-formatter="formatSumber">Detail Entri</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="mForm" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header tx-bold">Formulir Isian Tabungan</div>
            <div class="modal-body">
                <form action="#">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label>Pegawai</label>
                        <select name="pegawai_id[]" class="form-control select2-pegawai" required></select>
                        <p class="form-text"></p>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12 col-md-4">
                            <label>Jumlah Nominal</label>
                            <input type="number" step="1000" name="nominal" class="form-control" min="0" placeholder="Nominal" required>
                            <p class="form-text"></p>
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label>Jenis Tabungan</label>
                            <select name="jenis" class="form-control">
                                <option value="harian" selected>Harian</option>
                                <option value="mingguan">Mingguan</option>
                                <option value="bulanan">Bulanan</option>
                            </select>
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label>Arus Transaksi</label>
                            <select name="arus" class="form-control">
                                <option value="in" selected>Setor</option>
                                <option value="out">Tarik</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12 col-md-4">
                            <label>Kas</label>
                            <select name="kas_id" class="form-control" required>
                                <option value="">- Pilih Kas -</option>
                                @foreach($kas AS $k)
                                <option value="{{$k->id}}">{{$k->label}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label>Tanggal Transaksi</label>
                            <input type="date" name="tanggal_transaksi" class="form-control" value="{{date('Y-m-d')}}" placeholder="Tanggal Transaksi">
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label>Keterangan (opsional)</label>
                            <input type="text" name="keterangan" class="form-control" placeholder="Masukkan Keterangan">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="save()">Simpan Transaksi</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    url = "{{site_url('api/internal/tabungan')}}";
    token = "{{$this->session->auth['token']}}";
    // INITS
    $(".select2-pegawai").prop("multiple", "multiple").select2({
        placeholder: "Pilih Pegawai",
        minimumInputLength: 3,
        allowClear: true,
        dropdownParent: $('#mForm'),
        ajax: {
            url: "{{site_url('api/internal/pegawai/get_select2_data/'.$this->session->auth['token'])}}",
            dataType: "json",
            delay: 600
        }
    });

    // EVENTS
    $("#mForm").on("hidden.bs.modal", function() {
        $("#mForm form").trigger("reset").find('[disabled]').prop('disabled', false);
        $(".select2-pegawai").val(null).prop('multiple', true).trigger("change");
    });

    // FUNCTIONS
    function save() {
        // Validate
        var valid = true;
        $('[required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }

        // Save
        $.ajax({
            url: url + '/save/' + token,
            method: 'POST',
            data: $('#mForm form').serializeArray(),
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', result.message, 'success');
                    $('.table-js').bootstrapTable('refresh');
                    $("#mForm").modal("hide");
                } else {
                    Swal.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function edit(id) {
        $.getJSON(url + '/get/' + token + '?callback=callback_get&id=' + id, function(result) {
            if (result) {
                var opt = '<option value="' + result.id + '" selected>' + result.nama + '</option>';
                $('[name=id]').val(id);
                $('.select2-pegawai').html(opt).prop('disabled', true).trigger('change');
                $('[name=keterangan]').val(result.keterangan);
                $('[name=tanggal_transaksi]').val(moment(result.tanggal_transaksi).format('YYYY-MM-DD'));
                $('[name=nominal]').val(result.nominal);
                $('[name=arus] option[value=' + result.arus + ']').prop('selected', true);
                $('[name=jenis] option[value=' + result.jenis + ']').prop('selected', true);
                $('[name=kas_id] option[value=' + result.kas_id + ']').prop('selected', true);
                if(result.sumber == 'absensi'){
                    $('[name=arus]').prop('disabled', true);
                }
                $('#mForm').modal('show');
            }
        });
    }

    function remove(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Aksi ini tidak dapat diurungkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus data!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url + '/delete/' + token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', result.message, 'success');
                            $("#mForm").collapse("hide");
                            $('.table-js').bootstrapTable('refresh');
                        } else {
                            Toast.fire('Error!', result.error, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        })
    }

    function info() {
        Swal.fire('info', 'Tabungan yang dientri melalui absensi tidak dapat dihapus dari halaman ini', 'info');
    }

    function formatArus(value, row, index, field) {
        return row.arus == "in" ? '<span class="badge badge-success"><i class="fa fa-download mr-1"></i>setor</span>' : '<span class="badge badge-danger"><i class="fa fa-upload mr-1"></i>tarik</span>';
    }

    function formatSumber(value, row, index, field) {
        return "<b>" + row.sumber + "</b><br>" + (row.insert_time);
    }
</script>
@end