@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Tabungan</a></li>
    </ol>
</nav>
@end

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Saldo Bulan Ini</span><br>
                <h3 class="mt-2 tx-white tx-bold">Rp {{monefy($widget->setor-$widget->tarik, false)}}</h3>
                <i class="fa fa-exchange widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Setor</span>
                <h3 class="mt-2 tx-white tx-bold">Rp {{monefy($widget->setor, false)}}</h3>
                <i class="fa fa-download widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Tarik</span>
                <h3 class="mt-2 tx-white tx-bold">Rp {{monefy($widget->tarik, false)}}</h3>
                <i class="fa fa-upload widget-icon"></i>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body py-3 px-3">
        <a href="{{site_url('tabungan/data')}}" class="btn btn-primary mr-2"><i class="icon-book-open mr-2"></i>Data Tabungan</a>
        <!-- <a href="{{site_url('tabungan/data')}}" class="btn btn-primary"><i class="icon-book-open mr-2"></i>Laporan</a> -->
    </div>
</div>

<div class="card mt-3">
    <div class="card-body px-5 py-5">
        <span class="tx-bold tx-uppercase">Saldo tabungan tahun 2019</span>
        <canvas id="chart1" width="100%" height="35px"></canvas>
    </div>
</div>

<div class="row mt-3">
    <div class="col-12 col-lg-6">
        <div class="card">
            <div class="card-header tx-bold tx-uppercase">Transaksi Terakhir</div>
            <div class="card-body px-0 py-0">
                <table class="table table-white">
                    @foreach($last_transaction AS $lt)
                    <tr>
                        <td class="text-center">
                            <i class="tx-20 fa {{$lt->arus=='in'?'fa-download tx-success':'fa-upload tx-danger'}}"></i>
                        </td>
                        <td>
                            <span class="tx-bold tx-12">{{datify($lt->tanggal_transaksi,'d/m/Y')}}</span><br>
                            <span>{{$lt->nama}}</span>
                        </td>
                        <td class="text-right tx-18 pr-4">Rp {{monefy($lt->nominal,false)}}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="card">
            <div class="card-header tx-bold tx-uppercase">Penabung Terbaik Bulan Ini</div>
            <div class="card-body px-0 py-0">
                <table class="table table-white">
                    @foreach($top_penabung AS $in=>$tp)
                    <tr>
                        <td class="text-center"><i class="tx-20 fa {{$in=='0'?'fa-star tx-warning':'fa-trophy'}}"></i></td>
                        <td><span>{{$tp->nama}}</span></td>
                        <td class="text-right tx-20 pr-4">Rp {{monefy($tp->nominal, false)}}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/chartjs/chart.min.css')}}">
<style>
    td {
        vertical-align: middle
    }

    .widget-icon {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 7em;
    }
</style>
@end

@section('js')
<script src="{{base_url('assets/plugins/chartjs/chart.min.js')}}"></script>
<script>
    var myLineChart = new Chart($("#chart1"), {
        type: 'bar',
        data: {
            labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            datasets: [{
                    label: 'Setor',
                    data: [20, 20, 25, 60, 61, 67, 45, 67, 76, 90, 34, 67],
                    backgroundColor: 'rgba(107, 186, 255,.2)',
                    borderColor: 'rgba(107, 186, 255)',
                    pointBackgroundColor: 'rgba(107, 186, 255)'
                },
                {
                    label: 'Tarik',
                    data: [10, 20, 30, 40, 50, 87, 95, 97, 81, 20, 45, 56],
                    backgroundColor: 'rgba(255, 107, 107,.2)',
                    borderColor: 'rgba(255, 107, 107)',
                    pointBackgroundColor: 'rgba(255, 107, 107)'
                }
            ]
        }
    });
</script>
@end