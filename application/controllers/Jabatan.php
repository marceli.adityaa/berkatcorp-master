<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jabatan extends MY_Controller_admin
{
	public $class_id = 'jbt';

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->render('jabatan/index');
	}
}
