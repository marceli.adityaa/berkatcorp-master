<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller_admin
{
	public $class_id = 'dsb';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->render('dashboard/index');
		// dump(json_decode($this->session->auth['hak_akses'])->mstr);
	}
}