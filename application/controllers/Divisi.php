<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Divisi extends MY_Controller_admin
{
	public $class_id = 'dvs';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perusahaan_model', 'perusahaan');
	}

	public function index()
	{
		$data['perusahaan'] = $this->perusahaan->order_by('nama')->get_many_by(array('is_disabled'=>0));
		$this->render('divisi/index', $data);
	}
}
