<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi extends MY_Controller_admin
{
    public $class_id = 'abs';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Absensi_model', 'absensi');
    }

    public function index()
    {
        $data['widget'] = $this->absensi->get_widget();
        $data['graph'] = $this->absensi->get_graph();
        $this->render('absensi/index', $data);
    }

    public function buku()
    {
        $this->render('absensi/buku');
    }

    public function data()
    {
        $this->render('absensi/data');
    }

    public function add()
    {
        $this->load->model('Kas_model', 'kas');
        $this->load->model('Pegawai_model', 'pegawai');
        # Get kelompok
        $data['kelompok'] = $this->pegawai->select("pegawai_kelompok.id, pegawai_kelompok.nama")->join("pegawai_kelompok", "kelompok_id=pegawai_kelompok.id")->group_by("kelompok_id")->order_by('pegawai_kelompok.nama')->get_many_by(array('tanggal_masuk<=' => date('Y-m-d'), 'pegawai.is_disabled' => 0));
        # Generate html option
        $kas = $this->kas->get_many_by(array('is_disabled' => 0));
        $data['kasOption'] = "<option value=''>Pilih Kas</option>";
        foreach ($kas as $k) {
            $data['kasOption'] .= "<option value='" . $k->id . "'>" . $k->label . "</option>";
        }
        $this->render('absensi/form', $data);
    }

    public function edit($tanggal = '')
    {
        if (!$tanggal)
            show_404();

        $this->load->model('Kas_model', 'kas');

        $data['absensi'] = $this->absensi->get_data_absensi($tanggal);
        $data['kas']     = $this->kas->get_many_by(array('is_disabled' => 0));
        $data['tanggal'] = $tanggal;

        if (!$data)
            show_404();

        $this->render('absensi/form_edit', $data);
    }

    public function insert()
    {
        # Load model
        $this->load->model('Overtime_model', 'overtime');
        $this->load->model('Tabungan_model', 'tabungan');
        # Recieve data
        $dataAbsensi = $this->input->post('data');
        $tanggal = $this->input->post('tanggal');

        if (empty($tanggal))
            show_404();

        $dataOvertime = array();
        $dataTabungan = array();

        foreach ($dataAbsensi as $index => $d) {
            # Lengkapi data
            $dataAbsensi[$index]['tanggal'] = $tanggal;
            $dataAbsensi[$index]['scanlog_masuk'] = empty($d['scanlog_masuk']) || $d['scanlog_masuk'] == '-' ? null : $d['scanlog_masuk'];
            $dataAbsensi[$index]['scanlog_pulang'] = empty($d['scanlog_pulang']) || $d['scanlog_pulang'] == '-' ? null : $d['scanlog_pulang'];
            $dataAbsensi[$index]['overtime_ref_id'] = null;
            $dataAbsensi[$index]['tabungan_ref_id'] = null;
            $dataAbsensi[$index]['keterlambatan'] = $d['kehadiran'] != 'hadir' ? 0 : $d['keterlambatan'];

            # Cek
            $ref_id = uniqchar(5);

            # Tabungan
            if ($d['kehadiran'] == 'hadir' && isset($d['nominal_tabungan']) && !empty($d['nominal_tabungan'])) {
                $tmp = array(
                    'ref_id' => $ref_id,
                    'pegawai_id' => $d['pegawai_id'],
                    'arus' => 'in',
                    'tanggal_transaksi' => $tanggal,
                    'jenis' => 'harian',
                    'nominal' => $d['nominal_tabungan'],
                    'sumber' => 'absensi',
                    'kas_id' => $d['kas_id']
                );
                $dataTabungan[] = $tmp;
                $dataAbsensi[$index]['tabungan_ref_id'] = $ref_id;
            }

            # Unset
            unset($dataAbsensi[$index]['nominal_tabungan']);
            unset($dataAbsensi[$index]['kas_id']);

            # Jika Terlambat
            if ($d['kehadiran'] == 'hadir' && !empty($d['keterlambatan'])) {
                $durasi = $d['keterlambatan'];
                $waktuMulai = datify('Y-m-d h:i', $tanggal . ' 08:00');
                $tmp = array(
                    'ref_id' => $ref_id,
                    'pegawai_id' => $d['pegawai_id'],
                    'arus' => 'out',
                    'kategori' => 'kehadiran',
                    'durasi' => $durasi,
                    'waktu_mulai' => $waktuMulai,
                    'waktu_selesai' => date('Y-m-d h:i', strtotime('+' . $durasi . ' minutes', strtotime($waktuMulai))),
                    'keterangan' => "Terlambat {$durasi} menit pada tanggal $tanggal",
                    'sumber' => 'absensi'
                );
                $dataOvertime[] = $tmp;
                $dataAbsensi[$index]['overtime_ref_id'] = $ref_id;
                continue;
            }

            # Jika Alpa
            if ($d['kehadiran'] == 'alpa') {
                # Potong Overtime
                $durasi = 8 * 60;
                $waktuMulai = datify('Y-m-d h:i', $tanggal . ' 08:00');
                $tmp = array(
                    'ref_id' => $ref_id,
                    'pegawai_id' => $d['pegawai_id'],
                    'arus' => 'out',
                    'kategori' => 'kehadiran',
                    'durasi' => $d['keterlambatan'],
                    'waktu_mulai' => $waktuMulai,
                    'waktu_selesai' => date('Y-m-d h:i', strtotime('+' . $d['keterlambatan'] . ' minutes', strtotime($waktuMulai))),
                    'keterangan' => 'Tidak masuk pada tanggal ' . $tanggal,
                    'sumber' => 'absensi'
                );
                $dataOvertime[] = $tmp;
                $dataAbsensi[$index]['overtime_ref_id'] = $ref_id;
                continue;
            }
        }

        # Simpan data Absensi
        if ($dataAbsensi)
            $this->absensi->batch_insert($dataAbsensi);
        # Simpan data Overtime
        if ($dataOvertime)
            $this->overtime->batch_insert($dataOvertime);
        # Simpan data Tabungan
        if ($dataTabungan)
            $this->tabungan->batch_insert($dataTabungan);

        $this->message('Absensi berhasil ditambah', 'success');
        $this->go('absensi/buku');
    }

    public function update()
    {
        # Load model
        $this->load->model('Overtime_model', 'overtime');
        $this->load->model('Tabungan_model', 'tabungan');

        $tanggal = $this->input->post('tanggal');
        $data = $this->input->post('data');

        if (empty($tanggal))
            show_404();

        # Ambil saldo
        foreach ($data as $index => $d) {
            $old = $this->absensi->get_by(array('pegawai_id' => $d['pegawai_id'], 'tanggal' => $tanggal));
            # Hapus overtime
            if (!empty($old->overtime_ref_id))
                $this->overtime->delete_by(array('ref_id' => $old->overtime_ref_id));
            if (!empty($old->tabungan_ref_id))
                $this->tabungan->delete_by(array('ref_id' => $old->tabungan_ref_id));

            # Proses update
            $ref_id = uniqchar(5);
            # Tabungan
            if ($d['kehadiran'] == 'hadir' && isset($d['nominal_tabungan']) && !empty($d['nominal_tabungan'])) {
                $insert = array(
                    'ref_id' => $ref_id,
                    'pegawai_id' => $d['pegawai_id'],
                    'arus' => 'in',
                    'tanggal_transaksi' => $tanggal,
                    'jenis' => 'harian',
                    'nominal' => $d['nominal_tabungan'],
                    'sumber' => 'absensi',
                    'kas_id' => $d['kas_id']
                );
                $this->tabungan->insert($insert);
                $d['tabungan_ref_id'] = $ref_id;
            }

            unset($d['nominal_tabungan']);
            unset($d['kas_id']);
            # Overtime
            if ($d['kehadiran'] == 'hadir' && $d['keterlambatan'] > 0) {
                $insert = array(
                    'ref_id' => $ref_id,
                    'pegawai_id' => $d['pegawai_id'],
                    'waktu_mulai' => datify($tanggal . ' 08:00', 'Y-m-d h:i'),
                    'waktu_selesai' => date('Y-m-d h:i', strtotime('+' . $d['keterlambatan'] . ' minutes', strtotime(datify($tanggal . ' 08:00', 'Y-m-d h:i')))),
                    'durasi' => $d['keterlambatan'],
                    'keterangan' => 'Terlambat pada tanggal ' . $tanggal,
                    'kategori' => 'kehadiran',
                    'arus' => 'out',
                    'sumber' => 'absensi'
                );
                $this->overtime->insert($insert);
                $d['overtime_ref_id'] = $ref_id;
            }

            if ($d['kehadiran'] == 'alpa') {
                # Tidak ada saldo potong overtime
                $insert = array(
                    'ref_id' => $ref_id,
                    'pegawai_id' => $d['pegawai_id'],
                    'waktu_mulai' => datify($tanggal . ' 08:00', 'Y-m-d h:i'),
                    'waktu_selesai' => date('Y-m-d h:i', strtotime('+' . (8 * 60) . ' minutes', strtotime(datify($tanggal . ' 08:00', 'Y-m-d h:i')))),
                    'durasi' => 8 * 60,
                    'keterangan' => 'Tidak masuk kerja pada tanggal ' . $tanggal,
                    'kategori' => 'kehadiran',
                    'arus' => 'out',
                    'sumber' => 'absensi'
                );
                $this->overtime->insert($insert);
                $d['overtime_ref_id'] = $ref_id;
            }

            # Update absen
            if ($d['kehadiran'] != 'hadir')
                $d['keterlambatan'] = 0;

            $this->absensi->update_by(array('pegawai_id' => $d['pegawai_id'], 'tanggal' => $tanggal), $d);
        }

        $this->message('Data berhasil disunting', 'success');
        $this->go('absensi/buku');
    }

    public function cetak()
    {
        $filter = $this->input->post();
        $filter['tanggal_akhir'] = date('Y-m-d', strtotime($filter['tanggal_akhir']. ' +1 day'));
        if(!isset($filter['kolom'])){
            $filter['kolom'] = array();
        }
        $this->absensi->cetak($filter);
    }
}
