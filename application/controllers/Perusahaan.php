<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perusahaan extends MY_Controller_admin
{
	public $class_id = 'prhs';

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->render('perusahaan/index');
	}
}
