<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime extends MY_Controller_admin
{
    public $class_id = 'ovt';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Overtime_model', 'model');
    }

    public function index()
    {
        $data['notif'] = $this->db->where('status',0)->count_all_results('overtime_pengajuan');
        $data['today'] = $this->model->get_today_overtime();
        $data['widget'] = $this->model->get_widget();
        $data['graph'] = $this->model->get_graph();
        $this->render('overtime/index', $data);
    }
    
    public function data()
    {
        $this->render('overtime/data');
    }

    public function pengajuan()
    {
        $this->render('overtime/pengajuan');
    }
}
