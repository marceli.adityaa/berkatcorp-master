<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekening extends MY_Controller_admin
{
	public $class_id = 'rek';
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Perusahaan_model', 'perusahaan');
        $this->load->model('Bank_model', 'bank');
	}

	public function index()
	{
        $data['perusahaan'] = $this->perusahaan->get_active();
        $data['bank'] = $this->bank->get_active();
		$this->render('rekening/index', $data);
	}
}