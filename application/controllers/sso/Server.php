<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Server extends MY_Controller {
	private $url;
	private $allowedHosts = ['127.0.0.1', '128.1.1.85', '128.1.1.130', 'berkatcorp.com', 'master.berkatcorp.com','pegawai.berkatcorp.com', 'bas.berkatcorp.com', 'transport.berkatcorp.com'];
	private $localServerAddress = ['128.1.1.85', '127.0.0.1', '128.1.1.130', 'localhost'];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model', 'user');
		$this->load->model('Credential_model', 'credential');
	}

	public function index()
	{
		if ($this->hasCredential()) {
			$this->render('sso/landing');
		} else {
			$this->signin();
		}
	}

	public function signin()
	{
		$source = $this->input->get('source');
		$source = empty($source)?$this->config->item('sso_address'):$source;

		$this->url = generate_save_url($source);

		if (!$this->validateSource()) {
			show_error('Sumber alamat tidak valid', '404');
		}

		if($this->hasCredential()) {
			$this->returnRequest();
		}

		$data['source'] = !empty($this->url) ? $this->url : site_url('signin');
		$this->render('sso/signin', $data);
	}

	public function signout()
	{
		$token = $this->getToken();
		$this->credential->update_by(array('token'=>$token), array('status'=>0));
		$this->session->sess_destroy();
		$this->go('signin');
	}

	public function auth()
	{
		$data = $this->input->post();
		
		if (!isset($data['source']) OR empty($data['source'])) {
			die(json_encode(array('error'=>'Sumber tidak valid')));
		}

		$this->url = generate_save_url($data['source']);
		unset($data['source']);
		
		# Simple validasi
		if (array_has_empty($data)) {
			die(json_encode(array('error'=>'Username dan Password tidak boleh kosong')));
		}

		# Validasi source
		if (!$this->validateSource()) {
			die(json_encode(array('error'=>'Sumber tidak valid')));
		}

		$usr = $this->user->get_by(array('username'=>$data['username'], 'is_disabled'=>0));
		if (!empty($usr)) {
			if (password_verify($data['password'], $usr->password)) {
				# Tulis credential
				$credential = $this->generateCredential();
				$credential['user_id'] = $usr->id;
				# Strore credential
				$this->credential->insert($credential);
				$this->stroreCredential($credential);
				$this->returnRequest(true);
			} else {
				die(json_encode(array('error'=>'Username atau password salah')));

			}
		} else {
			die(json_encode(array('error'=>'Username atau password salah')));
		}
	}

	public function get_credential()
	{
		$token  = $this->input->get('token');
		$this->url = $this->input->get('source');

		if (!$this->validateSource()) {
			echo(json_encode(array('error'=>'Sumber tidak valid')));
			return;
		}

		$c = $this->credential
		->select('user.id AS id, pegawai_id, nama, hak_akses, role, token, credential.status AS status, expired_at')
		->join('user', 'user_id = user.id')
		->join('pegawai', 'pegawai_id = pegawai.id')
		->as_array()
		->get_by('token', $token);

		if ($c['status'] == 0) {
			echo(json_encode(array('error'=>'Token kadaluarsa')));
			return;
		}

		if (datify($c['expired_at'], 'Y-m-d h:i:s') < date('Y-m-d h:i:s')) {
			$this->credential->update_by(array('token'=>$token), array('status'=>0));
			echo(json_encode(array('error'=>'Token kadaluarsa')));
			return;
		}

		echo(json_encode($c));
		return;
	}

	public function token_auth()
	{
		# Get source
		$this->url = $this->input->get('source');
		# Cek apakah source valid
		if(!$this->validateSource()) {
			die(json_encode(array('status'=>'error','message'=>'source tidak valid')));
		}

		# Cek token
		$token = $this->input->get('token');
		$cred = $this->credential->get_by(array('token'=>$token));
		if(!$cred) {
			die(json_encode(array('status'=>'error','message'=>'token tidak valid')));
		}

		if($cred->status == '0' OR $cred->expired_at <= date('Y-m-d h:i:s')) {
			die(json_encode(array('status'=>'error','message'=>'token kadaluarsa')));
		}

		die(json_encode(array('status'=>'success')));
	}

	private function validateSource()
	{
		$p = parse_url($this->url);
		return isset($p['host']) && in_array($p['host'], $this->allowedHosts);
	}

	private function hasCredential()
	{
		$c = $this->session->credential;
		return !empty($c);
	}

	private function returnRequest($isJson = false)
	{	
		$source = site_url('sso/server');

		if (!empty($this->url)) {
			# Get token
			$token = $this->getToken();
			# Generate acc_req link
			$p = parse_url($this->url);
			
			# Jika host adalah lokal
			if (in_array($p['host'], $this->localServerAddress)) {
				$p['host'] .= '/berkatcorp-master';
			}
			
			$p = prep_url($p['host'].'/sso/client/acc_req').'?token='.$token.'&source='.urlencode($this->url); 
			# Redirect
			$source = $p;
		}

		if ($isJson) {
			echo json_encode(array('source'=>$source));
		} else {
			$this->go($source, true, 'refresh');
		}
	}

	private function getToken()
	{
		if ($this->hasCredential()) {
			return $this->session->credential['token'];
		}

		return md5(date('Ymdhis').''.uniqid(rand(), true));
	}

	private function generateCredential()
	{
		$c['token']    	 = $this->getToken();
		$c['expired_at'] = date("Y-m-d H:i:s", strtotime('+24 hours'));
		return $c;
	}

	private function stroreCredential($c = array())
	{
		if($this->session->credential)
			$this->session->unset_userdata('credential');
		$this->session->set_userdata('credential', $c);
	}
}