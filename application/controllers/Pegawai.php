<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends MY_Controller_admin
{
	public $class_id = 'pgw';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perusahaan_model', 'perusahaan');
		$this->load->model('Jabatan_model', 'jabatan');
		$this->load->model('Kelompok_kerja_model', 'kelompok');
		$this->load->model('Jam_kerja_model', 'jamker');
	}

	public function index()
	{
		$data['perusahaan'] = $this->perusahaan->order_by('nama')->get_many_by(array('is_disabled'=>0));
		$data['jabatan'] = $this->jabatan->order_by('nama')->get_many_by(array('is_disabled'=>0));
		$data['kelompok'] = $this->kelompok->order_by('nama')->get_many_by(array('is_disabled'=>0));
		$data['jamker'] = $this->jamker->order_by('nama')->get_many_by(array('is_disabled'=>0));
		$this->render('pegawai/index', $data);
	}
}
