<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tabungan extends MY_Controller_admin
{
	public $class_id = 'tbg';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tabungan_model', 'tabungan');
		$this->load->model('Kas_model', 'kas');
	}

	public function index()
	{
		$data['widget'] = $this->tabungan->get_widget();
		$data['last_transaction'] = $this->tabungan->join('pegawai','pegawai_id=pegawai.id')->limit(5)->order_by('tabungan.tanggal_transaksi', 'DESC')->get_all();
		$data['top_penabung'] = $this->tabungan->get_top_penabung();
		$this->render('tabungan/index', $data);
	}

	public function data()
	{
		$data['kas'] = $this->kas->order_by('label')->get_all();
		$this->render('tabungan/data', $data);
	}
}