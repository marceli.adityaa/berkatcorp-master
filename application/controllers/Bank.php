<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bank extends MY_Controller_admin
{
	public $class_id = 'bank';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perusahaan_model', 'perusahaan');
	}

	public function index()
	{
		$data['perusahaan'] = $this->perusahaan->get_active();
		$this->render('bank/index', $data);
	}
}