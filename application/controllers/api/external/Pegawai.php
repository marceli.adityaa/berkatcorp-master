<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends MY_Controller_api
{
	protected $auth_type = 'external';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pegawai_model','model');
	}

	
	public function set_overtime(){
		$param = $this->input->get();

        # Ambil data pegawai berdasarkan tanggal
		$this->db->select('a.*');
		// $this->db->where_in('b.kelompok_id', array(1,12, 14));
		$this->db->where('a.kehadiran', 'hadir');
		$this->db->where('a.scanlog_pulang <>', 'NULL');
		$this->db->where('a.tanggal>=', $param['startdate']);
		$this->db->where('a.tanggal<=', $param['enddate']);
		$this->db->where('a.pegawai_id', $param['id']);
		$this->db->join('pegawai b', 'a.pegawai_id = b.id');
		$absen = $this->db->get('absensi a')->result_array();
		
		$data = array();
		# Hitung overtime
		foreach($absen as $row){
			if($row['scanlog_pulang'] > $row['jam_pulang']){
				$jam_pulang = $row['tanggal'].' '.$row['jam_pulang'];
				$scanlog_pulang = $row['tanggal'].' '.$row['scanlog_pulang'];
				$durasi = (strtotime($scanlog_pulang) - strtotime($jam_pulang))/60;
				if($durasi > 30){
					# cek apakah data overtime sudah tersedia atau belum
					$this->db->where('arus', 'in');
					$this->db->where('pegawai_id', $row['pegawai_id']);
					$this->db->where('DATE_FORMAT(waktu_mulai, "%Y-%m-%d") =', $param['tanggal']);
					$cek = $this->db->get('overtime')->result();
					if(!$cek){
						$log = array(
							'pegawai_id' => $row['pegawai_id'],
							'waktu_mulai' => $jam_pulang,
							'waktu_selesai' => $scanlog_pulang,
							'durasi' => $durasi,
							'kategori' => 'lembur',
							'arus' => 'in',
							'sumber' => 'entri',
						);
						array_push($data, $log);
					}
				}
			}
		}

		# Batch Insert
		if($data){
			$result = $this->db->insert_batch('overtime', $data);
		}else{
			$result = false;
		}
		echo json_encode($result);
	}

/*
	public function set_overtime_shift(){
		$param = $this->input->get();

        # Ambil data pegawai berdasarkan tanggal
		$this->db->select('a.*');
		$this->db->where('pegawai_id', $param['id']);
		$this->db->where('a.kehadiran', 'hadir');
		$this->db->where('a.scanlog_pulang <>', 'NULL');
		$this->db->where('a.tanggal >=', $param['start']);
		$this->db->where('a.tanggal <=', $param['end']);
		$this->db->join('pegawai b', 'a.pegawai_id = b.id');
		$absen = $this->db->get('absensi a')->result_array();
		$data = array();
		# Hitung overtime
		foreach($absen as $row){
			if($row['scanlog_masuk'] < $row['jam_masuk']){
				$scanlog_masuk = $row['tanggal'].' '.$row['scanlog_masuk'];
				$jam_masuk = $row['tanggal'].' '.$row['jam_masuk'];
				$durasi = (strtotime($jam_masuk) - strtotime($scanlog_masuk))/60;
				if($durasi > 4){
					# cek apakah data overtime sudah tersedia atau belum
					if($durasi > 30){
						$durasi = 30;
					}
					
					$log = array(
						'pegawai_id' => $row['pegawai_id'],
						'waktu_mulai' => $scanlog_masuk,
						'waktu_selesai' => $jam_masuk,
						'durasi' => $durasi,
						'kategori' => 'shift pagi',
						'arus' => 'in',
						'sumber' => 'entri',
					);
					array_push($data, $log);
				}
			}
		}

		# Batch Insert
		if($data){
			$result = $this->db->insert_batch('overtime', $data);
		}else{
			$result = false;
		}
		echo json_encode($result);
	}
	*/

}