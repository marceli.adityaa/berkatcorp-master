<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekening extends MY_Controller_api
{
	protected $auth_type = 'external';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Rekening_model','model');
    }
    
    public function get_active(){
        $param = $this->input->get();
        # Autentikasi Token
		if(!$this->validateToken($param['token'])) {
            show_404();
        }
        $result = $this->model->get_active($param);
        echo json_encode($result);
    }

}