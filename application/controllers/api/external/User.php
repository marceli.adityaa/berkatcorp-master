<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller_api
{
	protected $auth_type = 'external';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model','model');
	}

}