<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Arus_kas extends MY_Controller_api
{
	protected $auth_type = 'external';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Arus_kas_model','model');
        $this->load->model('Arus_kas_mutasi_model','mutasi');
    }
    
  //   public function get_active(){
  //       $param = $this->input->get();
  //       # Autentikasi Token
		// if(!$this->validateToken($param['token'])) {
  //           show_404();
  //       }
  //       $result = $this->model->get_active($param);
  //       echo json_encode($result);
  //   }

  //   public function get_rekening(){
  //       $param = $this->input->get();
  //       # Autentikasi Token
		// if(!$this->validateToken($param['token'])) {
  //           show_404();
  //       }
  //       $result = $this->model->get_rekening($param);
  //       echo json_encode($result);
  //   }

    public function get_data_persetujuan(){
        $param = $this->input->get();
        # Autentikasi Token
        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        $result = $this->model->get_data_persetujuan($param);
        echo json_encode($result);
    }

    public function get_data_rekap(){
        $param = $this->input->get();
        # Autentikasi Token
        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        $result['data'] = $this->model->get_data_rekap($param);
        $result['saldo_awal'] = $this->model->get_saldo_awal($param);
        echo json_encode($result);
    }

    public function get_data_export(){
        $param = $this->input->get();
        # Autentikasi Token
        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        $result['data'] = $this->model->get_data_rekap($param);
        $result['saldo_awal'] = $this->model->get_saldo_awal_export($param);
        echo json_encode($result);
    }

    public function get_jumlah_pengajuan(){
        $param = $this->input->get();
        # Autentikasi Token
        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        $result = $this->model->get_jumlah_pengajuan($param);
        echo json_encode($result);
    }

    public function get_detail_transaksi(){
        $param = $this->input->get();
        $id = $this->input->post('id');
        # Autentikasi Token
        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        $result = $this->model->get_detail($id);
        echo json_encode($result);
    }

    public function insert(){
        $data = $this->input->post();
        $token = $this->input->get('token');
        # Autentikasi Token
		if(!$this->validateToken($token)) {
            show_404();
        }
        $result = $this->model->insert($data);
        echo json_encode($result);
    }

    public function update_transaksi(){
        $data = $this->input->post();
        $token = $this->input->get('token');
        $id = $data['id'];
        unset($data['id']);
        # Autentikasi Token
        if(!$this->validateToken($token)) {
            show_404();
        }
        $result = $this->model->update($id, $data);
        echo json_encode($result);
    }

    public function delete_transaksi(){
        $id = $this->input->post('id');
        $token = $this->input->get('token');
        # Autentikasi Token
        if(!$this->validateToken($token)) {
            show_404();
        }

        $isValid = $this->model->cek_validasi($id);
        if($isValid){
            $result = $this->model->delete($id);
        }else{
            $result = false;
        }
        echo json_encode($result);
    }

    # Mutasi

    public function get_data_mutasi(){
        $param = $this->input->get();
        # Autentikasi Token
        // if(!$this->validateToken($param['token'])) {
        //     show_404();
        // }
        $result = $this->mutasi->get_data($param);
        echo json_encode($result);
    }

    public function get_detail_mutasi(){
        $id = $this->input->post('id');
        $token = $this->input->get('token');
        # Autentikasi Token
        if(!$this->validateToken($token)) {
            show_404();
        }
        $result = $this->mutasi->get_detail_mutasi($id);
        echo json_encode($result);
    }

    public function insert_mutasi_akun(){
        $data = $this->input->post();
        $token = $this->input->get('token');
        # Autentikasi Token
        if(!$this->validateToken($token)) {
            show_404();
        }
        $result = $this->mutasi->insert($data);
        if($result){
            echo json_encode($this->db->insert_id());
        }else{
            echo json_encode($result);
        }
    }

    public function delete_mutasi(){
        $id = $this->input->post('id');
        $token = $this->input->get('token');
        # Autentikasi Token
        if(!$this->validateToken($token)) {
            show_404();
        }
        $result = $this->mutasi->delete($id);
        echo json_encode($result);
    }

    public function delete_detail_mutasi(){
        $id = $this->input->post('id');
        $token = $this->input->get('token');
        # Autentikasi Token
        if(!$this->validateToken($token)) {
            show_404();
        }
        $result = $this->model->delete($id);
        echo json_encode($result);
    }


}