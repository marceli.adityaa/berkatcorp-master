<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime_pengajuan extends MY_Controller_api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Overtime_model', 'model');
        $this->model->_table = 'overtime_pengajuan';
    }

    public function callback_table($data = [])
	{
		$sColor = array('secondary', 'success', 'danger');
        $sIcon = array('minus', 'check', 'times');

        foreach ($data['rows'] as $k => $v) {
            $v->nomor = $k + 1;
            $v->tanggal_verifikasi = $v->status > 0 ? datify($v->tanggal_pengajuan, 'd-M-Y, h:i')  : null;
            if ($v->status < 1) {
                $v->aksi  = "<div class='d-flex'><button class='btn btn-success btn-sm flex-fill' title='Setujui Pengajuan' onclick='setuju(" . $v->id . ")'><i class='fa fa-check'></i></button> ";
                $v->aksi .= "<button class='btn btn-danger btn-sm flex-fill' title='Tolak Pengajuan' onclick='tolak(" . $v->id . ")'><i class='fa fa-times'></i></button></div>";
            } else {
                $v->aksi  = "<div class='d-flex'><button class='btn btn-secondary btn-sm flex-fill' title='pesan verifikasi' onclick='pesan(" . $v->id . ")'><i class='fa fa-commenting'></i> pesan</button> ";
                $v->aksi .= "<button class='btn btn-warning btn-sm flex-fill' title='batalkan persetujuan' onclick='batal(" . $v->id . ")'><i class='fa fa-refresh'></i></button></div> ";
            }
            $v->status = '<span class="badge badge-' . $sColor[$v->status] . '"><i class="fa fa-' . $sIcon[$v->status] . '"></i></span>';
        }

		return $data;
	}

    public function save($token = '')
    {
        # Auth
        if(!$this->validateToken($token)) {
            show_404();
        }
        # Get data
        $param = $this->input->post();
        # Validation
        foreach ($param as $key => $value) {
            if ($key != 'pesan' && empty($value)) {
                die(json_encode(array('status' => 'error', 'message' => 'Illegal data')));
            }
        }
        # Prepare data
        $param['tanggal_verifikasi'] = date('Y-m-d h:i:s');
        $id = $param['id'];
        unset($param['id']);

        # Update data
        if ($this->model->update($id, $param)) {
            if ($param['status'] === '1') {

                $overtime = $this->model->select('pegawai_id,waktu_mulai,waktu_selesai,durasi,kategori,keterangan')->as_array()->get($id);
                $overtime['pengajuan_id'] = $id;
                $overtime['arus'] = "in";

                $this->db->insert('overtime', $overtime);
            }
            # Return
            die(json_encode(array('status' => 'success', 'message' => 'Pengajuan Berhasil Dikonfirmasi')));
        } else {
            die(json_encode(array('status' => 'error')));
        }
    }

    public function cancel($token = '')
    {
        # Auth
        if(!$this->validateToken($token)) {
            show_404();
        }
        # Proses
        $id = $this->input->post('id');
        # Validasi
        if (empty($id) or $this->model->count_by(array('id' => $id) == 0)) {
            die(json_encode(array('status' => 'error', 'message' => 'Data tidak valid')));
        }

        $overtime = $this->model->get($id);

        if ($this->model->update($id, array('status' => 0, 'pesan' => 'dibatalkan'))) {
            if ($overtime->status == 1) {
                $this->db->where('pengajuan_id', $id)->delete('overtime');
            }

            die(json_encode(array('status' => 'success', 'message' => 'Pengajuan berhasil dibatalkan')));
        }
    }
}
