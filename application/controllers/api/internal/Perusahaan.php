<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends MY_Controller_api {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perusahaan_model', 'model');
	}

	public function callback_table($data = [])
	{
		foreach ($data['rows'] as $k => $v) {
			if(isset($v->insert_time))
				$v->insert_time = time_ago($v->update_time);
			# Aksi=========================================
			$disablePar = $v->is_disabled == '1' ?true:false;
			$v->aksi  = "<div><button class='btn btn-warning btn-sm' title='Sunting' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button> ";
			$v->aksi .= "<button class='btn ".($disablePar?'btn-danger':'btn-success')." btn-sm bDisable' title='".($disablePar?'Aktifkan':'Nonaktifkan')."' onclick='disable(" . $v->id . ", ".$disablePar.")'><i class='fa ".($disablePar?'fa-ban':'fa-check')." mr-2'></i>".($disablePar?'nonaktif':'aktif')."</button></div>";
		}

		return $data;
	}
}