<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime extends MY_Controller_api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Overtime_model', 'model');
        $this->load->model('Pegawai_model', 'pegawai');
    }

    public function callback_get($data = [])
	{
		$data->nama = $this->pegawai->get($data->pegawai_id)->nama;
		return $data;
    }
    
    public function callback_table($data = [])
	{
		foreach ($data['rows'] as $k => $v) {
            if ($v->sumber != 'pengajuan' && $v->sumber != 'absensi') {
                $v->aksi  = "<div class='d-flex'>";
                $v->aksi .= "<button class='flex-fill btn btn-warning btn-sm' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button>";
                $v->aksi .= "<button class='flex-fill btn btn-danger btn-sm' onclick='remove(" . $v->id . ")'><i class='icon-trash'></i></button></div>";
            } else {
                $v->aksi  = "<button class='btn btn-block btn-warning btn-sm' onclick='help()'><i class='fa fa-question'></i></button> ";
            }
        }

		return $data;
	}

    public function get_saldo_by($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->get('id');
        $result = $this->model->get_saldo_by($id);

        echo json_encode(array('saldo' => $result));
    }

    public function save($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $data = $this->input->post();
        foreach ($data as $key => $value) {
            if ($key != 'id' && $key != 'keterangan') {
                if (empty($value)) {
                    die(json_encode(array('status' => 'error', 'message' => $key . ' tidak boleh kosong')));
                }
            }
        }

        if (!$data['id']) {
            # Insert
            # Generate data
            $dataInsert = array();
            foreach ($data['pegawai_id'] as $id) {
                $tmp = $data;
                $tmp['pegawai_id'] = $id;
                $dataInsert[] = $tmp;
            }
            $this->model->batch_insert($dataInsert);
            die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di simpan')));
        } else {
            # Update
            $id = $data['id'];
            unset($data['id']);
            $this->model->update($id, $data);
            die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di sunting')));
        }
    }

    private function hitungJumlahJam($date1, $date2)
    {
        $date1 = new DateTime($date1);
        $date2 = new DateTime($date2);
        $interval = $date1->diff($date2);

        if ($interval->h > 0) {
            if ($interval->i > 0) {
                return zerofy($interval->h) . ' Jam ' . zerofy($interval->i) . ' Menit';
            } else {
                return zerofy($interval->h) . ' Jam';
            }
        } else {
            return zerofy($interval->i) . ' Menit';
        }
    }
}
