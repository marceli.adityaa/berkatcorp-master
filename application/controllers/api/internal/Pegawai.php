<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends MY_Controller_api
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pegawai_model', 'model');
		$this->load->model('Divisi_model', 'divisi');
	}

	public function callback_get($data = [])
	{
		$data->perusahaan_id = $this->divisi->get($data->divisi_id)->perusahaan_id;
		return $data;
	}

	public function callback_table($data = [])
	{
		foreach ($data['rows'] as $k => $v) {
			if(isset($v->insert_time))
				$v->insert_time = time_ago($v->update_time);
			# Aksi=========================================
			$disablePar = $v->is_disabled == '1' ?true:false;
			$v->aksi  = "<div><button class='btn btn-warning btn-sm' title='Sunting' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button> ";
			$v->aksi .= "<button class='btn ".($disablePar?'btn-danger':'btn-success')." btn-sm bDisable' title='".($disablePar?'Aktifkan':'Nonaktifkan')."' onclick='disable(" . $v->id . ", ".$disablePar.")'><i class='fa ".($disablePar?'fa-ban':'fa-check')." mr-2'></i>".($disablePar?'nonaktif':'aktif')."</button></div>";
		}

		return $data;
	}

	public function get_select2_data($token = '')
	{
		if(!$this->validateToken($token)) {
            show_404();
        }

		$par = $this->input->get();
		$data = array(
			'results' => $this->model->select('id, nama AS text')->like(array('nama' => $par['q']))->get_many_by(array('status' => 'aktif','is_disabled'=>0)),
			'pagination' => array('more' => false)
		);
		echo json_encode($data);
	}
}
