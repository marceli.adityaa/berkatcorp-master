<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tabungan extends MY_Controller_api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Tabungan_model', 'model');
        $this->load->model('Pegawai_model', 'pegawai');
    }

    public function callback_get($data = [])
	{
		$data->nama = $this->pegawai->get($data->pegawai_id)->nama;
		return $data;
    }
    
    public function callback_table($data = [])
	{
		foreach ($data['rows'] as $k => $v) {
            $v->nominal = monefy($v->nominal, false);
            if (isset($v->insert_time))
                $v->insert_time = time_ago($v->insert_time);
            # Aksi==============================
            $v->aksi  = "<div><button class='btn btn-warning btn-sm' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button> ";
            if ($v->sumber != 'absensi') {
                $v->aksi .= "<button class='btn btn-danger btn-sm' onclick='remove(" . $v->id . ")'><i class='icon-trash'></i></button></div>";
            } else {
                $v->aksi .= "<button class='btn btn-secondary btn-sm' onclick='info()'><i class='icon-question'></i></button></div>";
            }
        }

		return $data;
	}

    public function save($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $data = $this->input->post();
        foreach ($data as $key => $value) {
            if ($key != 'id' && $key != 'keterangan') {
                if (empty($value)) {
                    die(json_encode(array('status' => 'error', 'message' => $key . ' tidak boleh kosong')));
                }
            }
        }

        if (!$data['id']) {
            # Insert
            # Generate data
            $dataInsert = array();
            foreach ($data['pegawai_id'] as $id) {
                $tmp = $data;
                $tmp['pegawai_id'] = $id;
                $dataInsert[] = $tmp;
            }
            $this->model->batch_insert($dataInsert);
            die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di simpan')));
        } else {
            # Update
            $id = $data['id'];
            unset($data['id']);
            $this->model->update($id, $data);
            die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di sunting')));
        }
    }
}
