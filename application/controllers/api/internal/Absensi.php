<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi extends MY_Controller_api
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Absensi_model', 'model');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Cuti_model', 'cuti');
    }

    public function get_absensi_group($token = '')
    {
        $filter = $this->input->get();
        $result = $this->model->get_data_absensi_group($filter);
        $hari = array('Minggu','Senen','Selasa','Rabu','Kamis','Jumat','Sabtu');
        
        foreach($result['rows'] AS $r) {
            $r->aksi = '<a href="'.site_url('absensi/edit/'.$r->tanggal).'" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>';
            $r->tanggal = $hari[datify($r->tanggal, 'w')].', '.datify($r->tanggal,'d/m/Y');
            $r->tabungan = empty($r->tabungan)?'-':'Rp '.monefy($r->tabungan, false);
        }
        
        echo json_encode($result);
    }

    public function get_daftar_pegawai($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $tanggal = $this->input->post('tanggal');

        $kelompok = $this->pegawai
        ->select('pegawai_kelompok.id AS id, pegawai_kelompok.nama AS nama')
        ->join('pegawai_kelompok','kelompok_id=pegawai_kelompok.id')
        ->group_by('kelompok_id')
        ->order_by('pegawai_kelompok.nama')
        ->get_many_by(array('tanggal_masuk<='=>$tanggal,'status'=>'aktif','pegawai.is_disabled'=>0));
        
        $cuti = array_column($this->cuti->get_many_by('tanggal_cuti', $tanggal), 'pegawai_id');
        $pegawai = array();
        foreach($kelompok AS $kel) {
            $pegawai[$kel->nama] = $this->pegawai->select('pegawai.id, ucase(pegawai.nama) as nama, jenis_pegawai, nominal_tabungan, fingerprint_id, jam_masuk,jam_pulang')->join('jam_kerja','jam_kerja.id = jamker_id')->order_by('pegawai.nama')->get_many_by(array('kelompok_id'=>$kel->id,'status'=>'aktif','pegawai.is_disabled'=>0));
            foreach($pegawai[$kel->nama] AS $peg) {
                $peg->kelompok = "kel".$kel->id;
                $peg->has_cuti = null;
                $peg->jam_masuk = datify($peg->jam_masuk, 'H:i'); 
                $peg->jam_pulang = datify($peg->jam_pulang, 'H:i'); 
                if(in_array($peg->id, $cuti)) {
                    $peg->has_cuti =  'true';
                    $peg->keterangan = $this->cuti->select('keterangan')->get_by(array('tanggal_cuti'=>$tanggal,'pegawai_id'=>$peg->id))->keterangan;
                }
            }
        }

        echo json_encode($pegawai);
    }

    public function has_absensi($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $tanggal = $this->input->get('tanggal');
        echo json_encode(array('jumlah'=>$this->model->count_by('tanggal', $tanggal)));
    }

    public function callback_table($data = array())
	{
		foreach ($data['rows'] as $k => $v) {
            $v->aksi = '<button class="btn btn-sm btn-primary" onclick="getScanlog('.$v->fingerprint_id.', \''.$v->tanggal.'\')" title="riwayat scanlog"><i class="fa fa-clock-o"></i></button>';
        }

        return $data;
	}
}
