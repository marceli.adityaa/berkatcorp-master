<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kas extends MY_Controller_api {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kas_model', 'model');
		$this->load->model('Pegawai_model', 'pegawai');
	}

	public function get_data_table(){
		$data= $this->model->get_data($this->input->get());
		echo json_encode($this->callback_table($data));
	}
	
	public function callback_get($data = [])
	{
		$data->pic = $this->pegawai->get($data->pegawai_id)->nama;
		return $data;
	}

    public function callback_table($data = [])
	{	
		foreach ($data['rows'] as $k => $v) {
			if(isset($v->insert_time))
				$v->insert_time = time_ago($v->update_time);
			if($v->is_rekening == 1){
				$v->is_rekening = "<label class='badge badge-info'>Rekening</label>";
				$v->label = "<label class='badge badge-primary'>".ucwords($v->bank)."</label> <br><label class='badge badge-light'>".$v->no_rekening."</label><br><label class='badge badge-light'>".ucwords($v->nama)."</label>";
			}else{
				$v->is_rekening = "<label class='badge badge-light'>Kas</label>";
				$v->label = $v->label.'';
			}

			# Aksi=========================================
			$disablePar = $v->is_disabled == '1' ?true:false;
			$v->aksi  = "<div><button class='btn btn-warning btn-sm' title='Sunting' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button> ";
			$v->aksi .= "<button class='btn ".($disablePar?'btn-danger':'btn-success')." btn-sm bDisable' title='".($disablePar?'Aktifkan':'Nonaktifkan')."' onclick='disable(" . $v->id . ", ".$disablePar.")'><i class='fa ".($disablePar?'fa-ban':'fa-check')." mr-2'></i>".($disablePar?'nonaktif':'aktif')."</button></div>";
		}
		return $data;
	}
}