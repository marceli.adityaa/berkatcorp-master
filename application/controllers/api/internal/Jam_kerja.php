<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jam_kerja extends MY_Controller_api
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jam_kerja_model', 'model');
	}

	public function save($token = '')
	{
		# Autentikasi Token
		if(!$this->validateToken($token)) {
            show_404();
        }

		$data = $this->input->post();
		$data['hari'] = json_encode($data['hari']);

		if (!$data['id']) {
			# Insert
			$this->model->insert($data);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di simpan')));
		} else {
			# Update
			$id = $data['id'];
			unset($data['id']);
			$this->model->update($id, $data);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di sunting')));
		}
	}

	public function delete()
	{
		$id = $this->input->post('id');
		if ($id) {
			$this->load->model('Pegawai_model', 'pegawai');
			$count = $this->pegawai->count_by(array('jamker_id' => $id));
			if ($count > 0) { 
				die(json_encode(array('status' => 'error', 'message' => 'Profil Jam Kerja ini terikat dengan data pegawai.')));
			} else {
				$this->model->delete($id);
				die(json_encode(array('status' => 'success', 'message' => 'data berhasil dihapus')));
			}
		}

		show_404();
	}
}
