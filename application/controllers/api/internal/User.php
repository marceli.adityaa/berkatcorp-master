<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller_api
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model', 'model');
		$this->load->model('Credential_model', 'cred');
	}

	public function callback_table($data = [])
	{
		$cred = $this->cred->select('user_id, MAX(login_at) AS last_login')->group_by('user_id')->as_array()->get_all();
		$cred = array_column($cred, 'last_login', 'user_id');

		foreach ($data['rows'] as $k => $v) {
			# Terakhir login
			if (array_key_exists($v->id, $cred))
				$v->last_login = time_ago($cred[$v->id]);
			# Waktu input 
			$v->insert_time = time_ago($v->insert_time);
			# Aksi==========================================
			$disablePar = $v->is_disabled == '1' ? true : false;
			$v->aksi  = "<div><a href='" . site_url('user/edit/' . $v->id) . "' class='btn btn-warning btn-sm'><i class='icon-pencil'></i></a> ";
			$v->aksi .= "<button class='btn " . ($disablePar ? 'btn-danger' : 'btn-success') . " btn-sm bDisable' title='" . ($disablePar ? 'Aktifkan' : 'Nonaktifkan') . "' onclick='disable(" . $v->id . ", " . $disablePar . ")'><i class='fa " . ($disablePar ? 'fa-ban' : 'fa-check') . " mr-2'></i>" . ($disablePar ? 'nonaktif' : 'aktif') . "</button></div>";
		}

		return $data;
	}

	public function save($token = '')
	{
		if(!$this->validateToken($token)) {
            show_404();
        }
		
		$data = $this->input->post();

		if (!$data['id']) {
			unset($data['id']);

			if (array_has_empty($data)) {
				die(json_encode(array('status' => 'error', 'message' => 'Terdapat data yang belum diisi')));
			}

			if ($data['password'] !== $data['password_re']) {
				die(json_encode(array('status' => 'error', 'message' => 'Tulis ulang password tidak sama')));
			}

			unset($data['password_re']);
			$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
			$data['hak_akses'] = json_encode($data['hak_akses']);
			$data['role'] = json_encode($data['role_akses']);
			unset($data['role_akses']);
			$this->model->insert($data);
			die(json_encode(array('status' => 'success', 'message' => 'User berhasil ditambah')));
		} else {
			# SUNTING
			$id = $data['id'];
			unset($data['id']);

			if (empty($data['password'])) {
				unset($data['password'], $data['password_re']);
			} else {
				if (!empty($data['password']) && $data['password'] !== $data['password_re']) {
					die(json_encode(array('status' => 'error', 'message' => 'Tulis ulang password tidak sama')));
				}
				$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
				unset($data['password_re']);
			}

			if (array_has_empty($data)) {
				die(json_encode(array('status' => 'error', 'message' => 'Terdapat data yang belum diisi')));
			}

			$data['hak_akses'] = json_encode($data['hak_akses']);
			$data['role'] = json_encode($data['role_akses']);
			unset($data['role_akses']);
			$this->model->update($id, $data);
			die(json_encode(array('status' => 'success', 'message' => 'User berhasil disunting')));
		}
	}
}
