<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cuti extends MY_Controller_api
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cuti_model', 'model');
		$this->load->model('Pegawai_model', 'pegawai');
	}

	public function callback_get($data = [])
	{
		$data->nama = $this->pegawai->get($data->pegawai_id)->nama;
		return $data;
	}

	public function callback_table($data = [])
	{
		foreach ($data['rows'] as $k => $v) {
			if (!$v->pengajuan_id) {
				$v->aksi = "<div class='d-flex'><button class='flex-fill btn btn-warning btn-sm' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button>";
				$v->aksi .= "<button class='flex-fill btn btn-danger btn-sm' onclick='remove(" . $v->id . ")'><i class='icon-trash'></i></button></div>";
			} else {
				$v->aksi  = "<button class='btn btn-block btn-warning btn-sm' onclick='help()'><i class='fa fa-question'></i></button> ";
			}
		}

		return $data;
	}

	public function save($token = '')
	{
		# Autentikasi Token
		if (!$this->validateToken($token)) {
			show_404();
		}
		# Get data
		$data = $this->input->post();

		foreach ($data as $key => $value) {
			if ($key != 'id' && $key != 'keterangan') {
				if (empty($value)) {
					die(json_encode(array('status' => 'error', 'message' => $key . ' tidak boleh kosong')));
				}
			}
		}

		if (!$data['id']) {
			# Insert
			# Generate data
			$dataInsert = array();
			foreach ($data['pegawai_id'] as $id) {
				$hasCuti = $this->model->get_by(['pegawai_id' => $id, 'tanggal_cuti' => $data['tanggal_cuti']]);
				if ($hasCuti) {
					$namaPegawai = $this->pegawai->get($id)->nama;
					die(json_encode(['status'=>'error', 'message'=>$namaPegawai.' telah memiliki cuti pada tanggal ini']));
				} else {
					$tmp = $data;
					$tmp['pegawai_id'] = $id;
					$dataInsert[] = $tmp;
				}
			}
			$this->model->batch_insert($dataInsert);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di simpan')));
		} else {
			# Update
			$id = $data['id'];
			unset($data['id']);
			$this->model->update($id, $data);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di sunting')));
		}
	}

	public function delete($token = '')
	{
		if (!$this->validateToken($token)) {
			show_404();
		}

		$id = $this->input->post('id');
		$cuti = $this->model->get($id);

		if (empty($id))
			show_404();
		# Cek apakah sudah tercatat di absensi
		$this->load->model('Absensi_model', 'absensi');
		$result = $this->absensi->get_many_by(array('pegawai_id' => $cuti->pegawai_id, 'tanggal' => $cuti->tanggal_cuti, 'kehadiran' => 'cuti'));

		if ($result) {
			die(json_encode(array('status' => 'error', 'message' => 'Data cuti telah tercatat dalam absensi. Silahkan mengganti data pada absensi sebelum menghapus cuti ini.')));
		} else {
			$this->model->delete($id);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil dihapus')));
		}
	}
}
