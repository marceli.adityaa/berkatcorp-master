<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kas extends MY_Controller_admin
{
	public $class_id = 'kas';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perusahaan_model', 'perusahaan');
		$this->load->model('Bank_model', 'bank');
		// $this->load->model('Pegawai_model', 'pegawai');
	}

	public function index()
	{
		$data['perusahaan'] = $this->perusahaan->get_active();
		$data['bank'] = $this->bank->get_active();
		// $data['pegawai'] = $this->pegawai->get_data_pic();
		$this->render('kas/index', $data);
	}
}