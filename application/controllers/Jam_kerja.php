<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jam_kerja extends MY_Controller_admin
{
	public $class_id = 'jmk';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jam_kerja_model', 'jamker');
	}

	public function index()
	{
		$data['data'] = $this->jamker->order_by('nama')->get_all();
		$this->render('jam_kerja/index', $data);
	}
}