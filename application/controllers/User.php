<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller_admin
{
	public $class_id = 'usr';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model', 'model');
		$this->load->model('Menu_model', 'menu');
		$this->url_bas = $this->config->item('rice_address');
		$this->url_transport = $this->config->item('transportasi_address');
	}

	public function index()
	{
		$this->render('user/index');
	}

	public function add()
	{
		$data['menu_data'] = $this->menu->get_all_menu();
		$data['role'] = array('super', 'qc', 'supervisor', 'user');
		$this->render('user/form', $data);
	}

	public function edit($id = null)
	{
		if(empty($id))
			show_404();

		$data['user'] = $this->model->select('user.*, nama')->join('pegawai','pegawai_id=pegawai.id')->get_by(array('user.id'=>$id));
		$data['menu_data'] = $this->menu->get_all_menu();
		$data['role'] = array('super', 'qc', 'supervisor', 'user');
		if($data['user'])
			$data['user']->hak_akses = json_decode($data['user']->hak_akses);
		// dump($data['menu_data']);
		$this->render('user/form_edit', $data);
	}
}