<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cuti extends MY_Controller_admin
{
	public $class_id = 'ct';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cuti_model', 'cuti');
	}

	public function index()
	{
		$data['notif'] = $this->db->where('status',0)->count_all_results('cuti_pengajuan');
		$data['widget'] = $this->cuti->get_widget();
		$data['graph'] = $this->cuti->get_graph();
		$data['schedule'] = $this->cuti->select('nama,tanggal_cuti,kategori,keterangan')->join('pegawai','pegawai.id=pegawai_id')->order_by('tanggal_cuti')->get_many_by(array('MONTH(tanggal_cuti)'=>date('m'), 'YEAR(tanggal_cuti)'=>date('Y')));
		$this->render('cuti/index', $data);
	}

	public function data()
	{
		$this->render('cuti/data');
	}

	public function pengajuan()
	{
		$this->render('cuti/pengajuan');
	}

	public function cetak()
	{
		$this->cuti->cetak_rekap();
	}
}
