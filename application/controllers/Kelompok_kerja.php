<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelompok_kerja extends MY_Controller_admin
{
	public $class_id = 'klpk';

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->render('kelompok_kerja/index');
	}
}
