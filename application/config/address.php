<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

switch (ENVIRONMENT) {
	case 'development':
		$config['sso_address'] = 'http://127.0.0.1/berkatcorp-master/sso/server';
		$config['pegawai_address'] = 'http://127.0.0.1/berkatcorp-pegawai';
		$config['transport_address'] = 'http://127.0.0.1/berkatcorp-transportasi';
		$config['bas_address'] = 'http://127.0.0.1/berkatcorp-rice';
		break;
	case 'production':
		$config['sso_address'] = 'http://master.berkatcorp.com/sso/server';
		$config['pegawai_address'] = 'http://pegawai.berkatcorp.com/';
		$config['transport_address'] = 'http://transportasi.berkatcorp.com/';
		$config['bas_address'] = 'http://rice.berkatcorp.com/';
		break;
}