-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2019 at 09:34 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berkatcorp`
--

--
-- Dumping data for table `company_division`
--

INSERT INTO `perusahaan_divisi` (`id`, `nama`, `perusahaan_id`) VALUES
(1, 'General Affair', 1),
(2, 'General', 1),
(3, 'Development', 1),
(4, 'Construction', 2),
(5, 'DOC', 2),
(6, 'General Affair', 2),
(7, 'Kennel', 2),
(8, 'Mixer', 2),
(9, 'Packaging', 2),
(10, 'Production Close', 2),
(11, 'Production Close 2', 2),
(12, 'Production Open', 2),
(13, 'General', 2),
(14, 'Production BMI', 3),
(15, 'Furniture', 4),
(16, 'Chef', 5),
(17, 'Cleaning Service', 5),
(18, 'General Affair', 5),
(19, 'Helper', 5),
(20, 'Finance', 5),
(21, 'Marketing', 5),
(22, 'Agriculture', 7),
(23, 'Production', 5),
(24, 'Quality Control', 5),
(25, 'Security', 5),
(26, 'Transportation', 6),
(27, 'Owner', 1),
(28, 'Family', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
