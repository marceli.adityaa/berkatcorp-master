# Table : OVERTIME
delete a from overtime a join overtime b
where a.id < b.id and a.pegawai_id = b.pegawai_id and a.sumber = b.sumber and a.waktu_mulai = b.waktu_mulai and date_format(a.waktu_mulai, "%Y-%m-%d") = '2020-01-02' 

# Table : ABSENSI
delete a from absensi a join absensi b
where a.id < b.id and a.pegawai_id = b.pegawai_id and a.tanggal = b.tanggal and a.tanggal = '2020-01-02'

# Table : TABUNGAN
delete a from tabungan a join tabungan b
where a.id < b.id and a.pegawai_id = b.pegawai_id and a.tanggal_transaksi = b.tanggal_transaksi and a.tanggal_transaksi = '2020-01-02'